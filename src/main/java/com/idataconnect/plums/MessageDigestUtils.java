/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Message digest utilities.
 * @author Ben Upsavs
 */
public class MessageDigestUtils {

    private static final char[] hexDigit = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * Uses the built in Java MD5 Digester to generate an MD5 checksum of
     * <code>source</code> and formats it as a standard MD5 string.
     * @param source The source data.
     * @return The MD5 fingerprint.
     */
    public static String md5(String source) {
        return md5(new String[]{source});
    }

    /**
     * Uses the built in Java MD5 Digester to generate an MD5 checksum of
     * <code>source</code> and formats it as a standard MD5 string.
     * @param sources The source data.
     * @return The MD5 fingerprint.
     */
    public static String md5(String[] sources) {
        java.security.MessageDigest md = null;
        try {
            md = java.security.MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("No Such Algorthm (MD5)", ex);
        }

        for (String s : sources) {
            try {
                md.update(s.getBytes("US-ASCII"));
            } catch (UnsupportedEncodingException ex) {
                md.update(s.getBytes());
            }
        }
        byte[] mdBytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for (int count = 0; count < mdBytes.length; count++) {
            int d1 = (mdBytes[count] & 0x0f);
            int d2 = (mdBytes[count] & 0xf0) >>> 4;
            sb.append(hexDigit[d2]);
            sb.append(hexDigit[d1]);
        }

        return sb.toString();
    }

    /**
     * Returns the byte representation of an MD5 fingerprint, rather than
     * the hex string representation.
     * @param sources The source data.
     * @return The MD5 fingerprint as bytes.
     */
    public static byte[] md5Bytes(String[] sources) {
        java.security.MessageDigest md = null;
        try {
            md = java.security.MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("No Such Algorthm (MD5)", ex);
        }

        for (String s : sources) {
            try {
                md.update(s.getBytes("US-ASCII"));
            } catch (UnsupportedEncodingException ex) {
                md.update(s.getBytes());
            }
        }
        byte[] mdBytes = md.digest();

        return mdBytes;
    }

    /**
     * Performs a HMAC-MD5 calculation on <code>message</code>, using the
     * specified <code>key</code>.
     * @param message The message to encode.
     * @param key The key to use.
     * @return The HMAC-MD5 digest.
     */
    public static String hmacMd5(String message, String key) {
        if (key.length() > 64) {
            key = md5(key).substring(0, 16);
        }

        char[] keyXorIpad = new char[64];
        char[] keyXorOpad = new char[64];

        for (int count = 0; count < key.length(); count++) {
            keyXorIpad[count] = keyXorOpad[count] = key.charAt(count);
        }

        for (int count = 0; count < 64; count++) {
            keyXorIpad[count] ^= 0x36;
            keyXorOpad[count] ^= 0x5c;
        }

        byte[] digestBytes = md5Bytes(new String[]{new String(keyXorIpad), message});

        String digest = md5(new String[]{new String(keyXorOpad), new String(digestBytes)});

        return digest;
    }
}
