/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums;

import com.idataconnect.plums.config.ConfigFrame;
import java.awt.GraphicsEnvironment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * A configuration program. This program is designed to make initial and
 * ongoing configuration as easy as possible for the user. There are two interfaces
 * that this program can run as - as CLI interface and a GUI interface. The GUI
 * interface is preferred, although some users will be connecting through a
 * headless system and must use the CLI version. both interfaces should be as up
 * to date as each other.
 * @author Ben Upsavs
 */
public class ConfigMain {

    /**
     * The main method. It will start the configuration program in either CLI
     * mode or GUI mode. The -cli and -gui arguments can be used to explicitly
     * choose the mode.
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        boolean cli = true;
        boolean forceCli = false;
        boolean forceGui = false;

        // Assume cli but change to GUI if possible, unless cli is requested.
        if (args.length > 0) {
            if (args[0].equals("-cli"))
                forceCli = true;
            else if (args[0].equals("-gui"))
                forceGui = true;
        }
        if (GraphicsEnvironment.getLocalGraphicsEnvironment().isHeadlessInstance())
            cli = true;

        if (forceCli)
            cli = true;
        else if (forceGui)
            cli = false;

        if (!cli) {
            try {
                showGraphicalInterface();
            } catch (Exception ex) {
                System.err.println("Unable to open configuration window; Falling back to CLI");
                enterCommandLineInterfaceLoop();
            }
        } else {
            enterCommandLineInterfaceLoop();
        }
    }

    public static void enterCommandLineInterfaceLoop() {
        System.err.println(Main.PRODUCT_NAME + " " + Main.VERSION);
        System.err.println("Copyright " + Main.COPYRIGHT_START + "-" + Main.COPYRIGHT_END + " " + Main.PRODUCT_COMPANY_NAME);
        System.err.println("Created by Ben Upsavs <ben@idataconnect.com>");
        System.err.println("This free software comes with no warranty.");
        System.err.println("");
        System.err.println("Type help for a list of commands");
        System.err.println("");

        // Create a BufferedReader in order to read one line at a time.
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        boolean changesSaved = true;
        while (true) {
            System.err.print("> ");
            System.err.flush();

            try {
                String input = in.readLine();

                if (input.equalsIgnoreCase("q") || input.equalsIgnoreCase("quit")) {
                    if (!changesSaved) {
                        System.err.println("There are unsaved changes. Type q! to force");
                    } else {
                        System.err.println("Goodbye");
                        break;
                    }
                }
                if (input.equalsIgnoreCase("q!") || input.equalsIgnoreCase("quit!")) {
                    if (!changesSaved) {
                        System.err.println("Discarding unsaved changes");
                    } else {
                        System.err.println("Goodbye");
                    }

                    break;
                } else if (input.equalsIgnoreCase("h") || input.equalsIgnoreCase("help") || input.equals("?")) {
                    System.err.println("Available commands:");
                    System.err.println("(h)Help");
                    System.err.println("(q)Quit");
                    System.err.println("(s)Save");
                    System.err.println("(l)License");
                    System.err.println("(sl)SMTP LIST; (sc)SMTP CREATE; (sd)SMTP DELETE; (se)SMTP EDIT");
                    System.err.println("(pl)POP LIST; (pc)POP CREATE; (pd)POP DELETE; (pe)POP EDIT");
                    System.err.println("(il)IMAP LIST; (ic)IMAP CREATE; (id)IMAP DELETE; (ie)IMAP EDIT");
                } else if (input.equalsIgnoreCase("l") || input.equalsIgnoreCase("license")) {
                    System.err.println("Copyright (c) " + Main.COPYRIGHT_START + "-" + Main.COPYRIGHT_END + ", " + Main.PRODUCT_COMPANY_NAME);
                    System.err.println("All rights reserved.");
                    System.err.println("");
                    System.err.println("Redistribution and use in source and binary forms, with or without");
                    System.err.println("modification, are permitted provided that the following conditions are met:");
                    System.err.println("");
                    System.err.println("Redistributions of source code must retain the above copyright notice,");
                    System.err.println("this list of conditions and the following disclaimer.");
                    System.err.println("");
                    System.err.println("Redistributions in binary form must reproduce the above copyright notice,");
                    System.err.println("this list of conditions and the following disclaimer in the documentation");
                    System.err.println("and/or other materials provided with the distribution.");
                    System.err.println("");
                    System.err.println("Neither the name of i Data Connect! nor the names of its contributors");
                    System.err.println("may be used to endorse or promote products derived from this software without");
                    System.err.println("specific prior written permission.");
                    System.err.println("");
                    System.err.println("THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"");
                    System.err.println("AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE");
                    System.err.println("IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE");
                    System.err.println("ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE");
                    System.err.println("LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR");
                    System.err.println("CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF");
                    System.err.println("SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS");
                    System.err.println("INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN");
                    System.err.println("CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)");
                    System.err.println("ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE");
                    System.err.println("POSSIBILITY OF SUCH DAMAGE.");
                } else {
                    System.err.println("Unknown command. Type h for help.");
                }
            } catch (IOException ex) {
                System.err.println("I/O error while reading stdin");
                System.exit(1);
            }
            System.err.println();
        }
    }

    public static void showGraphicalInterface() {
        new ConfigFrame().setVisible(true);
    }
}
