/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.imap;

import com.idataconnect.plums.entity.MailMessage;

/**
 * Asynchronous action to be performed on a mailbox by the IMAP protocol
 * handler.
 * @author Ben Upsavs
 */
public class MailboxAction {

    /**
     * The type of action to perform.
     */
    public enum AsyncActionType {
        
        /* Notification when flags are updated for a message in the mailbox. */
        FETCH_FLAGS,
        /* Notification when new messages are added to the mailbox. */
        NEW_MESSAGES,
    }
    private AsyncActionType actionType;
    private MailMessage message;

    /**
     * Gets the type of this action.
     * @return The type of this action.
     */
    public AsyncActionType getActionType() {
        return actionType;
    }

    /**
     * Sets the type of this action.
     * @param actionType The type of this action.
     */
    public void setActionType(AsyncActionType actionType) {
        this.actionType = actionType;
    }

    /**
     * Gets the mail message that this action is to be performed on.
     * @return The mail message that this action is to be performed on.
     */
    public MailMessage getMessage() {
        return message;
    }

    /**
     * Sets the mail message that this action is to be performed on.
     * @param message The mail message that this action is to be performed on.
     */
    public void setMessage(MailMessage message) {
        this.message = message;
    }
}
