/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.imap;

import com.idataconnect.plums.entity.MailMessage;
import com.idataconnect.plums.entity.MessageFlag;
import com.idataconnect.plums.network.PlumsGrizzlyFilter;
import com.idataconnect.plums.network.ServerInstance;
import com.sun.grizzly.Context;
import com.sun.grizzly.ProtocolParser;
import com.sun.grizzly.util.WorkerThread;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Grizzly <code>ProtocolFilter</code> implementation for IMAP.
 * @author Ben Upsavs
 */
public class IMAPGrizzlyFilter implements PlumsGrizzlyFilter
{
    static final Map<SelectableChannel, IMAPConnectionState> connections = new HashMap<SelectableChannel, IMAPConnectionState>();

    /**
     * Immediately performs the requested mailbox action and sends the result
     * to the client.
     * @param state The state of the connection.
     * @param action The action to perform.
     * @param mailboxName The mailbox name to perform the action on.
     */
    static void doMailboxAction(IMAPConnectionState state, MailboxAction action, String mailboxName) {
        try {
            final SocketChannel channel = (SocketChannel) state.getChannel();

            switch (action.getActionType()) {
                case FETCH_FLAGS:
                    synchronized (channel) {
                        // Make sure the selected mailbox hasn't changed.
                        if (state.getSelectedMailbox() == null || !mailboxName.equals(state.getSelectedMailbox().getMailboxName())) {
                            return;
                        }

                        Iterator<MailMessage> i = state.getSelectedMailbox().getMessages().iterator();
                        for (int count = 0; i.hasNext(); count++) {
                            MailMessage m = i.next();

                            if (m.getMessageUid() == action.getMessage().getMessageUid()) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("* ");
                                sb.append(count + 1);
                                sb.append(" FETCH (FLAGS (");
                                Iterator<MessageFlag> fi = action.getMessage().getFlags().iterator();
                                for (boolean first = true; fi.hasNext(); first = false) {
                                    if (!first) {
                                        sb.append(' ');
                                    }

                                    MessageFlag f = fi.next();

                                    sb.append(f.getName());
                                }
                                sb.append("))\r\n");

                                ByteBuffer buf = ByteBuffer.wrap(sb.toString().getBytes());
                                while (buf.hasRemaining()) {
                                    channel.write(buf);
                                }

                                break;
                            }
                        }
                    }
                    break;
                case NEW_MESSAGES:
                    synchronized (channel) {
                        // Make sure the selected mailbox hasn't changed.
                        if (state.getSelectedMailbox() == null || !mailboxName.equals(state.getSelectedMailbox().getMailboxName())) {
                            return;
                        }

                        state.getSelectedMailbox().getMessages().add(action.getMessage());
                        ByteBuffer buf = ByteBuffer.wrap(("* " + state.getSelectedMailbox().getMessages().size() + " EXISTS\r\n").getBytes());
                        while (buf.hasRemaining()) {
                            channel.write(buf);
                        }
                    }
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(IMAPGrizzlyFilter.class.getName()).log(Level.WARNING,
                    "I/O error while applying mailbox action", ex);
        }
    }

    /**
     * Checks for messages that have been expunged since the last check and,
     * if there are any, sends the result to the client. This should only
     * be called when it is permitted per the IMAP specs.
     * @param state The connection state.
     * @param mailboxName The mailbox name to check for expunged messages.
     */
    static void checkForExpunged(IMAPConnectionState state, String mailboxName) {
        try {
            if (state.getExpungedMessages().size() > 0) {
                final SocketChannel c = (SocketChannel) state.getChannel();

                // synchronized so that asynchronous messages won't interfere with
                // this.
                synchronized (c) {
                    // Make sure the selected mailbox hasn't changed.
                    if (state.getSelectedMailbox() == null || !mailboxName.equals(state.getSelectedMailbox().getMailboxName())) {
                        return;
                    }

                    ByteBuffer buf = null;

                    while (state.getExpungedMessages().size() > 0) {
                        Integer expungedUid = state.getExpungedMessages().first();
                        state.getExpungedMessages().remove(expungedUid);
                        int expungedUidInt = expungedUid.intValue();

                        Iterator<MailMessage> i = state.getSelectedMailbox().getMessages().iterator();
                        for (int count = 0; i.hasNext(); count++) {
                            MailMessage m = i.next();
                            if (m.getMessageUid() == expungedUidInt) {
                                if (buf == null) {
                                    buf = ByteBuffer.allocate(128);
                                }

                                i.remove();
                                buf.clear();
                                buf.put(("* EXPUNGE " + (count + 1) + "\r\n").getBytes());
                                buf.flip();
                                while (buf.hasRemaining()) {
                                    c.write(buf);
                                }

                                break;
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IMAPGrizzlyFilter.class.getName()).log(Level.WARNING,
                    "I/O error while checking for expunged messages", ex);
        }
    }

    /**
     * Sends a goodbye message if necessary, and releases all resources for
     * the connection.
     * @param channel The channel that the connection is associated with.
     */
    public void cleanupConnection(SocketChannel channel) {
        try {
            if (channel.isOpen()) {
                IMAPConnectionState s = connections.get(channel);
                if (s != null) {
                    if (s.isNeedsDisconnectMessage()) {
                        try {
                            ByteBuffer buf = ByteBuffer.wrap("* BYE Connection timed out\r\n".getBytes());
                            synchronized (channel) {
                                while (buf.hasRemaining())
                                    ((SocketChannel)channel).write(buf);
                            }
                        }
                        catch (Exception ex) {}
                    }

                    Logger log = Logger.getLogger(IMAPGrizzlyFilter.class.getName());
                    log.log(Level.INFO,
                            "IMAP disconnected {0}", s.getRemoteHost().getHostAddress());
                    log.log(Level.FINER,
                            "There are now {0} active IMAP connections", connections.size());
                }
            }
        } finally {
            connections.remove(channel);
        }
    }

    /**
     * Called when a new IMAP client connects. It sends the initial greeting and
     * sets up the connection state.
     * @param channel The channel associated with the connection.
     * @param serverInstance The server instance that received the connection.
     * @param remoteHost The remote IP address that connected.
     */
    public void newConnection(SocketChannel channel, ServerInstance serverInstance, InetAddress remoteHost) {
        IMAPConnectionState state = new IMAPConnectionState();
        state.setChannel(channel);
        state.setServerInstance((IMAPServerInstance) serverInstance);
        state.setRemoteHost(remoteHost);

        String greeting = "* OK " + serverInstance.getNetworkAttributes().getHostname() + " IMAP service ready\r\n";
        ByteBuffer buf = ByteBuffer.wrap(greeting.getBytes());
        try {
            synchronized (channel) {
                while (buf.hasRemaining()) {
                    channel.write(buf);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IMAPGrizzlyFilter.class.getName()).warning("I/O error while sending IMAP greeting");
        }

        connections.put(channel, state);
        Logger log = Logger.getLogger(IMAPGrizzlyFilter.class.getName());
        log.log(Level.INFO, "IMAP connection from {0}", remoteHost.getHostAddress());
        log.log(Level.FINER, "There are now {0} active IMAP connections", connections.size());
    }

    /**
     * {@inheritDoc}
     */
    public boolean execute(Context ctx) throws IOException {
        String s = (String) ctx.getAttribute(ProtocolParser.MESSAGE);
        if (s != null) {
            WorkerThread workerThread = (WorkerThread) Thread.currentThread();
            SelectableChannel channel = ctx.getSelectionKey().channel();
            ByteBuffer buf = workerThread.getByteBuffer();
            buf.clear();

            IMAPCommandResponse response = IMAPProtocolHandler.processCommand(s, connections.get(channel));

            if (response != null) {
                // Synchronized so asynchronous messages won't interleave.
                synchronized (channel) {
                    for (int pos = 0; pos < response.getResponseMessage().getBytes().length; pos += buf.capacity()) {
                        buf.put(response.getResponseMessage().getBytes(), pos, Math.min(buf.capacity(), response.getResponseMessage().getBytes().length - pos));
                        buf.flip();
                        while (buf.hasRemaining()) {
                            ((SocketChannel) channel).write(buf);
                        }
                        buf.clear();
                    }

                    if (response.isDisconnect()) {
                        connections.get(channel).setNeedsDisconnectMessage(false);
                        ctx.getSelectorHandler().closeChannel(channel);
                    }
                }
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    public boolean postExecute(Context ctx) throws IOException {
        return true;
    }
}