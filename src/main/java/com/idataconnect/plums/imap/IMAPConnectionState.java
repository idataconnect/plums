/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.imap;

import com.idataconnect.plums.entity.MailUser;
import com.idataconnect.plums.entity.Mailbox;
import java.net.InetAddress;
import java.nio.channels.SelectableChannel;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * State information for one IMAP connection.
 * @author Ben Upsavs
 */
public class IMAPConnectionState {
    /**
     * The main IMAP state. This matches the states documented in the IMAP
     * specs.
     */
    public static enum MainState {

        /** The connection has not yet been authenticated. */
        NOT_AUTHENTICATED,

        /**
         * The connection has been authenticated, but no IMAP mailbox
         * has been selected yet.
         */
        AUTHENTICATED,

        /**
         * The connection is authenticated and an IMAP mailbox has been selected.
         */
        SELECTED,
    }

    private MainState mainState = MainState.NOT_AUTHENTICATED;
    private IMAPServerInstance serverInstance;
    private InetAddress remoteHost;
    private int recentBadCommands;
    private List<String> arguments = new LinkedList<String>();
    private int literalCharacters = -1;
    private Mailbox selectedMailbox;
    private boolean readWrite;
    private MailUser authenticatedUser;
    private boolean needsDisconnectMessage = true;
    private SortedSet<Integer> recentMessages; // initialized in setMainState()
    private SortedSet<Integer> expungedMessages;
    private SelectableChannel channel;

    /**
     * Queues an action which is propagated to all IMAP connections which
     * are currently connected to this mailbox.
     *
     * @param mailbox The mailbox to queue the action for.
     * @param action The action to queue.
     */
    static void queueMailboxAction(Mailbox mailbox, MailboxAction action) {
        synchronized (Mailbox.class) {
            Set<IMAPConnectionState> connections = mailbox.getImapConnections();
            for (IMAPConnectionState s : connections) {
                // TODO finish this
            }
        }
    }

    /**
     * Gets the main state of the connection.
     *
     * @return The main state of the connection.
     */
    public MainState getMainState() {
        return mainState;
    }

    /**
     * Sets the main state of the connection.
     *
     * @param mainState The main state of the connection.
     */
    public void setMainState(MainState mainState) {
        if (mainState.equals(MainState.AUTHENTICATED)) {
            recentMessages = new TreeSet<Integer>();
            expungedMessages = new TreeSet<Integer>();
        }

        this.mainState = mainState;
    }

    /**
     * Gets the server instance that this connection is associated with.
     *
     * @return The server instance that this connection is associated with.
     */
    public IMAPServerInstance getServerInstance() {
        return serverInstance;
    }

    /**
     * Sets the server instance that this connection is associated with.
     *
     * @param serverInstance The server instance that this connection is
     * associated with.
     */
    public void setServerInstance(IMAPServerInstance serverInstance) {
        this.serverInstance = serverInstance;
    }

    /**
     * Gets the remote host that initiated this connection.
     *
     * @return The remote host that initiated this connection.
     */
    public InetAddress getRemoteHost() {
        return remoteHost;
    }

    /**
     * Sets the remote host that initiated this connection.
     *
     * @param remoteHost The remote host that initiated this connection.
     */
    public void setRemoteHost(InetAddress remoteHost) {
        this.remoteHost = remoteHost;
    }

    /**
     * Gets the number of invalid commands recently sent to the server.
     *
     * @return The number of recent bad commands.
     */
    public int getRecentBadCommands() {
        return recentBadCommands;
    }

    /**
     * Sets the number of invalid commands recently sent to the server.
     *
     * @param recentBadCommands The number of recent bad commands.
     */
    public void setRecentBadCommands(int recentBadCommands) {
        this.recentBadCommands = recentBadCommands;
    }

    /**
     * Gets the list of arguments for the current command.
     *
     * @return The list of arguments for the current command.
     */
    public List<String> getArguments() {
        return arguments;
    }

    /**
     * Sets the list of arguments for the current command.
     *
     * @param arguments The list of arguments for the current command.
     */
    public void setArguments(LinkedList<String> arguments) {
        this.arguments = arguments;
    }

    /**
     * Gets the number of literal characters requested in the current command.
     *
     * @return The number of literal characters requested in the current command.
     */
    public int getLiteralCharacters() {
        return literalCharacters;
    }

    /**
     * Sets the number of literal characters requested in the current command.
     *
     * @param literalCharacters The number of literal characters requested in
     * the current command.
     */
    public void setLiteralCharacters(int literalCharacters) {
        this.literalCharacters = literalCharacters;
    }

    /**
     * Gets whether the connection to the current mailbox is read-write
     * or read-only.
     *
     * @return Whether the connection is read-write.
     */
    public boolean isReadWrite() {
        return readWrite;
    }

    /**
     * Sets whether the connection to the current mailbox is read-write
     * or read-only.
     *
     * @param readWrite Whether the connection is read-write.
     */
    public void setReadWrite(boolean readWrite) {
        this.readWrite = readWrite;
    }

    /**
     * Gets the authenticated user.
     *
     * @return The authenticated user, or <code>null</code> if no user has
     * been authenticated.
     */
    public MailUser getAuthenticatedUser() {
        return authenticatedUser;
    }

    /**
     * Sets the authenticated user.
     *
     * @param authenticatedUser The authenticated user.
     */
    public void setAuthenticatedUser(MailUser authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    /**
     * Gets the currently selected mailbox.
     *
     * @return The currently selected mailbox.
     */
    public Mailbox getSelectedMailbox() {
        return selectedMailbox;
    }

    /**
     * Sets the currently selected mailbox.
     *
     * @param selectedMailbox The currently selected mailbox.
     */
    public void setSelectedMailbox(Mailbox selectedMailbox) {
        this.selectedMailbox = selectedMailbox;
    }

    /**
     * Gets whether to send a goodbye message to the client before
     * disconnecting.
     *
     * @return Whether to send a disconnect message.
     */
    public boolean isNeedsDisconnectMessage() {
        return needsDisconnectMessage;
    }

    /**
     * Sets whether to send a goodbye message to the client before
     * disconnecting.
     *
     * @param needsDisconnectMessage Whether to send a disconnect message.
     */
    public void setNeedsDisconnectMessage(boolean needsDisconnectMessage) {
        this.needsDisconnectMessage = needsDisconnectMessage;
    }

    /**
     * Gets the message ids of all recent messages.
     *
     * @return The message ids of all recent messages.
     */
    public SortedSet<Integer> getRecentMessages() {
        return recentMessages;
    }

    /**
     * Sets the message ids of all recent messages.
     *
     * @param recentMessages The message ids of all recent messages.
     */
    public void setRecentMessages(SortedSet<Integer> recentMessages) {
        this.recentMessages = recentMessages;
    }

    /**
     * Gets the message ids of all expunged messages.
     *
     * @return The message ids of all expunged messages.
     */
    public SortedSet<Integer> getExpungedMessages() {
        return expungedMessages;
    }

    /**
     * Sets the message ids of all expunged messages.
     *
     * @param expungedMessages The message ids of all expunged messages.
     */
    public void setExpungedMessages(SortedSet<Integer> expungedMessages) {
        this.expungedMessages = expungedMessages;
    }

    /**
     * Gets the {@link SelectableChannel} associated with this connection.
     *
     * @return The <code>SelectableChannel</code> associated with this
     * connection.
     */
    public SelectableChannel getChannel() {
        return channel;
    }

    /**
     * Sets the {@link SelectableChannel} associated with this connection.
     *
     * @param channel The <code>SelectableChannel</code> associated
     * with this connection.
     */
    public void setChannel(SelectableChannel channel) {
        this.channel = channel;
    }
}