/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.imap;

import com.beetstra.jutf7.CharsetProvider;
import com.idataconnect.plums.Persistence;
import com.idataconnect.plums.entity.*;
import com.idataconnect.plums.imap.IMAPConnectionState.MainState;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Pattern;

/**
 * IMAP Protocol Handler.
 * @author Ben Upsavs
 */
public class IMAPProtocolHandler {

    /**
     * The RFC3501 modified UTF-7 charset for encoding mailbox names.
     */
    static Charset modifiedUtf7Charset = new CharsetProvider().charsetForName("X-IMAP-MODIFIED-UTF-7");

    /**
     * Process an IMAP command. The command will be checked for literals, and
     * once it contains no more, {@link #processParsedCommand} will be
     * called.
     * @param command The command sent from the client
     * @param state The state associated with this connection
     * @return The response to send to the client, or <code>null</code> if no
     * response should be sent.
     */
    public static IMAPCommandResponse processCommand(String command, IMAPConnectionState state) {
        // XXX This routine needs to be fixed.. it always processes literals
        // and quoted arguments, and it should only do so when expecting
        // a string. There should probably be a hard coded list of parameter
        // types for each command, and they should be enforced in this method
        // before passing the command to processParsedCommand(). The RFC also
        // requires that we only allow a single space between arguments and
        // there should be no extra arguments for commands, so we should
        // probably dump the tokenizer and do things with a custom parser.

        IMAPCommandResponse response = null;
        boolean error = false;
        boolean firstAsLiteral = false;

        if (state.getLiteralCharacters() != -1) {
            // A literal was used on the previous line, so we have to use
            // the requested length for our first arg
            if (state.getLiteralCharacters() == 0) {
                state.getArguments().add("");
            } else {
                int literalLength = Math.min(state.getLiteralCharacters(), command.length());
                state.getArguments().add(command.substring(0, literalLength));
                command = command.substring(literalLength);
                firstAsLiteral = true;
            }
        }

        StringTokenizer st = new StringTokenizer(command, " ", true);

        state.setLiteralCharacters(-1); // Remove literal from state
        boolean literalPlus = false;

        for (int tokenCount = 0; st.hasMoreTokens(); tokenCount++) {
            String arg = st.nextToken();
            if ((firstAsLiteral || tokenCount > 0) && !st.hasMoreTokens()
                    && arg.length() > 2 && arg.charAt(0) == '{'
                    && arg.charAt(arg.length() - 1) == '}') {
                // There appears to be a literal on the end of the command
                int literalCharacters = parseLiteral(arg);
                if (arg.charAt(arg.length() - 2) == '+') {
                    literalPlus = true;
                }
                if (literalCharacters != -1) {
                    state.setLiteralCharacters(literalCharacters);
                }
            } else {
                // Join quoted arguments
                if (arg.charAt(0) == '"') {
                    StringBuilder sb = new StringBuilder();
                    if (arg.charAt(arg.length() - 1) == '"') {
                        sb.append(arg.substring(1, arg.length() - 1));
                    } else {
                        sb.append(arg.substring(1));
                    }
                    while (st.hasMoreTokens() && (arg.charAt(arg.length() - 1) != '"')) {
                        arg = st.nextToken();
                        if (arg.charAt(arg.length() - 1) == '"') {
                            sb.append(arg.substring(0, arg.length() - 1));
                        } else {
                            sb.append(arg);
                        }
                    }
                    state.getArguments().add(sb.toString());
                } else {
                    if (arg.trim().length() > 0) {
                        state.getArguments().add(arg);
                    }
                }
            }
        }

        if (response != null) {
            if (error) {
                state.setRecentBadCommands(state.getRecentBadCommands() + 1);
                if (state.getRecentBadCommands() > 15) {
                    response = new IMAPCommandResponse("* BAD Too many bad commands. Disconnecting.\r\n", true);
                }
            } else {
                state.setRecentBadCommands(0);
            }

            return response;
        }

        if (state.getLiteralCharacters() == -1) {
            return processParsedCommand(state, command);
        } else {
            if (!literalPlus) {
                return new IMAPCommandResponse("+ Go ahead\r\n", false);
            } else {
                return null;
            }
        }
    }

    /**
     * Processes an IMAP command after literals have been parsed and all lines
     * have their arguments stored in the state object.
     * @param state The connection state.
     * @return A response to send to the client, or <code>null</code> if no
     * response should be sent.
     */
    public static IMAPCommandResponse processParsedCommand(IMAPConnectionState state, String command) {
        boolean error = false;
        IMAPCommandResponse response = null;

        if (state.getArguments().isEmpty()) {
            response = new IMAPCommandResponse("* BAD Received nothing\r\n", false);
        } else {
            Iterator<String> i = state.getArguments().iterator();
            // Get the client generated tag (e.g. a0001)
            String tag = i.next();
            if (!i.hasNext()) {
                response = new IMAPCommandResponse("* BAD Invalid tag\r\n", false);
                error = true;
            } else {
                String firstCommandPart = i.next();
                if (firstCommandPart.equalsIgnoreCase("LOGOUT")) {
                    response = new IMAPCommandResponse("* BYE Closing transmission channel\r\n" + tag + " OK LOGOUT completed\r\n", true);
                } else if (firstCommandPart.equalsIgnoreCase("NOOP")) {
                    if (state.getAuthenticatedUser() != null &&
                            state.getSelectedMailbox() != null)
                        IMAPGrizzlyFilter.checkForExpunged(state, state.getSelectedMailbox().getMailboxName());
                    response = new IMAPCommandResponse(tag + " OK NOOP completed\r\n", false);
                } else if (firstCommandPart.equalsIgnoreCase("LOGIN")) {
                    if (state.getMainState() != MainState.NOT_AUTHENTICATED) {
                        response = new IMAPCommandResponse(tag + " BAD Already logged in\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing username and password\r\n", false);
                            error = true;
                        } else {
                            String username = i.next();

                            if (!i.hasNext()) {
                                response = new IMAPCommandResponse(tag + " BAD Missing password\r\n", false);
                                error = true;
                            } else {
                                String password = i.next();

                                MailUser u = Persistence.getUserByEmailAddressAndPassword(username, password);
                                if (u == null) {
                                    response = new IMAPCommandResponse(tag + " NO Authentication failed\r\n", false);
                                } else {
                                    state.setAuthenticatedUser(u);
                                    state.setMainState(MainState.AUTHENTICATED);
                                    response = new IMAPCommandResponse(tag + " OK [CAPABILITY " + getCapabilities(true) + "] Logged in\r\n", false);
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("CAPABILITY")) {
                    response = new IMAPCommandResponse("* CAPABILITY " + getCapabilities(state.getAuthenticatedUser() != null) + "\r\n" + tag + " OK Capability list completed\r\n", false);
                } else if (firstCommandPart.equalsIgnoreCase("SELECT") || firstCommandPart.equalsIgnoreCase("EXAMINE")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD missing mailbox name\r\n", false);
                            error = true;
                        } else {
                            String mailboxName = fromModifiedUtf7(i.next());
                            Mailbox m = getMailboxByOwnerName(state.getAuthenticatedUser(), mailboxName);
                            if (m == null) {
                                response = new IMAPCommandResponse(tag + " NO Mailbox does not exist\r\n", false);
                                state.setMainState(MainState.AUTHENTICATED);
                                error = true;
                            } else if (!hasAccessToMailbox(state.getAuthenticatedUser(), m)) {
                                response = new IMAPCommandResponse(tag + " NO Access denied\r\n", false);
                                state.setMainState(MainState.AUTHENTICATED);
                                error = true;
                            } else if (m.isNoSelect()) {
                                response = new IMAPCommandResponse(tag + " NO Mailbox cannot be selected\r\n", false);
                                state.setMainState(MainState.AUTHENTICATED);
                                error = true;
                            } else {
                                state.setSelectedMailbox(m);
                                state.setReadWrite(firstCommandPart.equalsIgnoreCase("SELECT"));
                                state.setMainState(MainState.SELECTED);
                                StringBuilder sb = new StringBuilder();
                                sb.append("* FLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen)\r\n");
                                sb.append("* OK [PERMANENTFLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen \\*)]\r\n");
                                sb.append("* ").append(m.getMessages().size()).append(" EXISTS\r\n");
                                sb.append("* ").append(state.getRecentMessages().size()).append(" RECENT\r\n");
                                sb.append("* OK [UIDVALIDITY ").append(m.getUidValidity()).append("]\r\n");
                                sb.append("* OK [UIDNEXT ").append(m.getNextUid()).append("]\r\n");
                                sb.append(tag).append(" OK [READ-").append(state.isReadWrite() ? "WRITE" : "ONLY").append("] Completed\r\n");
                                response = new IMAPCommandResponse(sb.toString(), false);
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("UNSELECT")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        state.setMainState(MainState.AUTHENTICATED);
                        response = new IMAPCommandResponse(tag + " OK Unselect completed\r\n", false);
                    }
                } else if (firstCommandPart.equalsIgnoreCase("CREATE")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                            error = true;
                        } else {
                            String mailboxName = i.next();

                            if (mailboxName.length() == 0) {
                                response = new IMAPCommandResponse(tag + " NO Empty mailbox name\r\n", false);
                            } else {
                                Mailbox f = new Mailbox();

                                if (!contains8Bit(mailboxName)) {
                                    mailboxName = fromModifiedUtf7(mailboxName);
                                }

                                f.setMailboxName(getStoreMailboxName(state.getAuthenticatedUser(), mailboxName));

                                if (!hasAccessToMailbox(state.getAuthenticatedUser(), f)) {
                                    response = new IMAPCommandResponse(tag + " NO Access denied\r\n", false);
                                } else {
                                    // Okay to create mailbox.
                                    String mailboxCreationError = Persistence.addMailboxAndParents(f);
                                    if (mailboxCreationError != null) {
                                        response = new IMAPCommandResponse(tag + " NO " + mailboxCreationError + "\r\n", false);
                                    } else {
                                        response = new IMAPCommandResponse(tag + " OK Mailbox created\r\n", false);
                                    }
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("DELETE")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                            error = true;
                        } else {
                            String mailboxName = i.next();

                            if (mailboxName.length() == 0) {
                                response = new IMAPCommandResponse(tag + " NO Empty mailbox name\r\n", false);
                            } else {
                                Mailbox f = new Mailbox();

                                if (!contains8Bit(mailboxName)) {
                                    mailboxName = fromModifiedUtf7(mailboxName);
                                }

                                f.setMailboxName(getStoreMailboxName(state.getAuthenticatedUser(), mailboxName));

                                if (!hasAccessToMailbox(state.getAuthenticatedUser(), f)) {
                                    response = new IMAPCommandResponse(tag + " NO Access denied\r\n", false);
                                } else {
                                    // Okay to delete mailbox.

                                    // Fetch a copy with the correct primary key
                                    f = Persistence.getMailboxByName(f.getMailboxName());
                                    if (f == null) {
                                        response = new IMAPCommandResponse(tag + " NO Mailbox does not exist\r\n", false);
                                    } else {
                                        String deleteError = Persistence.deleteMailbox(f);
                                        if (deleteError == null) {
                                            response = new IMAPCommandResponse(tag + " OK Mailbox deleted\r\n", false);
                                        } else {
                                            response = new IMAPCommandResponse(tag + " NO " + deleteError + "\r\n", false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("LIST")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing reference name and mailbox name\r\n", false);
                            error = true;
                        } else {
                            String referenceName = i.next();

                            if (!i.hasNext()) {
                                response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                                error = true;
                            } else {
                                String mailboxName = i.next();

                                // Handle the case where wildcard
                                // characters aren't encoded with modified UTF7
                                StringTokenizer st = new StringTokenizer(referenceName + mailboxName, "%*", true);
                                StringBuilder sb = new StringBuilder();
                                while (st.hasMoreTokens()) {
                                    String token = st.nextToken();
                                    if (token.equals("%") || token.equals("*")) {
                                        sb.append(token);
                                    } else {
                                        sb.append(fromModifiedUtf7(token));
                                    }
                                }
                                String completeMailboxName = sb.toString();

                                if (completeMailboxName.length() == 0) {
                                    // They just want the hierarchy separator
                                    response = new IMAPCommandResponse("* LIST (\\Noselect) \".\" \"\"\r\n"
                                            + tag + " OK Hierarchy separator sent\r\n", false);
                                } else {
                                    List<Mailbox> mailboxes = getMailboxesBySearchTerm(state.getAuthenticatedUser(), completeMailboxName);
                                    sb = new StringBuilder();
                                    for (Mailbox m : mailboxes) {
                                        sb.append("* LIST (\\Has");
                                        if (!m.isHasChildren()) {
                                            sb.append("No");
                                        }
                                        sb.append("Children");
                                        if (m.isNoSelect()) {
                                            sb.append(" \\Noselect");
                                        }
                                        sb.append(") \".\" \"");
                                        sb.append(getOwnerMailboxName(state.getAuthenticatedUser(), m));
                                        sb.append("\"\r\n");
                                    }
                                    sb.append(tag);
                                    sb.append(" OK Completed mailbox list\r\n");
                                    response = new IMAPCommandResponse(sb.toString(), false);
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("LSUB")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing reference name and mailbox name\r\n", false);
                            error = true;
                        } else {
                            String referenceName = i.next();

                            if (!i.hasNext()) {
                                response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                                error = true;
                            } else {
                                String mailboxName = i.next();
                                // Handle the case where wildcard
                                // characters aren't encoded with modified UTF7
                                StringTokenizer st = new StringTokenizer(referenceName + mailboxName, "%*", true);
                                StringBuilder sb = new StringBuilder();
                                while (st.hasMoreTokens()) {
                                    String token = st.nextToken();
                                    if (token.equals("%") || token.equals("*")) {
                                        sb.append(token);
                                    } else {
                                        sb.append(fromModifiedUtf7(token));
                                    }
                                }
                                String completeMailboxName = sb.toString();

                                if (completeMailboxName.length() == 0) {
                                    response = new IMAPCommandResponse(tag + " OK\r\n", false);
                                } else {
                                    // Fetch the subscriptions for the current
                                    // user and put them in a hash set so they
                                    // can be looked up quickly.
                                    List<MailboxSubscription> subscriptions = Persistence.getMailboxSubscriptionsForMailUser(state.getAuthenticatedUser());
                                    Set<String> subscribedMailboxNames = new HashSet<String>(32);
                                    for (MailboxSubscription s : subscriptions) {
                                        subscribedMailboxNames.add(s.getMailboxName());
                                    }

                                    List<Mailbox> mailboxes = getMailboxesBySearchTerm(state.getAuthenticatedUser(), completeMailboxName);
                                    sb = new StringBuilder();
                                    for (Mailbox m : mailboxes) {
                                        // Skip the mailbox if it's not subscribed to.
                                        if (!subscribedMailboxNames.contains(m.getMailboxName())) {
                                            continue;
                                        }

                                        sb.append("* LSUB (");
                                        if (m.isHasChildren()) {
                                            sb.append("\\HasChildren");
                                        }
                                        if (m.isNoSelect()) {
                                            if (m.isHasChildren()) {
                                                sb.append(' ');
                                            }
                                            sb.append("\\Noselect");
                                        }
                                        sb.append(") \".\" \"");
                                        sb.append(getOwnerMailboxName(state.getAuthenticatedUser(), m));
                                        sb.append("\"\r\n");
                                    }
                                    sb.append(tag);
                                    sb.append(" OK Completed mailbox subscription list\r\n");
                                    response = new IMAPCommandResponse(sb.toString(), false);
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("SUBSCRIBE")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                            error = true;
                        } else {
                            String mailboxName = fromModifiedUtf7(i.next());

                            Mailbox m = getMailboxByOwnerName(state.getAuthenticatedUser(), getStoreMailboxName(state.getAuthenticatedUser(), mailboxName));
                            if (m == null) {
                                response = new IMAPCommandResponse(tag + " NO Mailbox not found\r\n", false);
                            } else if (!hasAccessToMailbox(state.getAuthenticatedUser(), m)) {
                                response = new IMAPCommandResponse(tag + " NO Access denied\r\n", false);
                            } else {
                                MailboxSubscription s = new MailboxSubscription();
                                s.setMailUser(state.getAuthenticatedUser());
                                s.setMailUserEmailAddress(state.getAuthenticatedUser().getEmailAddress());
                                s.setMailboxName(m.getMailboxName());
                                Persistence.addMailboxSubscription(s);

                                response = new IMAPCommandResponse(tag + " OK Subscribed\r\n", false);
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("UNSUBSCRIBE")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                            error = true;
                        } else {
                            String mailboxName = getStoreMailboxName(state.getAuthenticatedUser(), fromModifiedUtf7(i.next()));

                            MailboxSubscription s = new MailboxSubscription();
                            s.setMailUser(state.getAuthenticatedUser());
                            s.setMailUserEmailAddress(state.getAuthenticatedUser().getEmailAddress());
                            s.setMailboxName(mailboxName);
                            Persistence.deleteMailboxSubscription(s);

                            response = new IMAPCommandResponse(tag + " OK Unsubscribed\r\n", false);
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("STATUS")) {
                    if (state.getMainState().equals(MainState.NOT_AUTHENTICATED)) {
                        response = new IMAPCommandResponse(tag + " BAD Not authenticated\r\n", false);
                        error = true;
                    } else {
                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing mailbox name\r\n", false);
                            error = true;
                        } else {
                            String givenMailboxName = i.next();
                            String mailboxName = getStoreMailboxName(state.getAuthenticatedUser(), fromModifiedUtf7(givenMailboxName));

                            if (!i.hasNext()) {
                                response = new IMAPCommandResponse(tag + " BAD Missing attributes\r\n", false);
                                error = true;
                            } else {
                                String firstAttribute = i.next();
                                if (firstAttribute.length() < 2 || firstAttribute.equals("()")) {
                                    response = new IMAPCommandResponse(tag + " BAD Empty status attribute list\r\n", false);
                                    error = true;
                                } else {
                                    Mailbox m = Persistence.getMailboxByName(mailboxName);

                                    if (m == null) {
                                        response = new IMAPCommandResponse(tag + " NO Mailbox does not exist\r\n", false);
                                    } else {

                                        LinkedList<String> attributeNames = new LinkedList<String>();
                                        attributeNames.add(firstAttribute.substring(1));
                                        if (i.hasNext()) {
                                            while (i.hasNext()) {
                                                attributeNames.add(i.next());
                                            }

                                        }
                                        String last = attributeNames.getLast();
                                        attributeNames.set(attributeNames.size() - 1, last.substring(0, last.length() - 1));

                                        StringBuilder sb = new StringBuilder();
                                        sb.append("STATUS ");
                                        sb.append(givenMailboxName);
                                        sb.append(" (");
                                        boolean first = true;
                                        String invalidAttribute = null;
                                        for (String attributeName : attributeNames) {
                                            if (!first) {
                                                sb.append(" ");
                                            } else {
                                                first = false;
                                            }

                                            if (attributeName.equalsIgnoreCase("UIDVALIDITY")) {
                                                sb.append("UIDVALIDITY ");
                                                sb.append(m.getUidValidity());
                                            } else if (attributeName.equalsIgnoreCase("UIDNEXT")) {
                                                sb.append("UIDNEXT ");
                                                sb.append(m.getNextUid());
                                            } else if (attributeName.equalsIgnoreCase("RECENT")) {
                                                sb.append("RECENT ");
                                                //sb.append(state.getRecentMessages().size());
                                                // TODO fix this
                                                sb.append(0);
                                            } else if (attributeName.equalsIgnoreCase("UNSEEN")) {
                                                int messageNumber = 0;
                                                MessageFlag flag = new MessageFlag();
                                                flag.setName("\\Seen");
                                                Iterator<MailMessage> mi = m.getMessages().iterator();
                                                for (int count = 0; count < m.getMessages().size(); count++) {
                                                    MailMessage message = mi.next();
                                                    if (!message.getFlags().contains(flag)) {
                                                        messageNumber = (count + 1);
                                                        break;
                                                    }
                                                }

                                                sb.append("UNSEEN ");
                                                sb.append(messageNumber);
                                            } else if (attributeName.equalsIgnoreCase("MESSAGES")) {
                                                sb.append("MESSAGES ");
                                                sb.append(m.getMessages().size());
                                            } else {
                                                invalidAttribute = attributeName;
                                                break;
                                            }
                                        }
                                        sb.append(")");

                                        if (invalidAttribute != null) {
                                            response = new IMAPCommandResponse(tag + " BAD Invalid attribute: " + invalidAttribute + "\r\n", false);
                                            error = true;
                                        } else {
                                            response = new IMAPCommandResponse("* " + sb.toString() + "\r\n" + tag + " OK Status command succeeded\r\n", false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("STORE")) {
                    if (!i.hasNext()) {
                        response = new IMAPCommandResponse(tag + " BAD Missing sequence set, item name, item value\r\n", false);
                        error = true;
                    } else {
                        String sequenceSetString = i.next();

                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing item name, item value\r\n", false);
                            error = true;
                        } else {
                            String itemName = i.next();

                            if (!i.hasNext()) {
                                response = new IMAPCommandResponse(tag + " BAD Missing item value\r\n", false);
                                error = true;
                            } else {
                                String firstItemValue = i.next();

                                if (!state.getMainState().equals(MainState.SELECTED)) {
                                    response = new IMAPCommandResponse(tag + " NO No mailbox selected\r\n", false);
                                    error = true;
                                } else {
                                    Set<Integer> sequenceSet = parseSequenceSet(sequenceSetString, state.getSelectedMailbox().getMessages().size());
                                    if (sequenceSet == null) {
                                        response = new IMAPCommandResponse(tag + " BAD Invalid sequence set\r\n", false);
                                        error = true;
                                    } else {
                                        int action = 0;
                                        if (itemName.startsWith("-")) {
                                            action = -1;
                                            itemName = itemName.substring(1);
                                        } else if (itemName.startsWith("+")) {
                                            action = 1;
                                            itemName = itemName.substring(1);
                                        }

                                        boolean silent = false;
                                        boolean bad = false;
                                        if (itemName.equalsIgnoreCase("FLAGS")) {
                                        } else if (itemName.equalsIgnoreCase("FLAGS.SILENT")) {
                                            silent = true;
                                        } else {
                                            bad = true;
                                        }

                                        if (bad) {
                                            response = new IMAPCommandResponse(tag + " BAD Unknown item name\r\n", false);
                                            error = true;
                                        } else if (!state.isReadWrite()) {
                                            response = new IMAPCommandResponse(tag + " NO Connection to mailbox is currently read-only", false);
                                            error = true;
                                        } else {
                                            // FLAGS look like e.g. (\Deleted $Forwarded)
                                            // so for now, we just remove the parentheses
                                            // from the first and last argument.
                                            Set<MessageFlag> flags = new HashSet<MessageFlag>();

                                            if (!firstItemValue.startsWith("(")) {
                                                // flags should be in parentheses
                                                response = new IMAPCommandResponse(tag + " BAD Flags should be in parentheses\r\n", false);
                                                error = true;
                                            } else {
                                                if (!firstItemValue.equals("()")) {
                                                    if (!i.hasNext() && !firstItemValue.endsWith(")")) {
                                                        bad = true;
                                                    } else {
                                                        if (!i.hasNext()) {
                                                            firstItemValue = firstItemValue.substring(1, firstItemValue.length() - 1);
                                                        } else {
                                                            firstItemValue = firstItemValue.substring(1);
                                                        }

                                                        MessageFlag f = new MessageFlag();
                                                        f.setName(firstItemValue);
                                                        flags.add(f);

                                                        if (!MessageFlag.isValidName(firstItemValue)) {
                                                            bad = true;
                                                        }
                                                    }
                                                    while (i.hasNext()) {
                                                        String flagName = i.next();
                                                        if (!i.hasNext() && flagName.endsWith(")")) {
                                                            flagName = flagName.substring(0, flagName.length() - 1);
                                                        } else if (!i.hasNext()) {
                                                            bad = true;
                                                        }

                                                        MessageFlag f = new MessageFlag();
                                                        f.setName(flagName);
                                                        flags.add(f);

                                                        if (!MessageFlag.isValidName(flagName)) {
                                                            bad = true;
                                                        }
                                                    }
                                                }

                                                if (bad) {
                                                    response = new IMAPCommandResponse(tag + " BAD Error parsing flags/keywords\r\n", false);
                                                    error = true;
                                                } else {
                                                    Mailbox mailbox = state.getSelectedMailbox();
                                                    StringBuilder sb = new StringBuilder();
                                                    for (Integer messageId : sequenceSet) {
                                                        MailMessage m = mailbox.getMessages().get(messageId - 1);

                                                        switch (action) {
                                                            case -1: // Remove flags
                                                                Persistence.deleteFlagsFromMessage(m, flags);
                                                                break;
                                                            case 0: // Set flags
                                                                Persistence.setMessageFlags(m, flags);
                                                                break;
                                                            case 1: // Add flags
                                                                Persistence.addFlagsToMessage(m, flags);
                                                                break;
                                                        }
                                                        MailMessagePK pk = new MailMessagePK();
                                                        pk.setMailboxUidValidity(m.getMailboxUidValidity());
                                                        pk.setMessageUid(m.getMessageUid());
                                                        MailMessage originalMessage = m;
                                                        m = Persistence.getMailMessage(pk);
                                                        if (m == null) {
                                                            // bail out
                                                            response = new IMAPCommandResponse(tag + " BAD Could not find message UIDVALIDITY=" + originalMessage.getMailboxUidValidity() + " UID=" + originalMessage.getMessageUid(), true);
                                                            return response;
                                                        }
                                                        mailbox.getMessages().set(messageId - 1, m);
                                                        if (!silent) {
                                                            if (mailbox == state.getSelectedMailbox()) {
                                                                MailboxAction ma = new MailboxAction();
                                                                ma.setActionType(MailboxAction.AsyncActionType.FETCH_FLAGS);
                                                                ma.setMessage(m);
                                                                IMAPGrizzlyFilter.doMailboxAction(state, ma, mailbox.getMailboxName());
                                                            }
                                                        }
                                                    }

                                                    sb.append(tag).append(" OK ");
                                                    switch (action) {
                                                        case -1: // remove flags
                                                            sb.append("-");
                                                            break;
                                                        case 1: // add flags
                                                            sb.append("+");
                                                            break;
                                                    }
                                                    sb.append("FLAGS completed\r\n");

                                                    response = new IMAPCommandResponse(sb.toString(), false);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("FETCH")) {
                    if (!i.hasNext()) {
                        response = new IMAPCommandResponse(tag + " BAD Missing sequence set, item name, item value\r\n", false);
                        error = true;
                    } else {
                        String sequenceSetString = i.next();

                        if (!i.hasNext()) {
                            response = new IMAPCommandResponse(tag + " BAD Missing item name, item value\r\n", false);
                            error = true;
                        } else {
                            String itemName = i.next();

                            if (!i.hasNext()) {
                                response = new IMAPCommandResponse(tag + " BAD Missing item value\r\n", false);
                                error = true;
                            } else {
                                String firstItemValue = i.next();

                                if (!state.getMainState().equals(MainState.SELECTED)) {
                                    response = new IMAPCommandResponse(tag + " NO No mailbox selected\r\n", false);
                                    error = true;
                                } else {
                                    Set<Integer> sequenceSet = parseSequenceSet(sequenceSetString, state.getSelectedMailbox().getMessages().size());
                                    if (sequenceSet == null) {
                                        response = new IMAPCommandResponse(tag + " BAD Invalid sequence set\r\n", false);
                                        error = true;
                                    } else {
                                    }
                                }
                            }
                        }
                    }
                } else {
                    response = new IMAPCommandResponse(tag + " BAD Unknown command\r\n", false);
                    error = true;
                }
            }
        }

        if (error) {
            state.setRecentBadCommands(state.getRecentBadCommands() + 1);
            if (state.getRecentBadCommands() > 15) {
                response = new IMAPCommandResponse("* BYE Too many bad commands received\r\n* BAD\r\n", true);
            }
        } else {
            state.setRecentBadCommands(0);
        }

        state.getArguments().clear();
        return response;
    }

    /**
     * Parses an IMAP sequence set. This is used by clients when specifying
     * ranges of messages.
     * @param searchTerm The sequence set search term.
     * @param numberOfMessages The number of messages in the mailbox.
     * @return A Set containing all valid message numbers in the given sequence.
     */
    static Set<Integer> parseSequenceSet(String searchTerm, int numberOfMessages) {
        Set<Integer> sequenceNumbers = new HashSet<Integer>();
        StringTokenizer ct = new StringTokenizer(searchTerm, ",");
        while (ct.hasMoreTokens()) {
            // Check if this is a single number or a range
            String token = ct.nextToken();
            int splitPos = 0;
            for (int count = 0; count < token.length(); count++) {
                if (token.charAt(count) == ':') {
                    splitPos = count;
                    break;
                }
            }

            if (splitPos == 0) {
                // No range
                try {
                    int seq = Integer.parseInt(token);
                    if (seq < 1) {
                        throw new IllegalArgumentException("Bad sequence number in sequence set: " + seq);
                    } else if (seq > numberOfMessages) {
                        throw new IllegalArgumentException("Sequence number: " + seq + " larger than number of messages: " + numberOfMessages);
                    }
                    sequenceNumbers.add(seq);
                } catch (NumberFormatException ex) {
                    throw new IllegalArgumentException("Cannot parse " + token + " as a number in sequence set", ex);
                }
            } else {
                // Range
                String token1 = token.substring(0, splitPos);
                String token2 = token.substring(splitPos + 1);
                int lowRange = 0;
                int highRange = 0;
                String currentToken = null;

                try {
                    for (int count = 0; count < 2; count++) {
                        switch (count) {
                            case 0:
                                currentToken = token1;
                                break;
                            case 1:
                                currentToken = token2;
                                break;
                        }

                        final int seq;
                        if (currentToken.equals("*")) {
                            seq = numberOfMessages;
                        } else {
                            seq = Integer.parseInt(currentToken);
                        }

                        switch (count) {
                            case 0:
                                lowRange = seq;
                                break;
                            case 1:
                                if (lowRange > seq) {
                                    highRange = lowRange;
                                    lowRange = seq;
                                } else {
                                    highRange = seq;
                                }
                                break;
                        }
                    }
                } catch (NumberFormatException ex) {
                    throw new IllegalArgumentException("Cannot parse " + currentToken + " as a number in sequence set", ex);
                }

                if (lowRange < 1) {
                    throw new IllegalArgumentException("Bad sequence number in sequence set: " + lowRange);
                } else if (highRange > numberOfMessages) {
                    throw new IllegalArgumentException("Sequence number: " + highRange + " larger than number of messages: " + numberOfMessages);
                }

                for (int count = lowRange; count <= highRange; count++) {
                    sequenceNumbers.add(count);
                }
            }
        }

        return sequenceNumbers;
    }

    /**
     * Finds mailboxes available to a user, using the specified search term.
     * @param u The user performing the search.
     * @param searchTerm The search term.
     */
    static List<Mailbox> getMailboxesBySearchTerm(MailUser u, String searchTerm) {
        // Grab INBOX and all mailboxes under it.
        List<Mailbox> mailboxes = Persistence.getAllMailboxesForUser(u);
        if (!searchTerm.equals("*")) {
            // Build a regular expression from the search term.
            StringBuilder regex = new StringBuilder();
            for (int count = 0; count < searchTerm.length(); count++) {
                char ch = searchTerm.charAt(count);

                if (ch == '[' || ch == '\\' || ch == '^' || ch == '$'
                        || ch == '.' || ch == '|' || ch == '?'
                        || ch == '+' || ch == '(' || ch == ')' || ch == '{'
                        || ch == '}') {
                    // Escape special characters
                    regex.append('\\').append(ch);
                } else if (ch == '%') {
                    // Match anything except the hierarchy delimiter
                    regex.append("[^.]*");
                } else if (ch == '*') {
                    // Match anything
                    regex.append(".*");
                } else {
                    // No special meaning, so it's part of the search term
                    regex.append(ch);
                }
            }

            // Compile the regular expression once and use it for all mailboxes.
            Pattern p = Pattern.compile(regex.toString());

            Iterator<Mailbox> i = mailboxes.iterator();
            while (i.hasNext()) {
                Mailbox m = i.next();

                // Remove mailboxes that this user has no access to.
                if (!hasAccessToMailbox(u, m)) {
                    i.remove();
                }
                // Remove mailboxes that don't match the search term.
                else if (!p.matcher(getOwnerMailboxName(u, m)).matches()) {
                    i.remove();
                }
            }
        }

        return mailboxes;
    }

    /**
     * Converts a string to modified UTF-7 (RFC 3501).
     * @param string The string to convert.
     * @return A string encoded with modified UTF-7 that can be represented
     * using US-ASCII.
     */
    static String toModifiedUtf7(String string) {
        ByteBuffer bb = modifiedUtf7Charset.encode(string);
        return new String(bb.array(), bb.arrayOffset(), bb.limit());
    }

    /**
     * Converts a modified UTF-7 encoded string back to a regular Java string.
     * @param string The modified UTF-7 string (RFC 3501).
     * @return The decoded string.
     */
    static String fromModifiedUtf7(String string) {
        ByteBuffer bb = ByteBuffer.wrap(string.getBytes());
        return modifiedUtf7Charset.decode(bb).toString();
    }

    /**
     * Checks if the given string contains 8 bit characters.
     * @param string The string to check for 8 bit characters.
     * @return Whether the given string contains 8 bit characters.
     */
    static boolean contains8Bit(String string) {
        for (int count = 0; count < string.length(); count++) {
            if (string.charAt(count) > 127) {
                return true;
            }
        }

        return false;
    }

    /**
     * Fetches the list of capabilities that the IMAP server supports. These
     * tokens are separated by spaces so that the string may be sent as-is
     * to the client. Capabilities may change once the user has authenticated.
     * Usually the LOGINDISABLED capability will be enabled and all other
     * authentication related capabilities will become disabled once
     * authenticated.
     * @param authenticated Whether the client has authenticated and logged into
     * the server.
     * @return The list of capabilities, separated by spaces.
     */
    static String getCapabilities(final boolean authenticated) {
        String capabilities = "IMAP4rev1 ";
        if (authenticated)
            capabilities += "LOGINDISABLED ";
        capabilities += "LITERAL+ NAMESPACE UNSELECT CHILDREN AUTH=CRAM-MD5 SASL-IR";

        return capabilities;
    }

    /**
     * Checks whether the given user has access to the given mailbox.
     * @param user The user to check.
     * @param mailboxName The mailbox to check.
     * @return <code>true</code> if they have access to the mailbox, <code>false</code>
     * otherwise.
     */
    static boolean hasAccessToMailbox(MailUser user, Mailbox mailbox) {
        // TODO add permissions logic here. Be sure to support new mailboxes
        // that haven't yet been persisted. i.e. mailbox.getUidValidity() == 0
        if (mailbox.getMailboxName().equalsIgnoreCase("user." + user.getEmailAddress())) {
            // User's INBOX
            return true;
        } else if (mailbox.getMailboxName().toLowerCase().startsWith("user." + user.getEmailAddress().toLowerCase() + ".")) {
            // An inferior of the user's INBOX
            return true;
        } else {
            return false;
        }
    }

    /**
     * Attempts to find a mailbox by the name known by its owner. For example,
     * <b>INBOX.Drafts</b> may be used, which will translate to
     * <b>user.&lt;username&gt;.Drafts</b>
     * @param mailboxName The name of the mailbox, as given by the client.
     * @return A Mailbox instance, or <code>null</code> if it
     * could not be found.
     */
    static Mailbox getMailboxByOwnerName(MailUser user, String mailboxName) {
        String storeName = getStoreMailboxName(user, mailboxName);
        return Persistence.getMailboxByName(storeName);
    }

    /**
     * Attempts to find a mailbox by the name known by the server. For example,
     * <b>user.&lt;username&gt;.Drafts</b>.
     * @param storeMailboxName The name of the mailbox, as known by the server.
     * @return A Mailbox instance, or <code>null</code> if it could not be
     * found.
     */
    static Mailbox getMailboxByStoreName(String storeMailboxName) {
        return Persistence.getMailboxByName(storeMailboxName);
    }

    /**
     * Gets the mailbox name as the owner knows it. For example, if the real
     * mailbox name, returned by {@link #getMailboxName()} is user.test@example.com,
     * the owner name will be INBOX.
     * @param u The owner.
     * @param m The mailbox.
     * @return The mailbox name as the owner knows it.
     */
    static String getOwnerMailboxName(MailUser u, Mailbox m) {
        int dotPos = m.getMailboxName().indexOf('.', 5 + u.getEmailAddress().length());
        if (dotPos == -1) {
            return "INBOX";
        } else {
            return "INBOX." + m.getMailboxName().substring(dotPos + 1);
        }
    }

    /**
     * Translates a mailbox name from the name known by its owner to the
     * name that is used when storing the mailbox.
     * @param user The owner of the mailbox.
     * @param mailboxName The name that the owner would use.
     * @return The name that the mailbox would use internally.
     */
    static String getStoreMailboxName(MailUser user, String mailboxName) {
        if (mailboxName.equalsIgnoreCase("INBOX")) {
            mailboxName = "user." + user.getEmailAddress();
        } else if (mailboxName.toUpperCase().startsWith("INBOX.")) {
            mailboxName = "user." + user.getEmailAddress() + "." + mailboxName.substring(6);
        }

        return mailboxName;
    }

    /**
     * Parses a standard literal token or a literal plus token. For example,
     * {4} (standard token) or {10+} (literal plus token). This routine doesn't
     * actually check if there are braces in the token, so this should be
     * checked before this method is called.
     * @param literal The literal token.
     * @return The parsed number of characters inside the braces, or -1 if
     * there is no valid number inside the braces.
     */
    static int parseLiteral(String literal) {
        boolean literalPlus = literal.charAt(literal.length() - 2) == '+';
        String literalCharactersString = literal.substring(1, literal.length() - (literalPlus ? 2 : 1));
        int literalCharacters = -1;
        try {
            literalCharacters = Integer.parseInt(literalCharactersString);
        } catch (NumberFormatException ex) {
        }

        if (literalCharacters < 0) {
            literalCharacters = -1;
        }

        return literalCharacters;
    }
}
