/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.message;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * A Multipart MIME part which holds other MIME parts.
 * @author Ben Upsavs
 */
public class Multipart extends MimePart implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<MimePart> parts = new LinkedList<MimePart>();
    private String mimeBoundary;

    /** {@inheritDoc} */
    @Override
    public boolean isMultipart() {
        return true;
    }

    /**
     * Gets the list of parts within this multipart MIME part.
     * @return The list of parts.
     */
    public List<MimePart> getParts() {
        return Collections.unmodifiableList(parts);
    }

    /**
     * Sets the list of parts within this multipart MIME part.
     * @param parts The list of parts.
     */
    public void setParts(List<MimePart> parts) {
        this.parts.clear();
        this.parts.addAll(parts);
    }

    /**
     * Adds a part to this multipart MIME part.
     * @param part The part to add.
     */
    public void addPart(MimePart part) {
        getParts().add(part);
    }

    /** {@inheritDoc} */
    @Override
    public long getContentLength() {
        long totalLength = 0;
        for (MimePart part : getParts())
            totalLength += part.getContentLength();

        return totalLength;
    }

    /** {@inheritDoc} */
    @Override
    public long getNumberOfLines() {
        long totalLines = 0;
        for (MimePart part : getParts())
            totalLines += part.getNumberOfLines();

        return totalLines;
    }

    /**
     * Gets the MIME boundary for this MIME multipart.
     * @return The MIME boundary.
     */
    public String getMimeBoundary() {
        return mimeBoundary;
    }

    /**
     * Sets the MIME boundary for this MIME multipart.
     * @param mimeBoundary The MIME boundary.
     */
    public void setMimeBoundary(String mimeBoundary) {
        this.mimeBoundary = mimeBoundary;
    }
}
