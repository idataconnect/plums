/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.message;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * An object representing a MIME part of an email message. This may either
 * be a {@link Multipart} or a {@link LeafPart}.
 * @author Ben Upsavs
 */
public abstract class MimePart implements Serializable {
    protected String majorType;
    protected String minorType;
    protected long messageOffset;

    /**
     * Checks if this MIME part is a multipart or a leaf part.
     * @return <code>true</code> if this part is a multipart MIME part,
     * <code>false</code> otherwise.
     */
    public abstract boolean isMultipart();

    /**
     * Gets the length of the contents of this MIME part.
     * @return The length of the contents of this MIME part.
     */
    public abstract long getContentLength();

    /**
     * Gets the number of lines in this MIME part.
     * @return The number of lines in this MIME part.
     */
    public abstract long getNumberOfLines();

    /**
     * Gets the major type of the content type. For example, if the content
     * type is <tt>text/plain</tt>, the major type would be <tt>text</tt>.
     * @return The major type of the content type.
     */
    public String getMajorType() {
        return majorType;
    }

    /**
     * Sets the major type of the content type. For example, if the content
     * type is <tt>text/plain</tt>, the major type would be <tt>text</tt>.
     * @param majorType The major type of the content type.
     */
    public void setMajorType(String majorType) {
        this.majorType = majorType;
    }

    /**
     * Gets the minor type of the content type. For example, if the content
     * type is <tt>text/plain</tt>, the minor type would be <tt>plain</tt>.
     * @return The minor type of the content type.
     */
    public String getMinorType() {
        return minorType;
    }

    /**
     * Sets the minor type of the content type. For example, if the content
     * type is <tt>text/plain</tt>, the minor type would be <tt>plain</tt>.
     * @param minorType The minor type of the content type.
     */
    public void setMinorType(String minorType) {
        this.minorType = minorType;
    }

    /**
     * Gets the content type as a single string. For example,
     * a plain text content type would be <tt>text/plain</tt>. This is
     * equivalent to calling <code>getMajorType() + "/" + getMinorType()</code>.
     * @return The content type.
     */
    public String getContentType() {
        return getMajorType() + "/" + getMinorType();
    }

    /**
     * Sets the content type for this MIME part. The parameter
     * <code>contentType</code> is parsed and the major and minor types
     * are extracted and set individually. Therefore, there is a slight
     * performance gain by using <code>setMajorType()</code> and
     * <code>setMinorType()</code> rather than this method.
     * @param contentType The content type to set for this MIME part.
     * @throws IllegalArgumentException If the content type is not in a valid
     * form.
     */
    public void setContentType(String contentType) {
        StringTokenizer st = new StringTokenizer(contentType, "/");
        String token1 = st.nextToken();
        boolean bad = false;
        if (!st.hasMoreTokens())
            bad = true;
        String token2 = null;
        if (!bad)
            token2 = st.nextToken();
        if (st.hasMoreTokens())
            bad = true;
        if (bad)
            throw new IllegalArgumentException("Cannot parse content type for MIME part: " + contentType);
        setMajorType(token1);
        setMinorType(token2);
    }

    /**
     * Gets the position in the message that this MIME part occurs.
     * @return The position in the message that this MIME part occurs.
     */
    public long getMessageOffset() {
        return messageOffset;
    }

    /**
     * Sets the position in the message that this MIME part occurs.
     * @param messageOffset The position in the message that this MIME part
     * occurs.
     */
    public void setMessageOffset(long messageOffset) {
        this.messageOffset = messageOffset;
    }
}
