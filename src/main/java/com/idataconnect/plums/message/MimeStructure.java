/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.message;

import com.idataconnect.plums.entity.MailMessage;
import java.io.Serializable;

/**
 * An object representing the MIME structure of an email message.
 * @author Ben Upsavs
 */
public class MimeStructure implements Serializable {

    private static final long serialVersionUID = 1L;

    private MimePart rootMimePart;
    private int parseTimeMillis;
    private MailMessage message;
    private boolean mimeMessage;

    /**
     * Whether or not the root part is a multipart MIME part. This is equivalent
     * to <code>getRootMimePart().isMultipart()</code>.
     * @return Whether the root part is a multipart MIME part.
     */
    public boolean isMultipart() {
        return getRootMimePart().isMultipart();
    }

    /**
     * Gets the root MIME part of the message. If the message is not a MIME
     * message, the root part will be a single leaf part.
     * @return The root MIME part of the message.
     */
    public MimePart getRootMimePart() {
        return rootMimePart;
    }

    /**
     * Sets the root MIME part of the message.
     * @param rootMimePart The root MIME part of the message.
     */
    public void setRootMimePart(MimePart rootMimePart) {
        this.rootMimePart = rootMimePart;
    }

    /**
     * Getter for the parse time in milliseconds.
     * @return The parse time in milliseconds.
     */
    public int getParseTimeMillis() {
        return parseTimeMillis;
    }

    /**
     * Setter for the parse time in milliseconds.
     * @param parseTimeMillis The parse time in milliseconds.
     */
    public void setParseTimeMillis(int parseTimeMillis) {
        this.parseTimeMillis = parseTimeMillis;
    }

    /**
     * Getter for the mail message which was parsed.
     * @return The mail message which was parsed.
     */
    public MailMessage getMessage() {
        return message;
    }

    /**
     * Setter for the mail message which was parsed.
     * @param message The mail message which was parsed.
     */
    public void setMessage(MailMessage message) {
        this.message = message;
    }

    /**
     * Getter for whether or not the message is a MIME message. The message is
     * considered a MIME message if it has a MIME-Version header.
     * @return Whether or not the message is a MIME message.
     */
    public boolean isMimeMessage() {
        return mimeMessage;
    }

    /**
     * Setter for whether or not the message is a MIME message. The message is
     * considered a MIME message if it has a MIME-Version header.
     * @param mimeMessage Whether or not the message is a MIME message.
     */
    public void setMimeMessage(boolean mimeMessage) {
        this.mimeMessage = mimeMessage;
    }
}
