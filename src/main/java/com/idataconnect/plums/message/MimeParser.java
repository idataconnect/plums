/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.message;

import com.idataconnect.plums.entity.MailMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MIME parsing routines for email messages.
 * @author Ben Upsavs
 */
public class MimeParser {

    private MimeParser() {
    }

    /**
     * Parses a message and returns its MIME-IMB structure. If the message is
     * not a MIME message, or an error occurs while parsing, a default
     * plain text single part MIME structure will be returned. This routine
     * should be as robust as possible, with regards to parsing invalid
     * MIME content, and being able to always fall back to a plain text leaf
     * part when an error occurs during parsing.
     * @param message The message to parse.
     * @return The MIME-IMB structure.
     */
    public static MimeStructure parse(MailMessage message) {
        try {
            FileInputStream is = message.getInputStream();

            MimePart rootPart = parseMultipart(is.getChannel(),
                    null, message.getMessageSize());
            MimeStructure mimeStructure = new MimeStructure();
            mimeStructure.setRootMimePart(rootPart);

            return mimeStructure;
        } catch (Exception ex) {
            Logger.getLogger(MimeParser.class.getName()).log(Level.WARNING,
                    "Error while performing MIME parsing", ex);

            MimeStructure defaultStructure = new MimeStructure();
            LeafPart leafPart = new LeafPart();
            leafPart.setContentType("text/plain");
            defaultStructure.setRootMimePart(leafPart);

            return defaultStructure;
        }
    }

    /**
     * Parses a MIME message part, which is potentially a multipart type, and
     * returns a {@link MimePart} subclass instance, depending on whether the
     * parsed part is a multipart or a leaf part.
     * @param channel The <code>FileChannel</code> which may be used to
     * access the email message.
     * @param parentBoundary The boundary of the parent multipart, or
     * <code>null</code> if there is none, because this is the root part.
     * @param partSize The length of the part if known, or <code>0</code>
     * if unknown.
     * @return An instance of a subclass of <code>MimePart</code>.
     * @throws IOException If an I/O error occurs while reading the email
     * message.
     */
    protected static MimePart parseMultipart(FileChannel channel,
            String parentBoundary, long partSize)
            throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(8192);
        StringBuilder headers = new StringBuilder(8192);
        boolean newlinePrevious = false;
        int bytesRead = 0;
        long channelStartPosition = channel.position();

        // Read the message headers using NIO
        headerFetch:
        while (buf.hasRemaining() && channel.read(buf) != -1) {
            if (buf.position() > 0) {
                buf.flip();
                while (buf.hasRemaining()) {
                    char c = (char) buf.get();
                    switch (c) {
                        case '\r':
                            // ignore
                            break;
                        case '\n':
                            // end of line
                            headers.append('\r').append('\n');
                            if (newlinePrevious) {
                                break headerFetch;
                            }
                            newlinePrevious = true;
                            break;
                        default:
                            headers.append(c);
                            newlinePrevious = false;
                            break;
                    }

                    if (++bytesRead == 10240) {
                        // Headers too large; bail out with leaf part
                        LeafPart part = new LeafPart();
                        if (partSize != 0) {
                            part.setContentLength(partSize);
                        }
                        part.setMessageOffset(channelStartPosition);
                        part.setContentType("text/plain");
                        return part;
                    }
                }
                buf.clear();
            }
        }

        // TODO finish this
        return null;
    }
}
