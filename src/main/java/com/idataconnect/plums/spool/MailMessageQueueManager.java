/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.spool;

import com.idataconnect.plums.Persistence;
import com.idataconnect.plums.entity.Header;
import com.idataconnect.plums.entity.Mailbox;
import com.idataconnect.plums.entity.HostedDomain;
import com.idataconnect.plums.entity.MailMessage;
import com.idataconnect.plums.entity.MailMessageQueueItem;
import com.idataconnect.plums.entity.MailUser;
import com.idataconnect.plums.entity.SMTPDeliveryLogEntry;
import com.idataconnect.plums.network.DNSResolver;
import com.idataconnect.plums.network.MailServerRef;
import com.idataconnect.plums.smtp.SMTPConnectionState;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Mail message queue manager. <code>init()</code> should be called once
 * before using this class. The global config value
 * <code>mailMessageQueueThreadPoolSize</code> is used to specify the number
 * of threads used by the queue runner.
 * @author Ben Upsavs
 */
public class MailMessageQueueManager {

    private static ScheduledThreadPoolExecutor threadPool;
    private static final Set<Integer> queueLocks = new HashSet<Integer>();
    private static final Set<Integer> queueRunLock = new HashSet<Integer>();
    private static Timer queueTimer;
    private static final Logger log = Logger.getLogger(MailMessageQueueManager.class.getName());

    public static void init() {
        // Initialize the thread pool
        threadPool = new ScheduledThreadPoolExecutor(
                Integer.parseInt(Persistence.getGlobalConfigValue("mailMessageQueueThreadPoolSize", "4")),
                new ThreadFactory() {

                    public Thread newThread(Runnable r) {
                        Thread t = new Thread(r, "Pooled spool worker");
                        t.setDaemon(true);
                        return t;
                    }
                });

        // Start the queue timer
        queueTimer = new Timer(true);
        queueTimer.schedule(new TimerTask() {

            public void run() {
                Thread.currentThread().setName("Queue runner launcher");
                try {
                    queueRun();
                } catch (Exception ex) {
                    log.log(Level.WARNING, "Encountered an unexpected error during a queue run", ex);
                }
            }
        }, 10000, Integer.parseInt(Persistence.getGlobalConfigValue("queueRunInterval", "60000")));
    }

    public static void queueRun() {
        // Check if a queue run is already in progress (double checked locking)
        // TODO implement better lock
        if (queueRunLock.contains(new Integer(1)))
            return;

        synchronized (queueRunLock) {
            // Check again inside synchronized block
            if (queueRunLock.contains(new Integer(1)))
                return;
            // Don't let others start while we are running.
            queueRunLock.add(new Integer(1));
        }

        log.fine("Starting queue run");
        try {
            int queueChunkSize = Integer.parseInt(Persistence.getGlobalConfigValue("mailQueueProcessChunk", "1000"));
            int currentOffset = 0;
            int numberAttempted = 0;

            List<MailMessageQueueItem> queueItems;
            do {
                queueItems = Persistence.getMailMessageQueueItems(currentOffset, queueChunkSize);

                for (final MailMessageQueueItem queueItem : queueItems) {
                    // TODO Check if this ease-off algorithm is okay for
                    // production use.

                    // Don't process items that are still entering the queue.
                    if (queueItem.isIncoming())
                        continue;
                    // Don't process queue items that have been delivered and deleted
                    else if (queueItem.isDelivered())
                        continue;

                    boolean attemptDelivery = false;
                    if (queueItem.getNumberOfAttempts() == 0) {
                        attemptDelivery = true;
                    } else if (queueItem.getNumberOfAttempts() <= 10) {
                        // Ease off
                        if (queueItem.getLastAttemptTime().getTime() < (System.currentTimeMillis() - 60000 * queueItem.getNumberOfAttempts()))
                            attemptDelivery = true;
                    } else {
                        // number of attempts already more than 10
                        // Ease off even more
                        if (queueItem.getLastAttemptTime().getTime() < (System.currentTimeMillis() - 1000 * 60 * 60 * 24 * (0.9 + queueItem.getNumberOfAttempts() / 10)))
                            attemptDelivery = true;
                    }

                    if (attemptDelivery) {
                        numberAttempted++;
                        attemptDelivery(queueItem.getQueueId());
                    }
                }

                currentOffset += queueChunkSize;
            } while (queueItems.size() > 0);

            if (numberAttempted > 0)
                log.log(Level.INFO, "Queue run launched {0} delivery attempts", numberAttempted);
        } finally {
            // Make sure the lock is always removed.
            queueRunLock.remove(new Integer(1));
        }
    }

    /**
     * Adds a mail message to the queue, and provides a list of files that
     * need to have this message's content. The caller should subsequently
     * write the message data to the files.
     * @param state The {@link SMTPConnectionState} object used by the SMTP
     * server component.
     */
    public static List<MailMessageQueueItem> queueMessage(SMTPConnectionState state) throws IOException {
        List<MailMessageQueueItem> queueItems = new LinkedList<MailMessageQueueItem>();

        // Queue message to recipients.
        for (String emailAddress : state.getRcptTo()) {
            // Create a new SMTP log entry
            final SMTPDeliveryLogEntry l = Persistence.addSMTPLogEntry();
            l.setLocalHostname(state.getServerInstance().getNetworkAttributes().getHostname());
            l.setRemoteHostname(state.getRemoteHost().getHostName());
            l.setRemoteIp(state.getRemoteHost().getHostAddress());
            l.setDeliveryStatus("Pre-Queue");
            Persistence.updateSMTPLogEntry(l);

            if (emailAddress.equals("postmaster")) {
                // Special postmaster case, required by RFC
                // TODO translate postmaster into a configured address
            }

            MailMessageQueueItem qi = new MailMessageQueueItem();
            qi.setNumberOfAttempts(0);
            qi.setQueueId(l.getSmtpId());
            qi.setQueueTime(new Date());
            qi.setReturnPath(state.getMailFrom());
            qi.setRecipient(emailAddress);
            qi.setServerInstanceHostname(state.getServerInstance().getNetworkAttributes().getHostname());
            Persistence.addMailMessageQueueItem(qi);

            queueItems.add(qi);

            l.setDeliveryStatus("Queued");
            Persistence.updateSMTPLogEntry(l);
        }

        return queueItems;
    }

    /**
     * Uses the mail queue thread pool to attempt delivery of the requested
     * queued message. This method does not block.
     * @param queueId The queue ID of the message.
     */
    public static void attemptDelivery(final int queueId) {
        threadPool.execute(new Runnable() {

            public void run() {
                attemptDeliveryInCurrentThread(queueId);
            }
        });
    }

    /**
     * Attempts to deliver a message from the queue, in the current thread.
     * Since this method will block during delivery, {@link #attemptDelivery(int)}
     * should normally be used instead.
     * @param queueId The queue ID of the message.
     */
    public static void attemptDeliveryInCurrentThread(int queueId) {
        Integer i = new Integer(queueId);

        // Attempt to reduce synchronization overhead by using double checked
        // locking
        if (queueLocks.contains(1))
            return;
        synchronized (queueLocks) {
            // Check if this message is already being delivered
            if (queueLocks.contains(i))
                return;
            else
                queueLocks.add(i);
        }

        try {
            MailMessageQueueItem q = Persistence.getMailMessageQueueItem(queueId);
            if (q == null || q.isDelivered())
                return;

            // TODO implement an alias expansion feature. It should remove
            // duplicates.

            StringTokenizer st = new StringTokenizer(q.getRecipient(), "@");
            st.nextToken();
            String domain = st.nextToken();
            HostedDomain d = Persistence.getHostedDomain(domain);
            if (d != null) {
                // Hosted locally
                MailUser u = Persistence.getUserByEmailAddress(q.getRecipient());
                if (u != null) {
                    // Deliver the message to the local user's inbox

                    Mailbox f = Persistence.getMailboxByName("user." + u.getEmailAddress());
                    if (f != null) {
                        try {
                            // Copy the message from the queue file into the
                            // message file and cache the headers into the
                            // database.

                            MailMessage m = Persistence.addMailMessage(f.getMailboxName());

                            FileInputStream fis = q.getInputStream();
                            FileChannel ic = fis.getChannel();

                            FileOutputStream fos = m.getOutputStream();
                            FileChannel oc = fos.getChannel();

                            // Add Return-Path header since this is the
                            // final destination.
                            String returnPathHeader = "Return-Path: <" +
                                    q.getReturnPath() + ">\r\n";

                            // Add the return-path header to the database.
                            Header h = new Header();
                            h.setHeaderName("Return-Path");
                            h.setHeaderValue("<" + q.getReturnPath() + ">");
                            h.setMessage(m);
                            h.setHeaderSequence(0);
                            m.getHeaders().add(h);

                            ByteBuffer bb = ByteBuffer.allocate(8192);

                            // Write the newly added return-path header.
                            bb.put(returnPathHeader.getBytes());
                            bb.flip();
                            oc.write(bb);

                            // Find each header, unfolding lines if necessary,
                            // and add each one to the database.
                            StringBuilder sb = new StringBuilder();
                            h = new Header();
                            int searchState = 0;
                            int headerSequence = 0;

                            while (searchState != 9) {
                                bb.clear();
                                switch (ic.read(bb)) {
                                    case -1: // EOF
                                        searchState = 9;
                                    case 0:
                                        break;
                                    default:
                                        // We got some chars
                                        bb.flip();
                                        int origPos = bb.position();
                                        int origLimit = bb.limit();
                                        while (bb.hasRemaining()) {
                                            char c = (char) bb.get();

                                            if (searchState == 2) {
                                                if (c == '\n')
                                                    continue;
                                                else if (c == ' ' || c == '\t') // Continuation
                                                {
                                                    searchState = 3;
                                                    continue;
                                                } else if (c != '\r') {
                                                    // End of header
                                                    if (h.getHeaderName() == null)
                                                        h.setHeaderName("");
                                                    h.setHeaderValue(sb.toString());
                                                    h.setHeaderSequence(++headerSequence);
                                                    h.setMessage(m);
                                                    m.getHeaders().add(h);
                                                    h = new Header();
                                                    sb.setLength(0);
                                                    searchState = 0;
                                                }
                                            }

                                            switch (c) {
                                                case '\r':
                                                    if (h.getHeaderName() == null && sb.length() == 0) {
                                                        // end of headers
                                                        searchState = 9;
                                                    } else {
                                                        // End of line
                                                    }
                                                    break;
                                                case ':':
                                                    if (searchState == 0) {
                                                        h.setHeaderName(sb.toString());
                                                        sb.setLength(0);
                                                        searchState = 1;
                                                        break;
                                                    }
                                                case ' ':
                                                    if (searchState == 3 && c == ' ')
                                                        break;
                                                    else if (searchState == 1 && sb.length() == 0)
                                                        break;
                                                default:
                                                    if (searchState == 3) {
                                                        sb.append(' ');
                                                        searchState = 1;
                                                    }
                                                    sb.append(c);
                                                    break;
                                                case '\n':
                                                    searchState = 2;
                                                    break;
                                                case '\t':
                                            }
                                        }

                                        // reset and flush buffer
                                        bb.position(origPos);
                                        bb.limit(origLimit);
                                        while (bb.hasRemaining())
                                            oc.write(bb);
                                }
                            }

                            bb.clear();
                            while (ic.read(bb) != -1) {
                                bb.flip();
                                oc.write(bb);
                                bb.clear();
                            }
                            fis.close();
                            fos.close();

                            m.loadMessageSize();
                            m.setIncoming(false);
                            Persistence.updateMailMessage(m);

                            SMTPDeliveryLogEntry l = Persistence.getSMTPLogEntry(queueId);
                            if (l != null) {
                                l.setDeliveryStatus("Delivered locally");
                                l.setDelivered(true);
                                Persistence.updateSMTPLogEntry(l);
                            }

                            log.log(Level.INFO, "{0}: Delivered from {1} to {2}",
                                    new Object[] {q.getQueueIdForDisplay(),
                                    q.getReturnPath(), q.getRecipient()});

                            Persistence.deleteMailMessageQueueItem(q.getQueueId());
                        } catch (Exception ex) {
                            // An error occured during local delivery.
                            log.log(Level.SEVERE, "Encountered an error while attempting to deliver a message locally", ex);
                            q.setLastAttemptTime(new java.util.Date());
                            q.setLastErrorMessage("Internal delivery error: " + ex.getMessage());
                            q.setNumberOfAttempts(q.getNumberOfAttempts() + 1);
                            Persistence.updateMailMessageQueueItem(q);

                            // TODO notify postmaster
                        }
                    } else {
                        // Something went wrong finding the inbox
                        q.setLastAttemptTime(new java.util.Date());
                        q.setLastErrorMessage("INBOX for " + q.getRecipient() + " does not exist");
                        q.setNumberOfAttempts(q.getNumberOfAttempts() + 1);
                        Persistence.updateMailMessageQueueItem(q);

                        // TODO notify postmaster
                    }
                } else {
                    // User does not exist. This shouldn't normally happen. But
                    // it's possible that the user was deleted right before this
                    // delivery attempt.

                    // TODO send DSN

                    // Delete message from queue
                    Persistence.deleteMailMessageQueueItem(q.getQueueId());
                    SMTPDeliveryLogEntry l = Persistence.getSMTPLogEntry(queueId);
                    if (l != null) {
                        l.setDeliveryStatus("Failed");
                        Persistence.updateSMTPLogEntry(l);
                    }
                }
            } else // Must relay
            {
                attemptRelay(q);
            }
        } finally {
            // Make sure the lock is always removed.
            queueLocks.remove(i);
        }
    }

    /**
     * Attempts to relay a message from the mail queue to a remote SMTP server.
     * @param q The message in the queue.
     */
    public static void attemptRelay(MailMessageQueueItem q) {
        FileInputStream fis = null;
        try {
            // Cache the headers
            fis = q.getInputStream();

            StringBuilder sb = new StringBuilder();

            boolean lastCharWasNewline = false;
            while (true) {
                byte b = (byte) fis.read();

                if (b == -1) {
                    log.log(Level.WARNING, 
                            "{0}: End of message encountered before headers ended.",
                            q.getQueueIdForDisplay());
                    return;
                }

                // Ignore Carriage return
                if (b == '\r')
                    continue;
                else if (b == '\n') {
                    if (lastCharWasNewline) {
                        break;
                    } else {
                        sb.append("\r\n");
                        lastCharWasNewline = true;
                    }
                } else {
                    sb.append(b);
                    lastCharWasNewline = false;
                }
            }
            fis.close();

            String headers = sb.toString();
            sb = null;

            // Count the number of received headers, in order to detect mail loops.
            // Someone might want to implement a better loop detection algorithm.
            int receivedHeadersLimit = Integer.parseInt(Persistence.getGlobalConfigValue("receivedHeadersLimit", "100"));
            int position = 0;
            int receivedCount;
            for (receivedCount = 0;
                    (position != -1) && (receivedCount < receivedHeadersLimit);
                    receivedCount++) {
                position = headers.indexOf("\nReceived:", position + 1);
            }

            if (receivedCount == receivedHeadersLimit) {
                // TODO send DSN

                SMTPDeliveryLogEntry l = Persistence.getSMTPLogEntry(q.getQueueId());
                if (l != null) {
                    log.log(Level.INFO, "{0}: Mail loop detected.", q.getQueueIdForDisplay());
                    l.setDeliveryStatus("Mail loop detected.");
                    Persistence.updateSMTPLogEntry(l);
                }

                Persistence.deleteMailMessageQueueItem(q.getQueueId());
            } else {
                // No mail loop detected; attempt to relay.

                log.log(Level.INFO, "{0}: Attempting to relay to {1}", new Object[] {q.getQueueIdForDisplay(), q.getRecipient()});
                String domainName = q.getRecipient().substring(q.getRecipient().lastIndexOf('@') + 1);
                SortedSet<MailServerRef> servers = DNSResolver.getMailServerRefs(domainName);
                Iterator<MailServerRef> i = servers.iterator();
                boolean done = false;
                SMTPRelayResult result = null;
                while (!done && i.hasNext()) {
                    MailServerRef mailServerRef = i.next();
                    String ipAddress = DNSResolver.resolveHostname(mailServerRef.getHostname());
                    if (ipAddress != null) {
                        // The remote mail server's IP address was able to be
                        // resolved. Now we will try to relay.
                        log.log(Level.INFO, "{0}: Attempting relay to {1} for {2}",
                                new Object[] {q.getQueueIdForDisplay(),
                                ipAddress, q.getRecipient()});
                        result = SMTPRelay.relay(q, ipAddress);
                        SMTPDeliveryLogEntry l;
                        switch (result.getOutcome()) {
                            case SUCCESS:
                                log.log(Level.INFO, "{0}: Relay successful to {1} ({2})",
                                        new Object[] {q.getRecipient(), result.getResultMessage()});
                                Persistence.deleteMailMessageQueueItem(q.getQueueId());
                                l = Persistence.getSMTPLogEntry(q.getQueueId());
                                if (l != null) {
                                    l.setDelivered(true);
                                    l.setDeliveryStatus("Delivered to " + ipAddress);
                                    Persistence.updateSMTPLogEntry(l);
                                }
                                done = true;
                                break;
                            case TEMP_FAIL_OTHER:
                                log.log(Level.INFO, "{0}: Temp fail due to reason other than server response: {1}",
                                        new Object[] {q.getQueueIdForDisplay(), result.getResultMessage()});
                                break;
                            case TEMP_FAIL_SERVER:
                                log.log(Level.INFO, "{0}: Temp fail due to server response: {1}",
                                        new Object[] {q.getQueueIdForDisplay(), result.getResultMessage()});
                                break;
                            case PERM_FAIL:
                                log.log(Level.INFO, "{0}: Perm fail: {1}",
                                        new Object[] {q.getQueueIdForDisplay(), result.getResultMessage()});
                                Persistence.deleteMailMessageQueueItem(q.getQueueId());
                                l = Persistence.getSMTPLogEntry(q.getQueueId());
                                if (l != null) {
                                    l.setDeliveryStatus("Perm fail: " + result.getResultMessage());
                                    Persistence.updateSMTPLogEntry(l);
                                }
                                done = true;
                                break;
                        }
                    } else {
                        log.log(Level.INFO, "{0}: Unable to resolve {1}",
                                new Object[] {q.getQueueIdForDisplay(),
                                mailServerRef.getHostname()});
                    }
                }

                if (!done && (result != null) &&
                        !result.getOutcome().equals(SMTPRelayResult.Outcome.PERM_FAIL)) {
                    // Message temp failed on all remote servers.
                    q.setLastAttemptTime(new java.util.Date());
                    q.setLastErrorMessage(result.getResultMessage());
                    q.setNumberOfAttempts(q.getNumberOfAttempts() + 1);
                    Persistence.updateMailMessageQueueItem(q);
                }
            }

            fis.close();
        } catch (IOException ex) {
            log.log(Level.WARNING, "Error while attempting to relay", ex);
        } finally {
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException ex) {}
        }
    }
}
