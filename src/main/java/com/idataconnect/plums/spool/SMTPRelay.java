/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.spool;

import com.idataconnect.plums.entity.MailMessageQueueItem;
import com.idataconnect.plums.spool.SMTPRelayResult.Outcome;
import com.sun.grizzly.Controller;
import com.sun.grizzly.Controller.Protocol;
import com.sun.grizzly.ControllerStateListener;
import com.sun.grizzly.TCPConnectorHandler;
import com.sun.grizzly.TCPSelectorHandler;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SMTP relay functionality.
 * @author Ben Upsavs
 */
public class SMTPRelay {

    /** A set of relay ids that are currently in progress. */
    static final Set<Integer> relayLocks = new HashSet<Integer>(32);
    private static TCPSelectorHandler selectorHandler;
    private static Controller controller;
    private static final Logger log = Logger.getLogger(SMTPRelay.class.getName());

    /**
     * Attempt to relay a queued message to the mail server specified
     * by <code>ipAddress</code>. It is incorrect to place a hostname in
     * the <code>ipAddress</code> parameter since the cache time will be
     * undefined.
     * @param qi The queued message to relay.
     * @param ipAddress The IP address of the remote mail server.
     * @return The result of the relay, or <code>null</code> if the relay
     * was not attempted.
     */
    public static SMTPRelayResult relay(MailMessageQueueItem qi, String ipAddress) {
        // Make sure we don't relay the same message twice.
        Integer id = new Integer(qi.getQueueId());
        
        // Attempt to avoid some overhead of synchronization by using
        // double checked locking.
        if (relayLocks.contains(id))
            return null;
        synchronized (relayLocks) {
            if (relayLocks.contains(id))
                return null;
            else
                relayLocks.add(id);
        }

        SMTPRelayResult result = new SMTPRelayResult();

        TCPConnectorHandler connectorHandler = null;
        try {
            if (controller == null) {
                // Need to start the Grizzly client controller.
                startGrizzlyController();
            }

            connectorHandler = (TCPConnectorHandler) controller.acquireConnectorHandler(Protocol.TCP);
            connectorHandler.connect(new InetSocketAddress(ipAddress, 25));
            log.log(Level.FINE, "{0}: Connected to {1}:25",
                    new Object[] {qi.getQueueIdForDisplay(), ipAddress});

            byte[] bufBytes = new byte[512];
            ByteBuffer buf = ByteBuffer.wrap(bufBytes);

            List<String> lines = getLines(connectorHandler, bufBytes, buf);

            // Looking for 220
            if (lines.isEmpty()) {
                // Received no welcome message from remote SMTP server.
                result.setOutcome(Outcome.TEMP_FAIL_OTHER);
                result.setResultMessage(ipAddress + " did not send us a welcome message.");
            } else {
                String greetingLine = lines.remove(0);
                if (greetingLine.startsWith("220")) {
                    // XXX doesn't check for ESMTP greeting
                    // Send EHLO
                    buf.clear();
                    buf.put(("EHLO " + qi.getServerInstanceHostname() + "\r\n").getBytes());
                    buf.flip();
                    while (buf.hasRemaining())
                        connectorHandler.write(buf, true);

                    lines = getLines(connectorHandler, bufBytes, buf);

                    if (lines.isEmpty()) {
                        // Received no response after EHLO
                        result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                        result.setResultMessage(ipAddress + " sent nothing to us after we sent EHLO");
                    } else {
                        boolean esmtp = false;
                        boolean keepGoing = false;

                        String firstEhloLine = lines.get(0);
                        if (firstEhloLine.length() == 0) {
                            result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                            result.setResultMessage(ipAddress + " sent a blank line to us after we sent EHLO");
                        } else if (firstEhloLine.charAt(0) == '5') {
                            // EHLO seems unsupported. We need to try HELO.
                            buf.clear();
                            buf.put(("HELO " + qi.getServerInstanceHostname() + "\r\n").getBytes());
                            buf.flip();
                            while (buf.hasRemaining())
                                connectorHandler.write(buf, true);
                            lines = getLines(connectorHandler, bufBytes, buf);

                            if (lines.isEmpty()) {
                                result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                                result.setResultMessage(ipAddress + " sent nothing to us after we sent HELO");
                            } else {
                                firstEhloLine = lines.get(0);

                                if (firstEhloLine.length() == 0) {
                                    result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                                    result.setResultMessage(ipAddress + " sent a blank line to us after we sent HELO");
                                } else if (firstEhloLine.charAt(0) == '5') {
                                    // Server reports that it is unavailable at the moment.
                                    result.setOutcome(Outcome.PERM_FAIL);
                                    result.setResultMessage(ipAddress + " Rejected our HELO: " + firstEhloLine);
                                } else if (firstEhloLine.charAt(0) == '4') {
                                    // Server reports that it is unavailable at the moment.
                                    result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                                    result.setResultMessage(ipAddress + " temp failed us after HELO: " + firstEhloLine);
                                } else if (firstEhloLine.charAt(0) == '2') {
                                    esmtp = false;
                                    keepGoing = true;
                                } else {
                                    // Garbled response.
                                    result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                                    result.setResultMessage(ipAddress + " sent us a garbled HELO response: " + firstEhloLine);
                                }
                            }
                        } else if (firstEhloLine.charAt(0) == '4') {
                            // Server reports that it is unavailable at the moment.
                            result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                            result.setResultMessage(ipAddress + " temp failed us after EHLO: " + firstEhloLine);
                        } else if (firstEhloLine.charAt(0) == '2') {
                            esmtp = keepGoing = true;
                        } else {
                            // Garbled response.
                            result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                            result.setResultMessage(ipAddress + " sent us a garbled EHLO response: " + firstEhloLine);
                        }

                        if (keepGoing) // EHLO or HELO was okay..
                        {
                            if (esmtp)
                                log.log(Level.FINEST, "{0}: ESMTP is supported by the remote host", qi.getQueueIdForDisplay());
                            else
                                log.log(Level.FINEST, "{0}: ESMTP is not supported by the remote host", qi.getQueueIdForDisplay());

                            buf.clear();
                            buf.put(("MAIL FROM:<" + qi.getReturnPath() + ">\r\n").getBytes());
                            buf.flip();
                            while (buf.hasRemaining())
                                connectorHandler.write(buf, true);
                            lines = getLines(connectorHandler, bufBytes, buf);
                            if (checkResponse(result, lines, "MAIL FROM", ipAddress)) {
                                buf.clear();
                                buf.put(("RCPT TO:<" + qi.getRecipient() + ">\r\n").getBytes());
                                buf.flip();
                                while (buf.hasRemaining())
                                    connectorHandler.write(buf, true);
                                lines = getLines(connectorHandler, bufBytes, buf);
                                if (checkResponse(result, lines, "RCPT TO", ipAddress)) {
                                    buf.clear();
                                    buf.put(("DATA\r\n").getBytes());
                                    buf.flip();
                                    while (buf.hasRemaining())
                                        connectorHandler.write(buf, true);
                                    lines = getLines(connectorHandler, bufBytes, buf);
                                    if (checkResponse(result, lines, "DATA", ipAddress)) {
                                        FileInputStream fis = qi.getInputStream();
                                        BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                                        buf.clear();
                                        String line;
                                        while ((line = reader.readLine()) != null) {
                                            if (line.length() > 0 && line.charAt(0) == '.')
                                                line = '.' + line;
                                            line += "\r\n";
                                            buf.put(line.getBytes());
                                            buf.flip();
                                            while (buf.hasRemaining())
                                                connectorHandler.write(buf, true);
                                            buf.clear();
                                        }
                                        fis.close();
                                        buf.put(new byte[]{'.', '\r', '\n'});
                                        buf.flip();
                                        while (buf.hasRemaining())
                                            connectorHandler.write(buf, true);
                                        lines = getLines(connectorHandler, bufBytes, buf);
                                        if (checkResponse(result, lines, "end of message", ipAddress)) {
                                            result.setOutcome(Outcome.SUCCESS);
                                            result.setResultMessage(lines.get(0));
                                            qi.setDelivered(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    log.log(Level.INFO, "{0}: Received bad greeting from {1}: {2}",
                            new Object[] {qi.getQueueIdForDisplay(),
                            ipAddress, greetingLine});
                }
            }

            // Send QUIT, read a line (per RFC), disconnect
            buf.clear();
            buf.put(new byte[]{'Q', 'U', 'I', 'T', '\r', '\n'});
            buf.flip();
            while (buf.hasRemaining())
                connectorHandler.write(buf, true);
            getLines(connectorHandler, bufBytes, buf);
            connectorHandler.close();
        } catch (Exception ex) {
            result.setOutcome(Outcome.TEMP_FAIL_OTHER);
            result.setResultMessage("Error while trying to send to " + ipAddress + ": " + ex.getMessage());
        } finally {
            log.log(Level.INFO, "{0}: Relay to {1}: {2}", new Object[]
                    {qi.getQueueIdForDisplay(), ipAddress, result});

            // Make sure the lock is always removed.
            relayLocks.remove(id);

            // Make sure the client connection is always closed.
            if (connectorHandler != null) {
                try {
                    connectorHandler.close();
                } catch (IOException ex) {
                }
            }
        }

        return result;
    }

    /**
     * Checks a response received from the remote server. If an error occurs,
     * <code>result</code> will be populated with error information.
     * @param result The result object for the relay connection.
     * @param lines The line(s) received from the server.
     * @param operation The operation that was sent to the server.
     * @param ipAddress The IP address of the server.
     * @return Whether the operation was successful.
     */
    private static boolean checkResponse(SMTPRelayResult result,
            List<String> lines, String operation, String ipAddress) {
        boolean keepGoing = false;

        if (lines.isEmpty()) {
            result.setOutcome(Outcome.TEMP_FAIL_SERVER);
            result.setResultMessage(ipAddress + "Sent nothing after " + operation);
        } else {
            String line = lines.get(0);

            if (line.length() == 0) {
                result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                result.setResultMessage(ipAddress + "Sent a blank line after " + operation);
            } else {
                char c = line.charAt(0);
                switch (c) {
                    case '5':
                        result.setOutcome(Outcome.PERM_FAIL);
                        result.setResultMessage(ipAddress + " Perm failed us after " + operation + ": " + line);
                        break;
                    case '4':
                        result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                        result.setResultMessage(ipAddress + " Temp failed us after " + operation + ": " + line);
                        break;
                    case '2':
                    case '3':
                        keepGoing = true;
                        break;
                    default:
                        result.setOutcome(Outcome.TEMP_FAIL_SERVER);
                        result.setResultMessage(ipAddress + " Sent a garbled response after " + operation + ": " + line);
                }
            }
        }

        return keepGoing;
    }

    /**
     * Reads bytes from the remote server and returns them as lines.
     * @param connectorHandler The Grizzly TCPConnectorHandler instance.
     * @param bufBytes The array that the byte buffer is backing.
     * @param buf The byte buffer.
     * @return A list of lines received from the remote server.
     * @throws java.io.IOException If an I/O error occurs.
     */
    private static List<String> getLines(TCPConnectorHandler connectorHandler, byte[] bufBytes, ByteBuffer buf) throws IOException {
        List<String> lines = new LinkedList<String>();
        boolean done = false;
        for (int count = 0; !done; count++) {
            buf.clear();
            do {
                connectorHandler.read(buf, true);

                // unflip, since TCPConnectorHandler.read() annoyingly flips.
                buf.position(buf.limit());
                buf.limit(buf.capacity());
            } while (buf.hasRemaining() && ((buf.position() < 2) || (bufBytes[buf.position() - 2] != '\r')));

            if (buf.hasRemaining())
                done = true;

            StringTokenizer st = new StringTokenizer(new String(bufBytes, 0, buf.position()), "\n");
            boolean firstToken = true;
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                if (token.charAt(token.length() - 1) == '\r')
                    token = token.substring(0, token.length() - 1);
                if (firstToken && (count > 0))
                    lines.set(lines.size() - 1, lines.get(lines.size() - 1) + token);
                else
                    lines.add(token);
                firstToken = false;
            }
        }

        return lines;
    }

    /**
     * Starts the Grizzly client controller. It is okay to call this method
     * more than once, since it takes precautions to be sure that it only
     * starts the controller once.
     */
    public static void startGrizzlyController() {
        if (controller != null)
            return;
        synchronized (relayLocks) {
            if (controller != null)
                return;

            // A countdown latch helps us wait for the server to start,
            // without using spinlocks, callbacks, observers, etc.
            final CountDownLatch controllerReady = new CountDownLatch(1);
            controller = new Controller();
            selectorHandler = new TCPSelectorHandler(true);
            selectorHandler.setServerTimeout(20000);
            selectorHandler.setSocketTimeout(360000);
            controller.addSelectorHandler(selectorHandler);
            final ControllerStateListener controllerStateListener = new ControllerStateListener() {

                public void onStarted() {
                }

                public void onReady() {
                    log.finest("Grizzly client controller ready");
                    controllerReady.countDown();
                }

                public void onStopped() {
                }

                public void onException(Throwable e) {
                }
            };
            controller.addStateListener(controllerStateListener);

            new Thread(controller, "Grizzly client controller starter thread").start();
            try {
                // Wait for controller to become ready, but timeout after 20 seconds.
                controllerReady.await(20, TimeUnit.SECONDS);
                controller.removeStateListener(controllerStateListener);
            } catch (InterruptedException ex) {
                log.severe("Timeout while waiting for Grizzly client controller to start");
            }
        }
    }
}
