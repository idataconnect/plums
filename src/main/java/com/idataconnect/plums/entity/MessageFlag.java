/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Mail message flag entity. Its name is the name that IMAP expects. E.g. \Seen,
 * $Forwarded, etc, but not \Recent, since that flag can't be persisted, and it
 * is handled internally by the IMAP implementation. Note that since IMAP flags
 * are case insensitive, the named query is the correct way to look up a flag.
 * @author Ben Upsavs
 */
@Entity
@Table(name = "message_flag")
@NamedQueries({
    @NamedQuery(name = "MessageFlag.findByName", query = "SELECT f FROM MessageFlag f WHERE LOWER(f.name) = LOWER(:name)")
})
public class MessageFlag implements Serializable {

    @Id
    @Column(name = "flag_name")
    private String name;

    /**
     * Checks if a flag is a valid name to add to a message. This includes all
     * system and user defined flags, except \Recent.
     * @param nameToCheck The name to check.
     * @return Whether the flag is a valid name to persist to a message.
     */
    public static boolean isValidName(String nameToCheck) {
        // Can't have an empty flag
        if (nameToCheck.length() == 0) {
            return false;
        }

        // All characters must be printable 7 bit ascii
        for (int count = 0; count < nameToCheck.length(); count++) {
            if (nameToCheck.charAt(count) < 33 || nameToCheck.charAt(count) > 126) {
                return false;
            }
        }

        // Can't have a name starting with a backslash that's not a system flag.
        // We purposefully don't include \Recent here, since it is an automatic
        // flag that users can view but not write to.
        if ((nameToCheck.charAt(0) == '\\')
                && !nameToCheck.equalsIgnoreCase("\\Seen")
                && !nameToCheck.equalsIgnoreCase("\\Answered")
                && !nameToCheck.equalsIgnoreCase("\\Flagged")
                && !nameToCheck.equalsIgnoreCase("\\Deleted")
                && !nameToCheck.equalsIgnoreCase("\\Draft")) {
            return false;
        }

        return true;
    }

    /**
     * Gets the message flag name.
     * @return The message flag name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the message flag name.
     * @param name The message flag name.
     */
    public void setName(String name) {
        if (name != null && name.length() > 0 && name.charAt(0) == '\\') {
            // Correctly capitalize system flag
            byte[] bytes = new byte[name.length()];
            bytes[0] = '\\';
            bytes[1] = (byte) Character.toUpperCase(name.charAt(1));
            for (int count = 2; count < name.length(); count++) {
                bytes[count] = (byte) Character.toLowerCase(name.charAt(count));
            }

            name = new String(bytes);
        }
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MessageFlag)) {
            return false;
        }
        return getName().equals(((MessageFlag) obj).getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
