/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.entity;

import java.io.Serializable;

/**
 * Primary key class for the MailMessage entity. Per the IMAP spec, a
 * message is unique to the server based on both its message UID and
 * the UIDVALIDITY value of the mailbox that contains it.
 * @author Ben Upsavs
 */
public class MailMessagePK implements Serializable {

    private int messageUid;
    private int mailboxUidValidity;

    /**
     * Gets the IMAP message uid of the mail message.
     * @return The message uid of the mail message.
     */
    public int getMessageUid() {
        return messageUid;
    }

    /**
     * Sets the IMAP message uid of the mail message.
     * @param messageUid The message uid of the mail message.
     */
    public void setMessageUid(int messageUid) {
        this.messageUid = messageUid;
    }

    /**
     * Gets the mailbox UIDVALIDITY value.
     * @return The mailbox UIDVALIDITY value.
     */
    public int getMailboxUidValidity() {
        return mailboxUidValidity;
    }

    /**
     * Sets the mailbox UIDVALIDITY value.
     * @param mailboxUidValidity The mailbox UIDVALIDITY value.
     */
    public void setMailboxUidValidity(int mailboxUidValidity) {
        this.mailboxUidValidity = mailboxUidValidity;
    }

    /**
     * {@inheritDoc}
     * All fields of this bean are used in the comparison.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailMessagePK other = (MailMessagePK) obj;
        if (this.messageUid != other.messageUid) {
            return false;
        }
        if (this.mailboxUidValidity != other.mailboxUidValidity) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * All fields of this bean are used when generating the hash code.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.messageUid;
        hash = 59 * hash + this.mailboxUidValidity;
        return hash;
    }
}
