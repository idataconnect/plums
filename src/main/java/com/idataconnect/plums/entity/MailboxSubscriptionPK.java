/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.entity;

import java.io.Serializable;

/**
 * Primary key class for the MailboxSubscription Entity.
 * @author Ben Upsavs
 */
public class MailboxSubscriptionPK implements Serializable {

    private String mailUserEmailAddress;
    private String mailboxName;

    /**
     * Gets the name of the mailbox.
     * @return The name of the mailbox.
     */
    public String getMailboxName() {
        return mailboxName;
    }

    /**
     * Sets the name of the mailbox.
     * @param mailboxName The name of the mailbox.
     */
    public void setMailboxName(String mailboxName) {
        this.mailboxName = mailboxName;
    }

    /**
     * Gets the mail user's email address.
     * @return The mail user's email address.
     */
    public String getMailUserEmailAddress() {
        return mailUserEmailAddress;
    }

    /**
     * Sets the mail user's email address.
     * @param mailUserEmailAddress The mail user's email address.
     */
    public void setMailUserEmailAddress(String mailUserEmailAddress) {
        this.mailUserEmailAddress = mailUserEmailAddress;
    }

    /**
     * {@inheritDoc}
     * All of this bean's fields are used in the comparison.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailboxSubscriptionPK other = (MailboxSubscriptionPK) obj;
        if ((this.mailUserEmailAddress == null) ? (other.mailUserEmailAddress != null) : !this.mailUserEmailAddress.equals(other.mailUserEmailAddress)) {
            return false;
        }
        if ((this.mailboxName == null) ? (other.mailboxName != null) : !this.mailboxName.equals(other.mailboxName)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * All of this bean's fields are used when generating the hash code.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.mailUserEmailAddress != null ? this.mailUserEmailAddress.hashCode() : 0);
        hash = 53 * hash + (this.mailboxName != null ? this.mailboxName.hashCode() : 0);
        return hash;
    }
}
