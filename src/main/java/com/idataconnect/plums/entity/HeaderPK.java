/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.entity;

import java.io.Serializable;

/**
 * Composite primary key for the Header entity.
 * @author Ben Upsavs
 */
public class HeaderPK implements Serializable {

    private static final long serialVersionUID = 1L;

    private int mailboxUidValidity;
    private int mailMessageUid;
    private int headerSequence;

    public int getMailboxUidValidity() {
        return mailboxUidValidity;
    }

    public void setMailboxUidValidity(int mailboxUidValidity) {
        this.mailboxUidValidity = mailboxUidValidity;
    }

    public int getHeaderSequence() {
        return headerSequence;
    }

    public void setHeaderSequence(int headerSequence) {
        this.headerSequence = headerSequence;
    }

    public int getMailMessageUid() {
        return mailMessageUid;
    }

    public void setMailMessageUid(int mailMessageUid) {
        this.mailMessageUid = mailMessageUid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HeaderPK other = (HeaderPK) obj;
        if (this.mailboxUidValidity != other.mailboxUidValidity) {
            return false;
        }
        if (this.mailMessageUid != other.mailMessageUid) {
            return false;
        }
        if (this.headerSequence != other.headerSequence) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.mailboxUidValidity;
        hash = 43 * hash + this.mailMessageUid;
        hash = 43 * hash + this.headerSequence;
        return hash;
    }
}
