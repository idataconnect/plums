/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Mail message queue entity.
 * @author Ben Upsavs
 */
@Entity
@Table(name = "mail_message_queue_item")
@NamedQueries({
    @NamedQuery(name = "MailMessageQueueItem.findAll", query = "SELECT q FROM MailMessageQueueItem q")
})
public class MailMessageQueueItem implements Serializable {

    @Id
    @Column(name = "queue_id")
    private int queueId;

    @Column(name = "queue_time", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date queueTime;

    @Column(name = "last_attempt_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastAttemptTime;

    @Column(name = "number_of_attempts", nullable = false)
    private int numberOfAttempts;

    @Lob
    @Column(name = "last_error_message")
    private String lastErrorMessage;

    @Column(name = "return_path", nullable = false, updatable = false)
    private String returnPath;

    @Column(name = "recipient", nullable = false, updatable = false)
    private String recipient;

    @Column(name = "incoming", nullable = false)
    private boolean incoming = true;

    @Column(name = "message_size", nullable = false)
    private int messageSize;

    @Column(name = "server_instance_hostname", nullable = false)
    private String serverInstanceHostname;
    
    @Transient
    private boolean delivered;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailMessageQueueItem other = (MailMessageQueueItem) obj;
        if (this.queueId != other.queueId) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * This implementation uses only the <tt>queueId</tt> field.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.queueId;
        return hash;
    }

    /**
     * Loads the message size by reading the size of the queue file.
     */
    public void loadMessageSize() {
        File f = new File("../../plums-data/queue", String.valueOf(getQueueId()));
        setMessageSize((int) f.length());
    }

    /**
     * Gets the queue id as a string that is suitable for displaying to the user.
     * @return The queue id for display.
     */
    public String getQueueIdForDisplay() {
        return Integer.toHexString(getQueueId());
    }

    /**
     * Gets an input stream for the queue file.
     * @return An input stream for the queue file.
     * @throws IOException If an I/O error occurs.
     */
    public FileInputStream getInputStream() throws IOException {
        File queueDirectory = new File("../../plums-data/queue");
        File queuefile = new File(queueDirectory, String.valueOf(getQueueId()));
        if (!queuefile.exists()) {
            try {
                synchronized (this) {
                    this.wait(5000);
                }
            } catch (InterruptedException ex) {
            }
            queuefile = new File(queueDirectory, String.valueOf(getQueueId()));
        }
        FileInputStream fis = new FileInputStream(queuefile);
        return fis;
    }

    /**
     * Gets an output stream for the queue file.
     * @return An output stream for the queue file.
     * @throws IOException If an I/O error occurs.
     */
    public FileOutputStream getOutputStream() throws IOException {
        if (getQueueId() == 1) {
            throw new IllegalStateException("Can't open an output stream for a mail queue item entity which has not been persisted.");
        } else {
            File messagesDirectory = new File("../../plums-data/queue");
            if (!messagesDirectory.exists()) {
                messagesDirectory.mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(new File(messagesDirectory, String.valueOf(getQueueId())));

            return fos;
        }
    }

    /**
     * Gets the queue file as a <code>File</code> reference.
     * @return The queue file.
     */
    public File getFile() {
        if (getQueueId() == 0) {
            throw new IllegalStateException("Cannot get file reference for a mail queue item entity which has not been persisted.");
        } else {
            return new File("../../plums-data/queue", String.valueOf(getQueueId()));
        }
    }

    /**
     * Gets the time that the message was last attempted to be delivered.
     * @return A <code>Date</code> object representing the time of the last
     * delivery attempt.
     */
    public Date getLastAttemptTime() {
        return lastAttemptTime;
    }

    /**
     * Sets the last delivery attempt time.
     * @param lastAttemptTime The last delivery attempt time.
     */
    public void setLastAttemptTime(Date lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    /**
     * Gets the delivery error that occured last time this message was attempted
     * to be delivered.
     * @return The error message.
     */
    public String getLastErrorMessage() {
        return lastErrorMessage;
    }

    /**
     * Sets the last delivery attempt error message.
     * @param lastErrorMessage The last delivery attempt error message.
     */
    public void setLastErrorMessage(String lastErrorMessage) {
        this.lastErrorMessage = lastErrorMessage;
    }

    /**
     * Gets the number of times that this message has been attempted to be
     * delivered.
     * @return The number of delivery attempts.
     */
    public int getNumberOfAttempts() {
        return numberOfAttempts;
    }

    /**
     * Sets the number of delivery attempts.
     * @param numberOfAttempts The number of delivery attempts.
     */
    public void setNumberOfAttempts(int numberOfAttempts) {
        this.numberOfAttempts = numberOfAttempts;
    }

    /**
     * Gets the decimal queue id of this queue item.
     * @return The queue id as a decimal integer.
     */
    public int getQueueId() {
        return queueId;
    }

    /**
     * Sets the queue id.
     * @param queueId The queue id.
     */
    public void setQueueId(int queueId) {
        this.queueId = queueId;
    }

    /**
     * Gets the time that this queue item was created.
     * @return The queue time.
     */
    public Date getQueueTime() {
        return queueTime;
    }

    /**
     * Sets The time that this queue item was created.
     * @param queueTime The queue time.
     */
    public void setQueueTime(Date queueTime) {
        this.queueTime = queueTime;
    }

    /**
     * Gets the return path of the queued message.
     * @return The return path of the queued message.
     */
    public String getReturnPath() {
        return returnPath;
    }

    /**
     * Sets the return path of the queued message.
     * @param returnPath The return path of the queued message.
     */
    public void setReturnPath(String returnPath) {
        this.returnPath = returnPath;
    }

    /**
     * Gets the recipient of the queued message.
     * @return The recipient of the queued message.
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * Sets the recipient of the queued message.
     * @param recipient The recipient of the queued message.
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * Checks if the queue item is incoming, and thus should be ignored
     * currently.
     * @return Whether the queue item is incoming.
     */
    public boolean isIncoming() {
        return incoming;
    }

    /**
     * Sets whether the queue item is incoming.
     * @param incoming Whether the queue item is incoming.
     */
    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    /**
     * Gets the size of the queued message.
     * @return The size of the queued message.
     */
    public int getMessageSize() {
        return messageSize;
    }

    /**
     * Sets the size of the queued message.
     * @param messageSize The size of the queued message.
     */
    public void setMessageSize(int messageSize) {
        this.messageSize = messageSize;
    }

    /**
     * Checks if the queue item was delivered, and thus should not be
     * attemped to be delivered again.
     * @return Whether the queue item was delivered.
     */
    public boolean isDelivered() {
        return delivered;
    }

    /**
     * Sets whether the queue item was delivered.
     * @param delivered Whether the queue item was delivered.
     */
    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    /**
     * Gets the hostname of the server instance that this queue item
     * belongs to.
     * @return The hostname of the server instance that this queue
     * item belongs to.
     */
    public String getServerInstanceHostname() {
        return serverInstanceHostname;
    }

    /**
     * Sets the hostname of the server instance that this queue item
     * belongs to.
     * @param serverInstanceHostname The hostname of the server insteance
     * that this queue item belongs to.
     */
    public void setServerInstanceHostname(String serverInstanceHostname) {
        this.serverInstanceHostname = serverInstanceHostname;
    }
}
