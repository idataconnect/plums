/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Mail message entity.
 * @author Ben Upsavs
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "MailMessage.findByMailbox", query = "SELECT m FROM MailMessage m WHERE m.mailbox.mailboxName = :mailboxName ORDER BY m.messageUid")
})
@IdClass(MailMessagePK.class)
@Table(name = "mail_message")
public class MailMessage implements Serializable {
    @Id
    @Column(name = "message_uid")
    private int messageUid;

    @ManyToOne
    @JoinColumn(name = "mailbox_uid_validity", nullable = false)
    private Mailbox mailbox;

    @Id
    @Column(name = "mailbox_uid_validity", insertable = false, updatable = false, nullable = false)
    private int mailboxUidValidity;

    @Column(name = "message_size")
    private int messageSize;

    @Column(name = "imap_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date imapDate;

    @Column(name = "recent", nullable = false)
    private boolean recent;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<MessageFlag> flags;

    @OneToMany(mappedBy = "message", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Header> headers;

    /**
     * Used internally to prevent the protocol handlers from accessing this
     * message before its message body has been written to disk.
     */
    @Transient
    private boolean incoming = true;

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MailMessage other = (MailMessage)obj;
        if (this.messageUid != other.messageUid)
            return false;
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 59 * hash + this.messageUid;
        return hash;
    }

    @Override
    public String toString()
    {
        return "MailMessage: (UIDVALIDITY=" + getMailboxUidValidity() + ", UID=" + getMessageUid() + ")";
    }

    public void loadMessageSize()
    {
        if (getMessageUid() == 0)
        {
            throw new IllegalStateException("Can't load the message size for a mail message entity before it has been persisted.");
        }
        else
        {
            File f = new File("../../plums-data/messages", String.valueOf(getMessageUid()));
            setMessageSize((int)f.length());
        }
    }

    public FileInputStream getInputStream() throws IOException
    {
        if (getMessageUid() == 0)
            throw new IllegalStateException("Can't open an input stream for a mail message entity before it has been persisted.");
        else
        {
            File messagesMailbox = new File("../../plums-data/messages");
            File messageFile = new File(messagesMailbox, String.valueOf(getMessageUid()));
            if (!messageFile.exists())
            {
                try
                {
                    synchronized (this)
                    {
                        this.wait(5000);
                    }
                }
                catch (InterruptedException ex) {}
                messageFile = new File(messagesMailbox, String.valueOf(getMessageUid()));
            }
            FileInputStream fis = new FileInputStream(messageFile);
            return fis;
        }
    }

    public FileOutputStream getOutputStream() throws IOException
    {
        if (getMessageUid() == 0)
        {
            throw new IllegalStateException("Can't open an output stream for a mail message entity before it has been persisted.");
        }
        else
        {
            File messagesMailbox = new File("../../plums-data/messages");
            if (!messagesMailbox.exists())
                messagesMailbox.mkdirs();
            FileOutputStream fos = new FileOutputStream(new File(messagesMailbox, String.valueOf(getMessageUid())));

            return fos;
        }
    }

    public int getMessageUid()
    {
        return messageUid;
    }

    public void setMessageUid(int messageUid)
    {
        this.messageUid = messageUid;
    }

    public Mailbox getMailbox()
    {
        return mailbox;
    }

    public void setMailbox(Mailbox mailbox)
    {
        this.mailbox = mailbox;
        this.mailboxUidValidity = mailbox.getUidValidity();
    }

    public int getMessageSize()
    {
        // Don't fetch the size from the file in here since if we do that,
        // bugs could slip through where the message size is never persisted
        // and this routine will have to keep asking the operating system
        // for the size of the file.

        // This value should only be zero for a very short time, while the
        // message is being transferred from the queue.
        if (messageSize == 0)
        {
            // Wait for setMessageSize() to be called
            try
            {
                synchronized (this)
                {
                    this.wait(5000);
                }
            }
            catch (InterruptedException ex) {}
        }

        return messageSize;
    }

    public void setMessageSize(int messageSize)
    {
        // Notify threads waiting for a size.
        synchronized(this)
        {
            notifyAll();
        }
        this.messageSize = messageSize;
    }

    public int getMailboxUidValidity()
    {
        return mailboxUidValidity;
    }

    public void setMailboxUidValidity(int mailboxUidValidity)
    {
        this.mailboxUidValidity = mailboxUidValidity;
    }

    public Date getImapDate()
    {
        return imapDate;
    }

    public void setImapDate(Date imapDate)
    {
        this.imapDate = imapDate;
    }

    public boolean isIncoming()
    {
        return incoming;
    }

    public void setIncoming(boolean incoming)
    {
        this.incoming = incoming;
    }

    public Set<MessageFlag> getFlags()
    {
        return flags;
    }

    public void setFlags(Set<MessageFlag> flags)
    {
        this.flags = flags;
    }

    public Set<Header> getHeaders()
    {
        return headers;
    }

    public void setHeaders(Set<Header> headers)
    {
        this.headers = headers;
    }

    public boolean isRecent()
    {
        return recent;
    }

    public void setRecent(boolean recent)
    {
        this.recent = recent;
    }
}