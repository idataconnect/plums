/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.entity;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;

/**
 * Prodocol configuration JPA Entity class. Each entity represents one network instance that
 * has a unique set of IP addresses and ports. It can represent any available
 * protocol type (POP, IMAP, etc).
 * @author Ben Upsavs
 */
@Entity
@Table(name="protocol_config")
@NamedQueries({
    @NamedQuery(name = "ProtocolConfig.findAll", query = "SELECT n FROM ProtocolConfig n")
})
public class ProtocolConfig implements Serializable {

    /**
     * The auto generated primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    /**
     * The IP addresses to bind to, separated by commas.
     */
    @Lob
    @Column(name = "addresses")
    private String addresses;

    /**
     * The ports to bind to, separated by commas.
     */
    @Lob
    @Column(name = "ports")
    private String ports;

    /**
     * The host name to advertise when a client connects.
     */
    @Column(name = "hostname")
    private String hostname;

    /**
     * The class name of the implementation of the server instance.
     */
    @Column(name="instance_class_name")
    private String instanceClassName;

    /**
     * The socket backlog. See {@link java.net.ServerSocket}.
     */
    @Column(name = "backlog")
    private int backlog;

    /**
     * The socket timeout.
     */
    @Column(name = "socket_timeout")
    private int socketTimeout;

    /**
    * Gets the auto generated primary key.
    * @return The auto generated primary key.
    */
    public int getId() {
        return id;
    }

    /**
     * Sets the primary key.
     * @param id The new primary key.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the hostname to use when chatting to remote hosts.
     * @return The hostname.
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Sets the hostname to use when chatting to remote hosts.
     * @param hostname The new hostname.
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * Gets the list of addresses to bind to, separated by commas.
     * @return The list of addresses.
     */
    public String getAddresses() {
        return addresses;
    }

    /**
     * Sets the list of addresses, separated by commas.
     * @param addresses The new list of addresses.
     */
    public void setAddresses(String addresses) {
        this.addresses = addresses;
    }

    /**
     * Gets the list of ports to bind to, separated by commas.
     * @return The list of ports.
     */
    public String getPorts() {
        return ports;
    }

    /**
     * Sets the list of ports to bind to, separated by commas.
     * @param ports The list of ports.
     */
    public void setPorts(String ports) {
        this.ports = ports;
    }

    /**
     * Gets the class name of the implementation of this instance.
     * @return The implementation class name..
     */
    public String getInstanceClassName() {
        return instanceClassName;
    }

    /**
     * Sets the class name of the implementation of this instance.
     * @param instanceClassName The implementation class name.
     */
    public void setInstanceClassName(String instanceClassName) {
        this.instanceClassName = instanceClassName;
    }

    /**
     * Gets the connection backlog. See {@link java.net.ServerSocket}.
     * @return The connection backlog.
     */
    public int getBacklog() {
        return backlog;
    }

    /**
     * Sets the connection backlog. See {@link java.net.ServerSocket}.
     * @param backlog The connection backlog.
     */
    public void setBacklog(int backlog) {
        this.backlog = backlog;
    }

    /**
     * Gets the socket timeout.
     * @return The socket timeout.
     */
    public int getSocketTimeout() {
        return socketTimeout;
    }

    /**
     * Sets the socket timeout.
     * @param socketTimeout The socket timeout.
     */
    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    /**
     * Validates and extracts an array or ports from a string of ports separated
     * by commas. Does not throw exceptions but logs each port that cannot be
     * parsed.
     * @param portsString The string to parse.
     * @return An array of validated ports.
     */
    public static short[] extractPorts(String portsString) {
        if (portsString == null) {
            return new short[0];
        }

        List<Short> portsList = new LinkedList<Short>();
        StringTokenizer st = new StringTokenizer(portsString, ", ");
        while (st.hasMoreTokens()) {
            String portString = st.nextToken();
            try {
                short port = Short.parseShort(portString);
                portsList.add(port);
            } catch (NumberFormatException ex) {
                Logger.getLogger(ProtocolConfig.class.getName()).log(Level.WARNING,
                        "Could not parse port {0}", portString);
            }
        }

        short[] ports = new short[portsList.size()];
        Iterator<Short> i = portsList.iterator();
        for (int count = 0; count < ports.length; count++) {
            ports[count] = i.next();
        }

        return ports;
    }

    /**
     * Validates and extracts an array of inet addresses from a string of
     * addresses separated by commas. Does not throw exceptions but logs each
     * inet address that cannot be parsed.
     * @param inetAddressesString The string to parse.
     * @return An array of validated inet addresses.
     */
    public static InetAddress[] extractInetAddresses(String inetAddressesString) {
        if (inetAddressesString == null) {
            return new InetAddress[0];
        }

        List<InetAddress> inetAddressList = new LinkedList<InetAddress>();
        StringTokenizer st = new StringTokenizer(inetAddressesString, ", ");
        while (st.hasMoreTokens()) {
            String inetAddressString = st.nextToken();
            try {
                InetAddress address = InetAddress.getByName(inetAddressString);
                inetAddressList.add(address);
            } catch (UnknownHostException ex) {
                Logger.getLogger(ProtocolConfig.class.getName()).log(Level.WARNING,
                        "Could not parse hostname {0}", inetAddressString);
            }
        }

        InetAddress[] addresses = new InetAddress[inetAddressList.size()];
        Iterator<InetAddress> i = inetAddressList.iterator();
        for (int count = 0; count < addresses.length; count++) {
            addresses[count] = i.next();
        }

        return addresses;
    }
}
