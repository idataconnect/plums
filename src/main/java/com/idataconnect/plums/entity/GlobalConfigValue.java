/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Global Server Configuration Value Entity.
 * @author Ben Upsavs
 */
@Entity
@Table(name = "global_config_value")
public class GlobalConfigValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "config_key", length = 50, nullable = false)
    private String key;
    
    @Column(name = "config_value", length = 255, nullable = false)
    private String value;

    /**
     * Constructs an empty instance of a global config value. Both the key
     * and the value are initially <code>null</code>.
     */
    public GlobalConfigValue() {
    }

    /**
     * Constructs a populated instance of a global config value.
     *
     * @param key The key of the global config value.
     * @param value The value of the global config value.
     */
    public GlobalConfigValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * gets the key of the config value.
     * @return The key of the config value.
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the key of the config value.
     * @param key The key of the config value.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets the value of the config value.
     * @return The value of the config value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the config value.
     * @param value The value of the config value.
     */
    public void setValue(String value) {
        this.value = value;
    }
}
