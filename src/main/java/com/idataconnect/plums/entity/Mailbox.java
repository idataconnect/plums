/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.entity;

import com.idataconnect.plums.imap.IMAPConnectionState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 * Mailbox entity. \NoSelect is stored in the noSelect field. \HasChildren or
 * \HasNoChildren is stored in the hasChildren field.
 * @author Ben Upsavs
 */
@Entity
@Table(name="mailbox")
@NamedQueries({
    @NamedQuery(name = "Mailbox.findByName", query = "SELECT m FROM Mailbox m WHERE LOWER(m.mailboxName) = LOWER(:mailboxName)"),
    @NamedQuery(name = "Mailbox.findByNameLike", query = "SELECT m FROM Mailbox m WHERE LOWER(m.mailboxName) LIKE :mailboxName")
})
public class Mailbox implements Serializable, Cloneable {

    @Id
    @GeneratedValue
    @Column(name = "uid_validity")
    private int uidValidity;

    @Column(name = "mailbox_name", unique = true, nullable = false)
    private String mailboxName;

    @Column(name = "next_uid")
    private int nextUid = 1;

    @Column(name = "no_select")
    private boolean noSelect;

    @Column(name = "has_children")
    private boolean hasChildren;

    @OneToMany(mappedBy="mailbox")
    private List<MailMessage> messages;

    @Transient
    private volatile Set<IMAPConnectionState> imapConnections;

    @Override
    public Object clone() throws CloneNotSupportedException {
        Mailbox cloned = (Mailbox) super.clone();
        cloned.setMessages(new ArrayList<MailMessage>(cloned.getMessages()));
        return cloned;
    }

    /**
     * Gets the mailbox name. E.g. <b>user.test@example.com.Trash</b>.
     *
     * @return The mailbox name.
     */
    public String getMailboxName() {
        return mailboxName;
    }

    /**
     * Sets the mailbox name. E.g. <b>user.test@example.com.Trash</b>.
     * @param mailboxName The new mailbox name.
     */
    public void setMailboxName(String mailboxName) {
        this.mailboxName = mailboxName;
    }

    /**
     * Gets the IMAP UIDVALIDITY of the mailbox. This is implemented as a
     * database generated value, since we don't want to preserve
     * NEXTUID message UIDs when mailboxes are deleted. See the IMAP spec for
     * more details.
     *
     * @return The IMAP UIDVALIDITY value.
     */
    public int getUidValidity() {
        return uidValidity;
    }

    /**
     * Sets a new UIDVALIDITY for this mailbox.
     *
     * @param uidValidity The new UIDVALIDITY value for the mailbox.
     */
    public void setUidValidity(int uidValidity) {
        this.uidValidity = uidValidity;
    }

    /**
     * Gets the UID of the next message to be stored in this mailbox.
     *
     * @return The next UID.
     */
    public synchronized int getNextUid() {
        return nextUid;
    }

    /**
     * Sets the UID of the next message to be stored in this mailbox.
     *
     * @param nextUid The next UID.
     */
    public synchronized void setNextUid(int nextUid) {
        this.nextUid = nextUid;
    }

    /**
     * Gets the next available UID, which is guaranteed to be unique.
     *
     * @return The next available UID.
     */
    public synchronized int allocateNextUid() {
        return nextUid++;
    }

    /**
     * Gets the messages stored in this <code>Mailbox</code>.
     *
     * @return A <code>List</code> of messages stored in the <code>Mailbox</code>.
     */
    public List<MailMessage> getMessages() {
        return messages;
    }

    /**
     * Sets the messages that are stored in this >code>Mailbox</code>.
     *
     * @param messages The <code>List</code> of messages to be stored in this
     * <code>Mailbox</code>.
     */
    public void setMessages(List<MailMessage> messages) {
        this.messages = messages;
    }

    /**
     * Gets whether this mailbox cannot have messages stored in it. If this is
     * <code>true</code>, it cannot be selected in an IMAP session, and it
     * functions only as a holder for inferiors.
     *
     * @return Whether this mailbox cannot have messages stored in it.
     */
    public boolean isNoSelect() {
        return noSelect;
    }

    /**
     * Sets whether this mailbox cannot have messages stored in it. If this is
     * <code>true</code>, it cannot be selected in an IMAP session, and it
     * functions only as a holder for inferiors. Note that all messages should
     * be deleted from the mailbox at the same time it is flagged as \NoSelect.
     *
     * @param noSelect Whether this mailbox cannot have messages stored in it.
     */
    public void setNoSelect(boolean noSelect) {
        this.noSelect = noSelect;
    }

    /**
     * Gets whether this mailbox has children. This is for the benefit of IMAP
     * routines which need to check if a mailbox has children, such as the
     * CHILDREN extension, and deleting a mailbox. It is redundant and advisory
     * so its value should be kept up to date whenever a mailbox is created or
     * deleted.
     *
     * @return Whether this mailbox has children
     */
    public boolean isHasChildren() {
        return hasChildren;
    }

    /**
     * Sets whether this mailbox has children. This is for the benefit of IMAP
     * routines which need to check if a mailbox has children, such as the
     * CHILDREN extension, and deleting a mailbox. It is redundant and advisory
     * so its value should be kept up to date whenever a mailbox is created or
     * deleted.
     *
     * @param hasChildren whether this mailbox has children
     */
    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public Set<IMAPConnectionState> getImapConnections() {
        return Collections.unmodifiableSet(getImapConnections0());
    }
    
    private Set<IMAPConnectionState> getImapConnections0() {
        if (imapConnections == null) {
            synchronized (this) {
                if (imapConnections == null) {
                    imapConnections = new HashSet<IMAPConnectionState>(16);
                }
            }
        }

        return imapConnections;
    }

    /**
     * Adds an IMAP connection to this mailbox.
     *
     * @param connectionState the IMAP connection's state instance
     * @return <code>true</code> if this connection was not already added
     */
    public boolean addImapConnection(IMAPConnectionState connectionState) {
        return getImapConnections0().add(connectionState);
    }

    /**
     * Removes an IMAP connection from this mailbox.
     *
     * @param connectionState the IMAP connection's state instance
     * @return <code>true</codE> if this connection existed
     */
    public boolean removeImapConnection(IMAPConnectionState connectionState) {
        return getImapConnections0().remove(connectionState);
    }

    /**
     * {@inheritDoc}
     * This implementation takes the uidValidity field into account only.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mailbox other = (Mailbox) obj;
        if (this.uidValidity != other.uidValidity) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * This implementation takes the uidValidity field into account only.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.uidValidity;
        return hash;
    }
}
