/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * JPA entity class for a single header contained in a mail message.
 * @author Ben Upsavs
 */
@Entity
@IdClass(HeaderPK.class)
@NamedQueries({
    @NamedQuery(name = "Header.findByMailMessage", query = "SELECT h FROM Header h WHERE h.message = :message ORDER BY h.headerSequence")
})
public class Header implements Serializable, Comparable {

    @Id
    @Column(name = "mailbox_uid_validity", nullable = false, insertable = false, updatable = false)
    private int mailboxUidValidity;
    
    @Id
    @Column(name = "mail_message_uid", nullable = false, insertable = false, updatable = false)
    private int mailMessageUid;

    @Id
    @Column(name = "header_sequence", nullable = false)
    private int headerSequence;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "mailbox_uid_validity", referencedColumnName = "mailbox_uid_validity"),
        @JoinColumn(name = "mail_message_uid", referencedColumnName = "message_uid")
    })
    private MailMessage message;

    @Column(name = "header_name", nullable = false)
    private String headerName;

    @Lob
    @Column(name = "header_value", nullable = false)
    private String headerValue;

    /**
     * Gets the mailbox UID validity of the message that this header
     * is attached to.
     * @return The mailbox UID validity.
     */
    public int getMailboxUidValidity() {
        return mailboxUidValidity;
    }

    /**
     * Sets the mailbox UID validity of the message that this header
     * is attached to.
     * @param mailboxUidValidity The mailbox UID validity.
     */
    public void setMailboxUidValidity(int mailboxUidValidity) {
        this.mailboxUidValidity = mailboxUidValidity;
    }

    /**
     * Gets the name of the header. The name is everything that
     * comes before the colon on the header line.
     * @return The name of the header.
     */
    public String getHeaderName() {
        return headerName;
    }

    /**
     * Sets the name of the header. The name is everything that comes
     * before the colon on the header line.
     * @param headerName The name of the header.
     */
    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    /**
     * Gets the sequence number of the header. This is used to track
     * which order the headers appear in the message.
     * @return The sequence number of the header.
     */
    public int getHeaderSequence() {
        return headerSequence;
    }

    /**
     * Sets the sequence number of the header. This is used to track
     * which order the headers appear in the message.
     * @param headerSequence The sequence number of the header.
     */
    public void setHeaderSequence(int headerSequence) {
        this.headerSequence = headerSequence;
    }

    /**
     * Gets the value of the header. This is everything that comes
     * after the colon on the header line.
     * @return The value of the header.
     */
    public String getHeaderValue() {
        return headerValue;
    }

    /**
     * Sets the value of the header. This is everything that comes after
     * the colon on the header line.
     * @param headerValue The value of the header.
     */
    public void setHeaderValue(String headerValue) {
        this.headerValue = headerValue;
    }

    /**
     * Gets the UID of the message that this header is attached to.
     * @return The UID of the message that this header is attached to.
     */
    public int getMailMessageUid() {
        return mailMessageUid;
    }

    /**
     * Sets the UID of the message that this header is attached to.
     * @param mailMessageUid The UID of the message that this header
     * is attached to.
     */
    public void setMailMessageUid(int mailMessageUid) {
        this.mailMessageUid = mailMessageUid;
    }

    /**
     * Gets the message that this header is attached to.
     * @return The message that this header is attached to.
     */
    public MailMessage getMessage() {
        return message;
    }

    /**
     * Sets the message that this header is attached to.
     * @param message The message that this header is attached to.
     */
    public void setMessage(MailMessage message) {
        this.message = message;
    }

    /**
     * {@inheritDoc}
     * This implementation compares the seqence of the header against
     * the sequence of the other header. Thus, this comparison is only
     * relevant to headers attached to the same message.
     */
    public int compareTo(Object o) {
        if (!(o instanceof Header)) {
            return 0;
        }

        Header other = (Header) o;
        return new Integer(getHeaderSequence()).compareTo(new Integer(other.getHeaderSequence()));
    }
}