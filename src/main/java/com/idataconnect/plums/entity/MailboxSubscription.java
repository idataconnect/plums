/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connnect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Mailbox subscription entity. This is implemented as a separate entity since
 * the possibility of having dangling records is a requirement.
 * In other words, if a user deletes a mailbox, their subscription remains, and
 * if they re-create a mailbox with the same name, they are subscribed to the
 * new mailbox. This is part of the IMAP spec.
 * @author Ben Upsavs
 */
@Entity
@Table(name = "mailbox_subscription")
@IdClass(MailboxSubscriptionPK.class)
@NamedQueries({
    @NamedQuery(name = "MailboxSubscription.findByMailUser", query = "SELECT fs FROM MailboxSubscription fs WHERE fs.mailUser = :mailUser")
})
public class MailboxSubscription implements Serializable {

    @Id
    @Column(name = "mail_user_email_address", insertable = false, updatable = false)
    private String mailUserEmailAddress;

    @ManyToOne
    @JoinColumn(name = "mail_user_email_address")
    private MailUser mailUser;

    @Id
    @Column(name = "mailbox_name")
    private String mailboxName;

    /**
     * Gets the name of the mailbox.
     * @return The name of the mailbox.
     */
    public String getMailboxName() {
        return mailboxName;
    }

    /**
     * Sets the name of the mailbox.
     * @param mailboxName The name of the mailbox.
     */
    public void setMailboxName(String mailboxName) {
        this.mailboxName = mailboxName;
    }

    /**
     * Gets the mail user who is subscribed to the mailbox.
     * @return The mail user who is subscribed to the mailbox.
     */
    public MailUser getMailUser() {
        return mailUser;
    }

    /**
     * Sets the mail user who is subscribed to the mailbox.
     * @param mailUser The mail user who is subscribed to the mailbox.
     */
    public void setMailUser(MailUser mailUser) {
        this.mailUser = mailUser;
    }

    /**
     * Gets the mail user's email address. This is a read-only field that
     * is included to satisfy the entity's primary key requirement.
     * @return The mail user's email address.
     */
    public String getMailUserEmailAddress() {
        return mailUserEmailAddress;
    }

    /**
     * Sets the mail user's email address. This is a read-only field that
     * is included to satify the entity's primary key requirement.
     * @param mailUserEmailAddress The mail user's email address.
     */
    public void setMailUserEmailAddress(String mailUserEmailAddress) {
        this.mailUserEmailAddress = mailUserEmailAddress;
    }
}