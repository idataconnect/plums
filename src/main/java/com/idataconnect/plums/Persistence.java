/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums;

import com.idataconnect.plums.entity.*;
import java.io.File;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * An application service class which handles as much of the persistence
 * implementation as possible.
 * @author Ben Upsavs
 */
public class Persistence {

    /**
     * Adds a user to the database.
     * @param emailAddress The unique email address of the new user.
     * @param password The new user's password.
     */
    public static void addUser(String emailAddress, String password) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            emailAddress = emailAddress.trim();

            // Check if the email address is unique.
            em.getTransaction().begin();
            MailUser user = null;
            try {
                user = (MailUser) em.createNamedQuery("MailUser.findByEmailAddress").setParameter("emailAddress", emailAddress).getSingleResult();
            } catch (NoResultException ex) {
            }

            if (user != null) {
                // User already exists
                em.getTransaction().rollback();
            } else {
                em.persist(user);
                em.getTransaction().commit();
            }
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a user from the database.
     * @param user The managed user entity.
     */
    public static void deleteUser(MailUser user) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();

            MailUser userToDelete = em.find(MailUser.class, user.getEmailAddress());
            if (userToDelete != null) {
                em.remove(userToDelete);
            }

            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Populates the database with default values. This is mainly used for
     * new installs.
     * @param force whether or not to force setting the values. If
     * <code>true</code>, an existing configuration will be overwritten
     * with default values. If <code>false</code>, only new installations
     * will have default values set.
     */
    public static void insertDefaultValues(boolean force) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            // Try to fetch the serverStartCount config value, to
            // determine whether this is a new installation.
            GlobalConfigValue serverStartCount = null;
            if (!force) {
                serverStartCount = em.find(GlobalConfigValue.class, "serverStartCount");
            }
            if (serverStartCount == null) {
                em.getTransaction().begin();
                if (force) {
                    Logger.getLogger(Persistence.class.getName()).info("Forcing delete of old data");
                    // Empty the existing databases - Keep this updated!
                    em.createQuery("DELETE FROM GlobalConfigValue").executeUpdate();
                    em.createQuery("DELETE FROM Mailbox").executeUpdate();
                    em.createQuery("DELETE FROM MailMessage").executeUpdate();
                    em.createQuery("DELETE FROM MailUser").executeUpdate();
                    em.createQuery("DELETE FROM ProtocolConfig").executeUpdate();
                    em.createQuery("DELETE FROM HostedDomain").executeUpdate();
                    em.createQuery("DELETE FROM SMTPLogEntry").executeUpdate();
                    em.createQuery("DELETE FROM MailMessageQueueItem").executeUpdate();
                    em.createQuery("DELETE FROM MessageFlag").executeUpdate();
                    em.createQuery("DELETE FROM MailboxSubscription").executeUpdate();
                    em.createQuery("DELETE FROM Header").executeUpdate();
                }
                serverStartCount = new GlobalConfigValue("serverStartCount", "0");
                em.persist(serverStartCount);

                // Add a default hosted domain
                HostedDomain domain = new HostedDomain();
                domain.setDomainName("example.com");
                em.persist(domain);

                // Add a default POP instance
                ProtocolConfig popInstance = new ProtocolConfig();
                popInstance.setAddresses(null);
                popInstance.setSocketTimeout(60000);
                try {
                    popInstance.setHostname(java.net.InetAddress.getLocalHost().getHostName());
                } catch (UnknownHostException ex) {
                    popInstance.setHostname("localhost");
                }
                popInstance.setInstanceClassName("com.idataconnect.plums.pop.POPServerInstance");
                popInstance.setPorts("11110");
                em.persist(popInstance);

                // Add a default SMTP instance
                ProtocolConfig smtpInstance = new ProtocolConfig();
                smtpInstance.setAddresses(null);
                smtpInstance.setSocketTimeout(360000);
                try {
                    smtpInstance.setHostname(java.net.InetAddress.getLocalHost().getHostName());
                } catch (UnknownHostException ex) {
                    smtpInstance.setHostname("localhost");
                }
                smtpInstance.setInstanceClassName("com.idataconnect.plums.smtp.SMTPServerInstance");
                smtpInstance.setPorts("11025");
                em.persist(smtpInstance);

                // Add a default IMAP instance
                ProtocolConfig imapInstance = new ProtocolConfig();
                imapInstance.setAddresses(null);
                imapInstance.setSocketTimeout(1800000);
                try {
                    imapInstance.setHostname(java.net.InetAddress.getLocalHost().getHostName());
                } catch (UnknownHostException ex) {
                    imapInstance.setHostname("localhost");
                }
                imapInstance.setInstanceClassName("com.idataconnect.plums.imap.IMAPServerInstance");
                imapInstance.setPorts("11143");
                em.persist(imapInstance);

                // Add a test user
                MailUser u = new MailUser();
                u.setDomainName("example.com");
                u.setEmailAddress("test@example.com");
                u.setUsername("test");
                u.setPassword("test");
                em.persist(u);

                // Create the test user's INBOX
                Mailbox f = new Mailbox();
                f.setMailboxName("user.test@example.com");
                em.persist(f);

                // Subscribe the user to their INBOX
                MailboxSubscription fs = new MailboxSubscription();
                fs.setMailboxName(f.getMailboxName());
                fs.setMailUser(u);
                em.persist(fs);

                em.getTransaction().commit();
            }
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a global server configuration value from the database. If the
     * entry was not found in the database, the default value is returned.
     * @param configKey The key that the config value is assigned to.
     * @param defaultValue The value that will be returned if there is no
     * value in the database.
     * @return The value from the database, or <code>defaultValue</code> if
     * no value exists in the database.
     */
    public static String getGlobalConfigValue(String configKey, String defaultValue) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            GlobalConfigValue gcv = em.find(GlobalConfigValue.class, configKey);
            return gcv == null ? defaultValue : gcv.getValue();
        } finally {
            em.close();
        }
    }

    /**
     * Updates a global server configuration value.
     * @param configKey The key that the config value is assigned to.
     * @param configValue The value to assign.
     */
    public static void setGlobalConfigValue(String configKey, String configValue) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            GlobalConfigValue v = em.find(GlobalConfigValue.class, configKey);
            if (v == null) {
                v = new GlobalConfigValue(configKey, configValue);
                em.persist(v);
            } else {
                v.setValue(configValue);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Adds a protocol config entity to the database.
     * @param protocolConfig The protocol config entity to add.
     */
    public static void addProtocolConfig(ProtocolConfig protocolConfig) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(protocolConfig);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Updates a protocol config entity in the database.
     * @param protocolConfig
     */
    public static void updateProtocolConfig(ProtocolConfig protocolConfig) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(protocolConfig);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a protocol config entity from the database.
     * @param protocolConfig
     */
    public static void deleteProtocolConfig(ProtocolConfig protocolConfig) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.remove(protocolConfig);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Fetches all protocol configs from the database.
     * @return A list of protocol configs.
     */
    @SuppressWarnings("unchecked")
    public static List<ProtocolConfig> getAllProtocolConfigs() {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (List<ProtocolConfig>) em.createNamedQuery("ProtocolConfig.findAll").getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a local mail user from the database by email address and password.
     * @param emailAddress The email address of the local user.
     * @param password The password of the local user.
     * @return The local user, or <code>null</code> if the specified user
     * could not be found, either because the username doesn't exist, or the
     * password is incorrect.
     */
    public static MailUser getUserByEmailAddressAndPassword(String emailAddress, String password) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (MailUser) em.createNamedQuery("MailUser.findByEmailAddressAndPassword").setParameter("emailAddress", emailAddress).setParameter("password", password).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a local mail user from the database by their email address.
     * @param emailAddress The email address of the local user.
     * @return The local user, or <code>null</code> if the specified user
     * could not be found.
     */
    public static MailUser getUserByEmailAddress(String emailAddress) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (MailUser) em.createNamedQuery("MailUser.findByEmailAddress").setParameter("emailAddress", emailAddress).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a mailbox entity from the database.
     * @param mailboxName The name of the mailbox to fetch.
     * @return The mailbox entity, or <code>null</code> if the mailbox could
     * not be found.
     */
    public static Mailbox getMailboxByName(String mailboxName) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (Mailbox) em.createNamedQuery("Mailbox.findByName").setParameter("mailboxName", mailboxName).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }

    /**
     * Fetches mailboxes using a search term.
     * @param searchTerm The JPQL LIKE query search term.
     * @return A list of mailboxes that match the search term.
     */
    @SuppressWarnings("unchecked")
    public static List<Mailbox> getMailboxesByNameLike(String searchTerm) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (List<Mailbox>) em.createNamedQuery("Mailbox.findByNameLike").setParameter("mailboxName", searchTerm).getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Fetches mailboes for the specified user. This includes their INBOX and
     * all inferiors.
     * @param user The user to fetch mailboxes for.
     * @return A list of all mailboxes belonging to the user.
     */
    public static List<Mailbox> getAllMailboxesForUser(MailUser user) {
        List<Mailbox> mailboxes = new LinkedList<Mailbox>();
        mailboxes.add(getMailboxByName("user." + user.getEmailAddress()));
        mailboxes.addAll(getMailboxesByNameLike("user." + user.getEmailAddress() + ".%"));
        return mailboxes;
    }

    /**
     * Deletes multiple messages from a mailbox.
     * @param mailboxName The name of the mailbox.
     * @param messageUids The uids of the messages to delete.
     */
    @SuppressWarnings("unchecked")
    public static void deleteMessagesFromMailbox(String mailboxName, Collection<Integer> messageUids) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            if (messageUids.isEmpty()) {
                return;
            }

            Mailbox m = null;
            try {
                m = (Mailbox) em.createNamedQuery("Mailbox.findByName").setParameter("mailboxName", mailboxName).getSingleResult();
            } catch (NoResultException ex) {
            }
            if (m != null) {
                Set<File> filesToDelete = new HashSet<File>();
                em.getTransaction().begin();
                List<MailMessage> messages = (List<MailMessage>) em.createNamedQuery("MailMessage.findByMailbox").setParameter("mailboxName", m.getMailboxName()).getResultList();
                File messagesMailbox = new File("../../plums-data/messages");
                for (MailMessage message : messages) {
                    if (messageUids.contains(message.getMessageUid())) {
                        message.getFlags().clear();
                        em.remove(message);
                        m.getMessages().remove(message);
                        File messageFile = new File(messagesMailbox, String.valueOf(message.getMessageUid()));
                        filesToDelete.add(messageFile);
                    }
                }
                em.getTransaction().commit();

                // Since the transaction commit successfully, delete the files
                // belonging to the messages we just deleted.
                for (File file : filesToDelete) {
                    try {
                        file.delete();
                    } catch (Exception ex) {
                        Logger.getLogger(Persistence.class.getName()).log(Level.SEVERE,
                                "Could not delete message file {0}: {1}",
                                new Object[] {file.getName(), ex.getMessage()});
                    }
                }
            }
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a hosted domain entity from the database. This is useful for
     * checking if a message may be locally delivered.
     * @param domainName The domain name.
     * @return The hosted domain entity, or <code>null</code> if it doesn't
     * exist.
     */
    public static HostedDomain getHostedDomain(String domainName) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return em.find(HostedDomain.class, domainName.toLowerCase());
        } finally {
            em.close();
        }
    }

    /**
     * Fetches all the hosted domain entities from the database. This is useful
     * for checking which email domains are hosted by this server.
     * @return A list of hosted domain entities.
     */
    @SuppressWarnings("unchecked")
    public static List<HostedDomain> getAllHostedDomains() {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (List<HostedDomain>) em.createNamedQuery("HostedDomain.findAll").getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a hosted domain entity from the database.
     * @param domainName The domain name.
     */
    public static void deleteHostedDomain(String domainName) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            HostedDomain d = em.find(HostedDomain.class, domainName.toLowerCase());
            if (d != null) {
                em.remove(d);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Adds a mail message to the specified mailbox, and returns the entity
     * that was created. The message data should subsequently be saved to disk
     * by calling the entity's <code>getOutputStream()</code> method.
     * @param mailboxName The name of the mailbox to save the entity to.
     * @return The new entity.
     */
    public static MailMessage addMailMessage(String mailboxName) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            Mailbox mailbox = null;
            try {
                mailbox = (Mailbox) em.createNamedQuery("Mailbox.findByName").setParameter("mailboxName", mailboxName).getSingleResult();
            } catch (NoResultException ex) {
                throw new IllegalArgumentException("Could not find mailbox " + mailboxName);
            }
            if (mailbox != null) {
                MailMessage message = new MailMessage();
                message.setImapDate(new java.util.Date());
                message.setIncoming(true);
                message.setMessageUid(mailbox.allocateNextUid());
                message.setMailbox(mailbox);
                em.persist(message);
                mailbox.getMessages().add(message);
                em.getTransaction().commit();
                return message;
            } else {
                em.getTransaction().rollback();
                throw new IllegalArgumentException("Cannot find mailbox: " + mailboxName);
            }
        } finally {
            em.close();
        }
    }

    /**
     * Updates the mail message entity.
     * @param m The mail message entity to update.
     */
    public static void updateMailMessage(MailMessage m) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(m);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Allocates a new SMTP log entry. This inserts an empty log entry into the
     * database, and obtains a new SMTP id. The log should subsequently be
     * populated then updated.
     * @return The new SMTP log.
     */
    public static SMTPDeliveryLogEntry addSMTPLogEntry() {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            SMTPDeliveryLogEntry l = new SMTPDeliveryLogEntry();
            em.persist(l);
            em.getTransaction().commit();

            return l;
        } finally {
            em.close();
        }
    }

    /**
     * Fetches an SMTP log entry from the database.
     * @param smtpId The ID of the log entry.
     * @return The SMTP log entry, or <code>null</code> if it could not be
     * found.
     */
    public static SMTPDeliveryLogEntry getSMTPLogEntry(int smtpId) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return em.find(SMTPDeliveryLogEntry.class, smtpId);
        } finally {
            em.close();
        }
    }

    /**
     * Updates an existing SMTP log entry.
     * @param l The existing SMTP log entry.
     */
    public static void updateSMTPLogEntry(SMTPDeliveryLogEntry l) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(l);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Adds a mail message queue item to the database. The message should
     * subsequently be written to disk using the entity's
     * <code>getOutputStream()</code> method.
     * @param q The mail message queue item to add.
     */
    public static void addMailMessageQueueItem(MailMessageQueueItem q) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(q);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Updates a mail message queue item to the database.
     * @param q The mail message queue item to update.
     */
    public static void updateMailMessageQueueItem(MailMessageQueueItem q) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(q);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Updates a collection of mail message queue items to the database.
     * @param queueItems
     */
    public static void updateMailMessageQueueItems(Collection<MailMessageQueueItem> queueItems) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            for (MailMessageQueueItem qi : queueItems) {
                em.merge(qi);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a mail message queue item from the database, including its
     * queue file, if there is one.
     * @param queueId The ID of the mail message queue item.
     */
    public static void deleteMailMessageQueueItem(int queueId) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            MailMessageQueueItem q = em.find(MailMessageQueueItem.class, queueId);
            if (q != null) {
                em.remove(q);
            }
            em.getTransaction().commit();
            File queueDirectory = new File("../../plums-data/queue"); // TODO make path configurable
            File queueFile = new File(queueDirectory, String.valueOf(q.getQueueId()));
            queueFile.delete();
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a collection of mail message queue items from the database. Their
     * queue files are deleted if they have them.
     * @param items The collection of mail message queue items.
     */
    public static void deleteMailMessageQueueItems(Collection<MailMessageQueueItem> items) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            final File[] filesToDelete = new File[items.size()];
            em.getTransaction().begin();
            Iterator<MailMessageQueueItem> i = items.iterator();
            for (int count = 0; count < items.size(); count++) {
                MailMessageQueueItem qi = i.next();
                MailMessageQueueItem currentItem = em.find(MailMessageQueueItem.class, qi.getQueueId());
                if (currentItem != null) {
                    em.remove(currentItem);
                }
                File queueDirectory = new File("../../plums-data/queue");
                File queueFile = new File(queueDirectory, String.valueOf(qi.getQueueId()));
                filesToDelete[count] = queueFile;
            }
            em.getTransaction().commit();

            // Delete the actual queue files in a different thread.
            Thread deleteThread = new Thread("Delete queue item files thread") {

                @Override
                public void run() {
                    for (File file : filesToDelete) {
                        file.delete();
                    }
                }
            };
            deleteThread.setDaemon(false);
            deleteThread.start();
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a mail message queue item from the database.
     * @param queueId The ID of the mail message queue item.
     * @return The mail message queue item, or <code>null</code> if it was
     * not found.
     */
    public static MailMessageQueueItem getMailMessageQueueItem(int queueId) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return em.find(MailMessageQueueItem.class, queueId);
        } finally {
            em.close();
        }
    }

    /**
     * Fetches a list of mail message queue items from the database.
     * @param offset The first row to fetch from the database.
     * @param limit The maximum number of mail message queue items to fetch.
     * @return The list of mail message queue items.
     */
    @SuppressWarnings("unchecked")
    public static List<MailMessageQueueItem> getMailMessageQueueItems(int offset, int limit) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (List<MailMessageQueueItem>) em.createNamedQuery("MailMessageQueueItem.findAll").setFirstResult(offset).setMaxResults(limit).getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Gets a space separated list of authentication mechanisms that are enabled.
     * This is the same information that is seen after the result code, as
     * a result of the client issuing an EHLO command.
     * @return The space separated list of enabled authentication mechanisms.
     */
    public static String getSMTPAuthenticationMechanismsAsString() {
        return getGlobalConfigValue("persistenceMechanisms", "CRAM-MD5 LOGIN PLAIN");
    }

    /**
     * Checks if the specified SMTP authentication mechanism is enabled.
     * @param mechanism The mechanism to check if it is enabled.
     * @return Whether the specified authentication mechanism is enabled.
     */
    public static boolean isSMTPAuthenticationMechanismEnabled(String mechanism) {
        String mechanismString = getSMTPAuthenticationMechanismsAsString();
        StringTokenizer st = new StringTokenizer(mechanismString, " ");
        while (st.hasMoreTokens()) {
            String m = st.nextToken();
            if (m.equalsIgnoreCase(mechanism)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the mailbox subscriptions for the given mail user.
     * @param user The mail user to fetch subscriptions for.
     * @return A list of subscriptions for the given mail user.
     */
    @SuppressWarnings("unchecked")
    public static List<MailboxSubscription> getMailboxSubscriptionsForMailUser(MailUser user) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return (List<MailboxSubscription>) em.createNamedQuery("MailboxSubscription.findByMailUser").setParameter("mailUser", user).getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Adds a mailbox subscription.
     * @param fs The mailbox subscrition to add.
     */
    public static void addMailboxSubscription(MailboxSubscription fs) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(fs);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a mailbox subscription.
     * @param fs The mailbox subscription to delete.
     */
    public static void deleteMailboxSubscription(MailboxSubscription fs) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            MailboxSubscriptionPK pk = new MailboxSubscriptionPK();
            pk.setMailboxName(fs.getMailboxName());
            pk.setMailUserEmailAddress(fs.getMailUser().getEmailAddress());
            fs = em.find(MailboxSubscription.class, pk);
            if (fs != null) {
                em.remove(fs);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Adds a mailbox and all required parents of the newly created mailbox.
     * @param m The mailbox to create.
     * @return <code>null</code> if the Mailbox was successfully created, or
     * an error message if not.
     */
    public static String addMailboxAndParents(Mailbox m) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();

            Mailbox existingMailbox = null;
            try {
                existingMailbox = (Mailbox) em.createNamedQuery("Mailbox.findByName").setParameter("mailboxName", m.getMailboxName()).getSingleResult();
            } catch (NoResultException ex) {
            }
            if (existingMailbox != null) {
                return "Mailbox already exists"; // Mailbox already exists
            }
            em.persist(m);

            String mailboxName = m.getMailboxName();
            // Create parents
            int dotPos;
            while ((dotPos = mailboxName.lastIndexOf('.')) != -1) {
                mailboxName = mailboxName.substring(0, dotPos);
                if (!mailboxName.equals("user")) {
                    Mailbox parent = null;
                    try {
                        parent = (Mailbox) em.createNamedQuery("Mailbox.findByName").setParameter("mailboxName", mailboxName).getSingleResult();
                    } catch (NoResultException ex) {
                    }
                    if (parent == null) {
                        Mailbox sf = new Mailbox();
                        sf.setMailboxName(mailboxName);
                        sf.setHasChildren(true);
                        em.persist(sf);
                    } else {
                        parent.setHasChildren(true);
                    }
                } else {
                    break;
                }
            }
            em.getTransaction().commit();

            return null;
        } catch (Exception ex) {
            return ex.getMessage();
        } finally {
            em.close();
        }
    }

    /**
     * Deletes a mailbox with IMAP rules. If there are inferiors to this
     * mailbox, this mailbox's messages are all deleted, and this mailbox is
     * marked as \NoSelect (noSelect field). If there are no inferiors, this
     * mailbox is deleted.
     * @param m The mailbox to delete.
     * @return <code>null</code> on success, or a failure message.
     */
    @SuppressWarnings("unchecked")
    public static String deleteMailbox(Mailbox m) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();

            m = em.find(Mailbox.class, m.getUidValidity());
            if (m == null) {
                return "Mailbox does not exist"; // Mailbox does not exist
            }

            // Check if it has sub mailboxes, and if it has, mark this mailbox as
            // \NoSelect. Otherwise, delete this mailbox.
            List<Mailbox> inferiors = (List<Mailbox>) em.createNamedQuery("Mailbox.findByNameLike").setParameter("mailboxName", m.getMailboxName().toLowerCase().replace("%", "\\%").replace("_", "\\_") + ".%").getResultList();

            em.createQuery("DELETE FROM MailMessage m WHERE m.mailbox = :mailbox").
                    setParameter("mailbox", m).executeUpdate();
            if (inferiors.isEmpty()) {
                // Has no inferiors. Now we need to check if the parent to this
                // mailbox should have the hasChildren flag set to false.
                em.remove(m);
                int dotPos = m.getMailboxName().lastIndexOf('.');
                String parentName = m.getMailboxName().substring(0, dotPos);
                if (!parentName.equals("user")) {
                    // Has a parent. Now update children flag.
                    Mailbox parent = null;
                    try {
                        parent = (Mailbox) em.createNamedQuery("Mailbox.findByName").setParameter("mailboxName", parentName).getSingleResult();
                    } catch (NoResultException ex) {
                    }
                    if (parent != null) {
                        updateMailboxChildrenFlag(em, parent);
                    }
                }
            } else {
                // Has inferiors
                m.setNoSelect(true);
            }

            em.getTransaction().commit();

            return null;
        } finally {
            em.close();
        }
    }

    /**
     * Adds some persistent IMAP flags to a mail message. This should be any of
     * the standard IMAP flag names, or a valid user defined name.
     * @param m The message to add the flag to.
     * @param flagsToAdd The flags to add to the message.
     */
    public static void addFlagsToMessage(MailMessage m, Collection<MessageFlag> flagsToAdd) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            MailMessagePK pk = new MailMessagePK();
            pk.setMailboxUidValidity(m.getMailboxUidValidity());
            pk.setMessageUid(m.getMessageUid());
            m = em.find(MailMessage.class, pk);
            if (m != null) {
                for (MessageFlag flag : flagsToAdd) {
                    MessageFlag attachedFlag = em.find(MessageFlag.class, flag.getName());
                    if (attachedFlag == null) {
                        attachedFlag = em.merge(flag);
                    }
                    m.getFlags().add(attachedFlag);
                }
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Removes some persistent IMAP flags from a mail message.
     * @param m The message to remove the flag from.
     * @param flagsToDelete The flags to remove from the message.
     */
    public static void deleteFlagsFromMessage(MailMessage m, Collection<MessageFlag> flagsToDelete) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            MailMessagePK pk = new MailMessagePK();
            pk.setMailboxUidValidity(m.getMailboxUidValidity());
            pk.setMessageUid(m.getMessageUid());
            m = em.find(MailMessage.class, pk);
            if (m != null) {
                m.getFlags().removeAll(flagsToDelete);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    /**
     * Sets the persistent IMAP flags for a mail message.
     * @param m The message to set the flags for.
     * @param flags The flags to set for the message.
     */
    public static void setMessageFlags(MailMessage m, Collection<MessageFlag> flags) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            em.getTransaction().begin();
            MailMessagePK pk = new MailMessagePK();
            pk.setMailboxUidValidity(m.getMailboxUidValidity());
            pk.setMessageUid(m.getMessageUid());
            m = em.find(MailMessage.class, pk);
            if (m != null) {
                m.getFlags().clear();
                for (MessageFlag flag : flags) {
                    MessageFlag attachedFlag = em.find(MessageFlag.class, flag.getName());
                    if (attachedFlag == null) {
                        attachedFlag = em.merge(flag);
                    }
                    m.getFlags().add(attachedFlag);
                }
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public static MailMessage getMailMessage(MailMessagePK pk) {
        EntityManager em = EMFSingleton.getEntityManager();
        try {
            return em.find(MailMessage.class, pk);
        } finally {
            em.close();
        }
    }

    /**
     * Updates the children flag for a mailbox.
     * @param em An EntityManager instance with an active transaction.
     * @param m The mailbox to update.
     */
    @SuppressWarnings("unchecked")
    private static void updateMailboxChildrenFlag(EntityManager em, Mailbox m) {
        List<Mailbox> children = (List<Mailbox>) em.createNamedQuery("Mailbox.findByNameLike").setParameter("mailboxName", m.getMailboxName().toLowerCase() + ".%").getResultList();
        m.setHasChildren(children.size() > 0);
    }
}
