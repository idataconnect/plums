/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.network;

import com.sun.grizzly.Context;
import com.sun.grizzly.ProtocolFilter;
import com.sun.grizzly.ProtocolParser;
import com.sun.grizzly.util.WorkerThread;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Grizzly filter which decides which protocol to hand off to. It also
 * handles parsing CRLF separated lines into messages, and keeps them in
 * a fifo stack until they are ready to send to the next level filter.
 *
 * This adapter class is completely decoupled from any concrete filter
 * implementation.
 *
 * @author Ben Upsavs
 */
public class GrizzlyFilterAdapter implements ProtocolFilter {

    /** Holds a single instance of each protocol filter. */
    Map<Class, ProtocolFilter> protocolFilters =
            new HashMap<Class, ProtocolFilter>();

    /** Holds chat state for each open channel associated with this filter. */
    Map<SelectableChannel, ChatState> chatStateMap = new HashMap<SelectableChannel, ChatState>();

    /**
     * Chat state holder.
     */
    class ChatState {

        /** Pending messages FIFO. */
        List<String> messages = new LinkedList<String>();
        /** Buffer for next message, until CRLF is received. */
        StringBuilder buf = new StringBuilder(256);
    }

    /** {@inheritDoc} */
    public boolean execute(Context ctx) throws IOException {
        SelectableChannel channel = ctx.getSelectionKey().channel();
        final WorkerThread workerThread = ((WorkerThread) Thread.currentThread());
        ByteBuffer buffer = workerThread.getByteBuffer();
        buffer.flip();
        if (buffer.hasRemaining()) {
            // Store incoming data in byte[]
            byte[] data = new byte[buffer.remaining()];
            int position = buffer.position();
            while (buffer.hasRemaining()) {
                buffer.get(data);
            }
            buffer.position(position);

            boolean foundAny = false;
            for (int count = 0; count < data.length; count++) {
                boolean found = false;
                if (data[count] == '\r') {
                    count++;
                    found = true;
                } else if (data[count] == '\n') {
                } else {
                    chatStateMap.get(channel).buf.append((char) data[count]);
                }

                if (found) {
                    foundAny = true;
                    // Add the message to the stack
                    chatStateMap.get(channel).messages.add(chatStateMap.get(channel).buf.toString());
                    // Reset the buffer
                    chatStateMap.get(channel).buf.setLength(0);
                }
            }

            // Check if one or more complete message lines were received
            if (foundAny) {
                ServerInstance serverInstance = (ServerInstance) ctx.getSelectorHandler().getAttribute("plumsServerInstance");

                // Fetch the single instance of this protocol filter implementation
                // or instantiate it if it hasn't been used before.
                ProtocolFilter nextFilter = nextFilter = protocolFilters.get(serverInstance.getProtocolFilterClass());
                if (nextFilter == null) {
                    try {
                        nextFilter = serverInstance.getProtocolFilterClass().newInstance();
                        protocolFilters.put(nextFilter.getClass(), nextFilter);
                    } catch (Exception ex) {
                        Logger.getLogger(getClass().getName())
                                .log(Level.SEVERE, "Instantiation failed for protocol filter {0}",
                                new Object[] {serverInstance.getProtocolFilterClass().getName(), ex});
                    }
                }

                if (nextFilter != null) {
                    ChatState chatState = chatStateMap.get(channel);
                    if (chatState != null) {
                        while (!chatState.messages.isEmpty()) {
                            // Pop the next message from the fifo stack
                            ctx.setAttribute(ProtocolParser.MESSAGE, chatState.messages.remove(0));
                            nextFilter.execute(ctx);
                        }
                    }
                }
            }
        }

        buffer.clear();

        return false;
    }

    /** {@inheritDoc} */
    public boolean postExecute(Context ctx) throws IOException {
        return true;
    }

    /**
     * Called by the server controller when a new network connection occurs.
     * A null pointer exception will be thrown if this is not called properly.
     * @param channel The channel associated with the connection.
     */
    public void newConnection(SelectableChannel channel) {
        chatStateMap.put(channel, new ChatState());
    }

    /**
     * Called by the server controller when a network connection disconnects.
     * A memory leak will occur if this is not called properly.
     * @param channel The channel for the inactive connection.
     */
    public void cleanupConnection(SelectableChannel channel) {
        chatStateMap.remove(channel);
    }
}
