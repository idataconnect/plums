/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.network;

import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;

/**
 * DNS resolver routines suitable for a long running mail server.
 * @author Ben Upsavs
 */
public class DNSResolver {

    static InitialDirContext dirContext;

    static {
        try {
            dirContext = new InitialDirContext();
        } catch (NamingException ex) {}
    }

    /**
     * Gets a set of mail server refs, which are usually MX records. In cases
     * where there are no MX records for a domain, the domain name is used as
     * an A record. When they are MX records, they are sorted by their
     * priority from smallest (highest priority) to largest (lowest priority).
     * @param domainName The domain name to fetch a set of mail server refs.
     * @return A set of mail server refs for the specified domain.
     */
    public static SortedSet<MailServerRef> getMailServerRefs(String domainName) {
        SortedSet<MailServerRef> refs = new TreeSet<MailServerRef>();
        try {
            Attributes allAttributes = dirContext.getAttributes("dns:" + domainName, new String[]{"MX"});
            Attribute mxRecordAttribute = allAttributes.get("MX");
            if (mxRecordAttribute == null) {
                // No MX records found for the specified domain. Use the domain
                // name's A record instead.
                refs.add(new MailServerRef(0, domainName, false));
            } else {
                for (int count = 0; count < mxRecordAttribute.size(); count++) {
                    String currentMxText = mxRecordAttribute.get(count).toString();
                    if (currentMxText != null) {
                        StringTokenizer st = new StringTokenizer(currentMxText, " ");
                        int priority = Integer.parseInt(st.nextToken());
                        String hostname = st.nextToken();

                        if (hostname.charAt(hostname.length() - 1) == '.') {
                            // Already has domain name info. Need to chop the dot.
                            hostname = hostname.substring(0, hostname.length() - 1);
                        } else {
                            // Needs domain info on the end.
                            hostname += "." + domainName;
                        }

                        refs.add(new MailServerRef(priority, hostname, true));
                    }
                }
            }
        } catch (NamingException ex) {
        }

        return refs;
    }

    /**
     * Resolves a host name into an IP address. If there are multiple IP
     * addresses for the given host name, one of them will be returned. This
     * method will not accept IP addresses. In other words, if the IP address
     * is already resolved, it is not correct to pass it to this method, and
     * doing so will cause undesired results.
     * @param hostname The host name to resolve.
     * @return The resolved IP address, or <code>null</code> if none were found.
     */
    public static String resolveHostname(String hostname) {
        String ipAddress = null;
        try {
            // TODO allow DNS servers to be set, or to use the platform's DNS
            // configuration (which is what is done now).
            Attributes allAttributes = dirContext.getAttributes("dns:/" + hostname, new String[]{"A"});
            Attribute aRecordAttribute = allAttributes.get("A");
            if (aRecordAttribute == null) {
                // No A records were found.
            } else {
                ipAddress = aRecordAttribute.get(0).toString();
            }
        } catch (NamingException ex) {
        }

        return ipAddress;
    }
}
