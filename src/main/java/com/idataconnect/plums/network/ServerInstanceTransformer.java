/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.network;

import com.idataconnect.plums.entity.ProtocolConfig;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A helper class which transforms entities from the database into objects
 * designed to be used by the mail server, and back again.
 * @author Ben Upsavs
 */
public class ServerInstanceTransformer {

    /**
     * Transforms a {@link com.idataconnect.plums.entity.ProtocolConfig} entity
     * into a {@link ServerInstance} object. The user would generally
     * fetch the entity class using the EntityManager and then pass that class
     * here to be transformed into an object that is used by the mail server.
     * @param entity The entity class.
     * @return An instance of a subclass of <code>ServerInstance</code>, or
     * <code>null</code> if that instance could not be instantiated.
     */
    public static ServerInstance transform(ProtocolConfig entity) {
        ServerInstance instance;
        try {
            instance = (ServerInstance) Class.forName(entity.getInstanceClassName()).newInstance();
        } catch (Exception ex) {
            Logger.getLogger(ServerInstanceTransformer.class.getName())
                    .log(Level.WARNING, "Unable to instantiate protocol implementaton: {0}",
                    entity.getInstanceClassName());
            return null;
        }

        NetworkAttributes networkAttributes = new NetworkAttributes();
        networkAttributes.setHostname(entity.getHostname());
        networkAttributes.setAddresses(ProtocolConfig.extractInetAddresses(entity.getAddresses()));
        networkAttributes.setPorts(ProtocolConfig.extractPorts(entity.getPorts()));
        networkAttributes.setBacklog(entity.getBacklog());
        networkAttributes.setSocketTimeout(entity.getSocketTimeout());
        instance.setNetworkAttributes(networkAttributes);

        return instance;
    }

    /**
     * Transforms multiple {@link ProtocolConfig} entities into
     * {@link ServerInstance} objects. Note that if any of the server instances
     * are unable to be instantiated, their element in the array will be
     * <code>null</code>.
     * @param entities The entities to transform.
     * @return An array of transformed server instance objects.
     */
    public static ServerInstance[] transform(ProtocolConfig[] entities) {
        ServerInstance[] instances = new ServerInstance[entities.length];

        for (int count = 0; count < entities.length; count++) {
            instances[count] = transform(entities[count]);
        }

        return instances;
    }

    /**
     * Transforms a server instance object into a protocol config entity.
     * @param serverInstance The server instance object.
     * @return The protocol config entity.
     */
    public static ProtocolConfig transform(ServerInstance serverInstance) {
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setInstanceClassName(serverInstance.getClass().getName());

        StringBuilder sb = new StringBuilder();
        for (int count = 0; count < serverInstance.getNetworkAttributes().getAddresses().length; count++) {
            if (count != 0) {
                sb.append(",");
            }

            sb.append(serverInstance.getNetworkAttributes().getAddresses()[count].getHostAddress());
        }
        protocolConfig.setAddresses(sb.toString());

        sb = new StringBuilder();
        for (int count = 0; count < serverInstance.getNetworkAttributes().getPorts().length; count++) {
            if (count != 0) {
                sb.append(",");
            }

            sb.append(serverInstance.getNetworkAttributes().getPorts()[count]);
        }
        protocolConfig.setPorts(sb.toString());

        protocolConfig.setHostname(serverInstance.getNetworkAttributes().getHostname());
        protocolConfig.setBacklog(serverInstance.getNetworkAttributes().getBacklog());
        protocolConfig.setSocketTimeout(serverInstance.getNetworkAttributes().getSocketTimeout());

        return protocolConfig;
    }

    /**
     * Transforms multiple server instance objects into protocol config entities.
     * @param serverInstances The server instance object.
     * @return The protocol config objects.
     */
    public static ProtocolConfig[] transform(ServerInstance[] serverInstances) {
        ProtocolConfig[] protocolConfigs = new ProtocolConfig[serverInstances.length];

        for (int count = 0; count < serverInstances.length; count++) {
            protocolConfigs[count] = transform(serverInstances[count]);
        }

        return protocolConfigs;
    }

    /**
     * This method will transform instances of the specified class. This is useful
     * for transforming only one type of network instance (e.g. POP3) when you
     * have all types in one collection.
     * 
     * @param <T> The server instance class type.
     * @param classToExtract The network instance class.
     * @param entities An array of network instances.
     * @return A list of server instances.
     */
    public static <T extends ServerInstance> List<T> transformInstancesByClass(Class<T> classToExtract, ProtocolConfig[] entities) {
        List<T> list = new LinkedList<T>();
        for (ProtocolConfig entity : entities) {
            ServerInstance instance = transform(entity);
            if (classToExtract.isInstance(instance)) {
                list.add(classToExtract.cast(instance));
            }
        }

        return list;
    }
}
