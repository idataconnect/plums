/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.network;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Arrays;

/**
 * Network attributes for an SMTP, POP, or IMAP instance.
 * @author Ben Upsavs
 */
public class NetworkAttributes implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * The host name to advertise to clients when they connect.
     */
    private String hostname;

    /**
     * The addresses to listen on, or <code>null</code> to listen on all
     * addresses.
     */
    private InetAddress[] addresses;

    /**
     * The ports to listen on.
     */
    private short[] ports;

    /**
     * The connection backlog, or <code>null</code> to use the system default
     * backlog.
     */
    private int backlog;

    /**
     * The socket timeout, or <code>0</code> to use the
     * default timeout.
     */
    private int socketTimeout;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.hostname != null ? this.hostname.hashCode() : 0);
        hash = 53 * hash + Arrays.deepHashCode(this.addresses);
        hash = 53 * hash + Arrays.hashCode(this.ports);
        hash = 53 * hash + this.backlog;
        hash = 53 * hash + this.socketTimeout;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NetworkAttributes other = (NetworkAttributes) obj;
        if ((this.hostname == null) ? (other.hostname != null) : !this.hostname.equals(other.hostname)) {
            return false;
        }
        if (!Arrays.deepEquals(this.addresses, other.addresses)) {
            return false;
        }
        if (!Arrays.equals(this.ports, other.ports)) {
            return false;
        }
        if (this.backlog != other.backlog) {
            return false;
        }
        if (this.socketTimeout != other.socketTimeout) {
            return false;
        }
        return true;
    }

    /**
     * Gets the hostname to advertise to clients when they connect.
     * @return The hostname to advertise to clients when they connect.
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Sets the hostname to advertise to clients when they connect.
     * @param hostname The hostname to advertise to clients when they connect.
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * Gets the addresses to bind the server socket to.
     * @return The addresses to bind the server socket to.
     */
    public InetAddress[] getAddresses() {
        return addresses;
    }

    /**
     * Sets the addresses to bind the server socket to.
     * @param addresses The addresses to bind the server socket to.
     */
    public void setAddresses(InetAddress[] addresses) {
        this.addresses = addresses;
    }

    /**
     * Gets the ports to bind the server socket to.
     * @return The ports to bind the server socket to.
     */
    public short[] getPorts() {
        return ports;
    }

    /**
     * Sets the ports to bind the server socket to.
     * @param ports The ports to bind the server socket to.
     */
    public void setPorts(short[] ports) {
        this.ports = ports;
    }

    /**
     * Gets the server socket backlog.
     * @return The server socket backlog.
     */
    public int getBacklog() {
        return backlog;
    }

    /**
     * Sets the server socket backlog.
     * @param backlog The server socket backlog.
     */
    public void setBacklog(int backlog) {
        this.backlog = backlog;
    }

    /**
     * Gets the server socket timeout.
     * @return The post authentication server socket timeout.
     */
    public int getSocketTimeout() {
        return socketTimeout;
    }

    /**
     * Sets the server socket timeout.
     * @param postAuthenticationTimeout The post authentication server socket timeout.
     */
    public void setSocketTimeout(int postAuthenticationTimeout) {
        this.socketTimeout = postAuthenticationTimeout;
    }
}
