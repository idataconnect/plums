/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.network;

/**
 * Represents either an MX record or an A record, either of which may be
 * used to refer to a mail server. An A record may only refer to
 * a mail server if there are no MX records listed for the domain.
 * @author Ben Upsavs
 */
public class MailServerRef implements Comparable {

    private int priority;
    private String hostname;
    private boolean mxRecord;

    /**
     * Creates an empty instance of MailServerRef.
     */
    public MailServerRef() {
    }

    /**
     * Creates a populated instance of MailServerRef.
     * @param priority The priority, if an MX record.
     * @param hostname The hostname.
     * @param isMxRecord Whether this represents an MX record, or an A record.
     */
    public MailServerRef(int priority, String hostname, boolean isMxRecord) {
        this.priority = priority;
        this.hostname = hostname;
        this.mxRecord = isMxRecord;
    }

    /**
     * Gets the host name of the remote mail server reference.
     * @return The host name of the remote mail server reference.
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Sets the host name of the remote mal server reference.
     * @param hostname The host name of the remote mail server reference.
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * If an MX record, gets the priority (lower is preferred). If this is not
     * an MX record, the priority is undefined.
     * @return The MX record priority.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * If an MX record, sets the priotity (lower is preferred). If this is not
     * an MX record, the priority is undefined.
     * @param priority The MX record priority.
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Gets whether this object represents an MX record. If not, it represents
     * an A record.
     * @return Whether this object represents an MX record, or an A record.
     */
    public boolean isMxRecord() {
        return mxRecord;
    }

    /**
     * Sets whether this object represents an MX record. If not, it represents
     * an A record.
     * @param isMxRecord Whether this object represents an MX record, or an A
     * record.
     */
    public void setMxRecord(boolean isMxRecord) {
        this.mxRecord = isMxRecord;
    }

    /**
     * Compares this mail server ref to another. They are compared solely by
     * priority. This is meaningless when the ref does not represent an MX
     * record, since A records have no concept of priority.
     * @param o The other mail server ref object.
     * @return {@inheritDoc}
     */
    public int compareTo(Object o) {
        if (o instanceof MailServerRef) {
            MailServerRef other = (MailServerRef) o;
            return new Integer(getPriority()).compareTo(new Integer(other.getPriority()));
        } else {
            return 0;
        }
    }
}
