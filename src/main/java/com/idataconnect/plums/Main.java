/*
 * Copyright (c) 2008-2009, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums;

import com.idataconnect.plums.spool.MailMessageQueueManager;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The main class, which performs a normal server startup, and optionally
 * to start server configuration.
 * @author Ben Upsavs
 */
public class Main {

    // TODO update with property file containing version info
    // TODO use maven-runtime plugin to obtain versions
    public static final String VERSION = "1.0.0-SNAPSHOT";
    public static final int COPYRIGHT_START = 2008;
    public static final int COPYRIGHT_END = 2013;
    public static final String PRODUCT_NAME = "plums";
    public static final String PRODUCT_COMPANY_NAME = "i Data Connect!";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Start the entity manager
        EMFSingleton.init();

        // Insert default values into the database if this is a new install
        Persistence.insertDefaultValues(false);

        // Check if we should run the configuration program or the server
        if (args.length > 0 && args[1].equalsIgnoreCase("-configure"))
            ConfigMain.enterCommandLineInterfaceLoop();
        else { // Run the server
            try {
                // Add a shutdown hook, to shut the server down cleanly when
                // the JVM exits.
                Runtime.getRuntime().addShutdownHook(new Thread() {

                    @Override
                    public void run() {
                        try {
                            ServerController.stop();
                        } catch (Exception ex) {
                        }
                    }
                });
                MailMessageQueueManager.init();
                ServerController.start();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "I/O error while starting server", ex);
            }
        }
    }
}
