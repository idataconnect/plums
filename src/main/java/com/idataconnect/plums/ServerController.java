/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums;

import com.idataconnect.plums.entity.ProtocolConfig;
import com.idataconnect.plums.network.*;
import com.sun.grizzly.*;
import com.sun.grizzly.filter.ReadFilter;
import com.sun.grizzly.util.DefaultThreadPool;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller which starts and stops the server.
 * @author Ben Upsavs
 */
public class ServerController {

    public static Controller grizzlyController;
    public static final GrizzlyFilterAdapter filterAdapter = new GrizzlyFilterAdapter();
    static Thread stdinReaderThread;

    private static final Logger log = Logger.getLogger(ServerController.class.getName());

    /**
     * Starts the server.
     * @throws java.io.IOException If the Grizzly framework has a problem.
     */
    public static void start() throws IOException {
        grizzlyController = new Controller();

        // Make sure the entity manager is running
        EMFSingleton.initIfClosed();

        // Bump the server start count
        Persistence.setGlobalConfigValue("serverStartCount", String.valueOf(Long.parseLong(Persistence.getGlobalConfigValue("serverStartCount", "1")) + 1));
        log.log(Level.FINE, "Server start count: {0}", Persistence.getGlobalConfigValue("serverStartCount", "Unknown"));

        // Set up a DefaultSelectionKeyHandler which we will use for all
        // SelectorHandlers.
        DefaultSelectionKeyHandler selectionKeyHandler = new DefaultSelectionKeyHandler();

        log.info("Reading server configuration");
        // Fetch the protocol config entities and convert them to server instances.
        List<ProtocolConfig> protocolConfigs = Persistence.getAllProtocolConfigs();
        if (protocolConfigs.isEmpty()) {
            log.warning("No protocols configured.");
            return;
        }
        ServerInstance[] serverInstances = new ServerInstance[protocolConfigs.size()];
        for (int count = 0; count < serverInstances.length; count++)
            serverInstances[count] = ServerInstanceTransformer.transform(protocolConfigs.get(count));

        int addressMultiplier = 1;
        int portMultiplier = 1;

        // Work out how many sockets need to be started, depending on how
        // many IP addresses and ports were chosen, and them all to the
        // Grizzly server configuration.
        for (final ServerInstance serverInstance : serverInstances) {
            NetworkAttributes na = serverInstance.getNetworkAttributes();
            InetAddress[] addresses = na.getAddresses();
            short[] ports = na.getPorts();

            if (addresses.length > 1)
                addressMultiplier = addresses.length;
            if (ports.length > 1)
                portMultiplier = ports.length;

            for (int aCount = 0; aCount < addressMultiplier; aCount++) {
                for (int pCount = 0; pCount < portMultiplier; pCount++) {
                    TCPSelectorHandler tcpSelector;

                    try {
                        final PlumsGrizzlyFilter protocolFilter = serverInstance.getProtocolFilterClass().newInstance();
                        tcpSelector = new TCPSelectorHandler() {

                            @Override
                            public void closeChannel(SelectableChannel channel) {
                                filterAdapter.cleanupConnection(channel);
                                try {
                                    if (channel instanceof SocketChannel)
                                        protocolFilter.cleanupConnection((SocketChannel) channel);
                                } catch (Exception ex) {
                                    log.log(Level.SEVERE, "Unable to invoke cleanup method on protocol filter {0}: {1}",
                                            new Object[] {serverInstance.getProtocolFilterClass(), ex});
                                }
                                super.closeChannel(channel);
                            }

                            @Override
                            public SelectableChannel acceptWithoutRegistration(SelectionKey key) throws IOException {
                                SocketChannel ret = (SocketChannel) super.acceptWithoutRegistration(key);
                                filterAdapter.newConnection(ret);
                                try {
                                    protocolFilter.newConnection(ret, serverInstance, ret.socket().getInetAddress());
                                } catch (Exception ex) {
                                    log.log(Level.SEVERE, "Unable to invoke new connection method on protocol filter {0}: {1}",
                                            new Object[] {serverInstance.getProtocolFilterClass(), ex});
                                }
                                return ret;
                            }
                        };

                        tcpSelector.setAttribute("plumsServerInstance", serverInstance);

                        if (addresses.length != 0)
                            tcpSelector.setInet(addresses[aCount]);
                        tcpSelector.setPort(ports[pCount]);
                        tcpSelector.setSocketTimeout(serverInstance.getNetworkAttributes().getSocketTimeout());
                        tcpSelector.setSsBackLog(serverInstance.getNetworkAttributes().getBacklog());
                        grizzlyController.addSelectorHandler(tcpSelector);
                        tcpSelector.setSelectionKeyHandler(selectionKeyHandler);
                        selectionKeyHandler.setTimeout(1000L * 60 * 30); // 30 minute timeout
                        log.log(Level.INFO, "Starting {0} on address {1}, port {2}",
                                new Object[] {serverInstance.getProtocolName(),
                                (addresses.length == 0 ? "All" : addresses[aCount]),
                                String.valueOf(ports[pCount])});
                    } catch (Exception ex) {
                        log.log(Level.SEVERE, "Failed to start {0} on {1}:{2} ({3})",
                                new Object[] {
                                serverInstance.getProtocolName(),
                                addresses[aCount],
                                ports[pCount],
                                ex});
                    }
                }
            }
        }

        grizzlyController.setThreadPool(new DefaultThreadPool());

        ProtocolChainInstanceHandler pciHandler =
                new ProtocolChainInstanceHandler() {

                    private final ProtocolChain protocolChain = new DefaultProtocolChain();

                    public ProtocolChain poll() {
                        return protocolChain;
                    }

                    public boolean offer(ProtocolChain instance) {
                        return true;
                    }
                };
        grizzlyController.setProtocolChainInstanceHandler(pciHandler);

        ProtocolChain protocolChain = pciHandler.poll();
        ((DefaultProtocolChain) protocolChain).setContinuousExecution(true);
        protocolChain.addFilter(new ReadFilter());
        protocolChain.addFilter(filterAdapter);

        if (stdinReaderThread == null) {
            log.finer("Starting STDIN reader");
            stdinReaderThread = new Thread("STDIN reader") {

                @Override
                public void run() {
                    System.err.println("Type q to quit or r to restart");
                    BufferedReader stdinReader = new BufferedReader(new InputStreamReader(System.in));
                    int c = 0;
                    while (c != 'q') {
                        try {
                            String line = stdinReader.readLine();
                            if (line.length() > 0)
                                c = line.charAt(0);

                            if (c == 'r') {
                                System.err.println("Restarting server...");
                                // Restart the server in a new thread since
                                // the stdin reader thread is a daemon thread
                                // and it won't keep the program running when
                                // Grizzly is stopped.
                                Thread restarterThread = new Thread("Server restarter") {

                                    @Override
                                    public void run() {
                                        try {
                                            ServerController.restart();
                                        } catch (IOException ex) {
                                            log.log(Level.WARNING, "Error while restarting server", ex);
                                        }
                                    }
                                };
                                restarterThread.setDaemon(false);
                                restarterThread.start();
                            }
                        } catch (IOException ex) {
                        }
                    }
                    System.err.println("Stopping server");

                    try {
                        ServerController.stop();
                    } catch (IOException ex) {}
                    System.exit(0);
                }
            };
            stdinReaderThread.setDaemon(true);
            stdinReaderThread.start();
        }

        grizzlyController.addStateListener(new ControllerStateListener() {

            public void onStarted() {
            }

            public void onReady() {
                log.info("Server ready to accept connections");
            }

            public void onStopped() {
                log.info("Server stopped");
            }

            public void onException(Throwable ex) {
                log.log(Level.SEVERE, "Grizzly threw an exception", ex);
            }

        });
        grizzlyController.start();
    }

    /**
     * Cleanly stops the server. This includes stopping Grizzly, and closing
     * the entity manager factory.
     * @throws java.io.IOException If Grizzly has a problem.
     */
    public static void stop() throws IOException {
        // Stop grizzly
        try {
            grizzlyController.stop();
        } catch (IOException ex) {
            throw ex;
        } catch (Exception ex) {}

        // Cleanly close the entity manager
        EMFSingleton.close();
    }

    /**
     * Cleanly stops, and then starts the server.
     * @throws java.io.IOException If Grizzly has a problem.
     */
    public static void restart() throws IOException {
        stop();
        start();
    }
}
