/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.pop;

import com.idataconnect.plums.entity.MailMessage;

/**
 * Response from {@link POPProtocolHandler#processCommand}.
 * @author Ben Upsavs
 */
public class POPCommandResponse {

    private String responseMessage;
    private boolean disconnect;
    private MailMessage sendMessage;
    private int linesToSend = -1;

    /**
     * Creates an empty instance of POPCommandResponse.
     */
    public POPCommandResponse() {
    }

    /**
     * Creates an instance of POPCommandResponse with the specified message,
     * without the intention to disconnect the socket after the response message
     * is sent.
     * @param responseMessage The response to send to the client.
     */
    public POPCommandResponse(String responseMessage) {
        this(responseMessage, false);
    }

    /**
     * Creates an instance of POPCommandResponse with the specified message
     * and whether to disconnect the socket.
     * @param responseMessage The response to send to the client.
     * @param disconnect Whether to disconnect the socket.
     */
    public POPCommandResponse(String responseMessage, boolean disconnect) {
        this.responseMessage = responseMessage;
        this.disconnect = disconnect;
    }

    /**
     * Creates an instance of POPCommandResponse, instructing the filter
     * to send the specified message to the client.
     * @param message The message to send to the client.
     */
    public POPCommandResponse(MailMessage message) {
        sendMessage = message;
    }

    /**
     * Creates an instance of POPCommandResponse, instructing the filter
     * to send all headers, and then <code>linesToSend</code> of lines of
     * the message, or the full message, whichever it smaller.
     * @param message The message to send to the client.
     * @param linesToSend The maximum number of lines to send, after the header.
     */
    public POPCommandResponse(MailMessage message, int linesToSend) {
        sendMessage = message;
        this.linesToSend = linesToSend;
    }

    /**
     * Gets whether the server should disconnect the client after sending
     * the current response line.
     * @return Whether the client should be disconnected.
     */
    public boolean isDisconnect() {
        return disconnect;
    }

    /**
     * Sets whether the server should disconnect the client after sending
     * the current response line.
     * @param disconnect Whether the client should be disconnected.
     */
    public void setDisconnect(boolean disconnect) {
        this.disconnect = disconnect;
    }

    /**
     * Gets the response line that should be sent to the client. This is a
     * POP3 protocol response. If <code>getSendMessage()</code> does not
     * return <code>null</code>, this value should be ignored, and the
     * message should be streamed to the client instead.
     * @return The response line to send to the client.
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * Sets the response line that should be sent to the client. This is a
     * POP3 protocol response. If <code>getSendMessage()</code> does not
     * return <code>null</code>, this value will be ignored, and the
     * message will be streamed to the client instead.
     * @param responseMessage The response line to send to the client.
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * Gets the message that will be streamed to the client. If this is set,
     * <code>getResponseMessage()</code> will be ignored.
     * @return The message that will be streamed to the client.
     */
    public MailMessage getSendMessage() {
        return sendMessage;
    }

    /**
     * Sets the message that will be streamed to the client. If this is set,
     * <code>getResponseMessage()</code> will be ignored.
     * @param sendMessage The message to stream to the client.
     */
    public void setSendMessage(MailMessage sendMessage) {
        this.sendMessage = sendMessage;
    }

    /**
     * Gets the number of lines of the requested message that should be streamed
     * to the client. This occurs due to a POP3 TOP command.
     * @return The number of lines of the message that should be streamed to
     * the client.
     */
    public int getLinesToSend() {
        return linesToSend;
    }

    /**
     * Sets the number of lines of the requested message that should be
     * streamed to the client. This occurs due to a POP3 TOP command.
     * @param linesToSend The number of lines of the message that should be
     * streamed to the client.
     */
    public void setLinesToSend(int linesToSend) {
        this.linesToSend = linesToSend;
    }
}
