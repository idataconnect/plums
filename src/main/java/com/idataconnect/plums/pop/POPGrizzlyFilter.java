/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.pop;

import com.idataconnect.plums.network.PlumsGrizzlyFilter;
import com.idataconnect.plums.network.ServerInstance;
import com.sun.grizzly.Context;
import com.sun.grizzly.ProtocolParser;
import com.sun.grizzly.util.WorkerThread;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Grizzly <code>ProtocolFilter</code> implementation for POP3.
 * @author Ben Upsavs
 */
public class POPGrizzlyFilter implements PlumsGrizzlyFilter {

    public static final Map<SelectableChannel, POPConnectionState> connections = new HashMap<SelectableChannel, POPConnectionState>();

    public void cleanupConnection(SocketChannel channel) {
        try {
            POPConnectionState s = connections.get(channel);
            if (s != null) {
                POPProtocolHandler.removeMailboxLock(s);
                Logger log = Logger.getLogger(POPGrizzlyFilter.class.getName());
                log.log(Level.INFO, "POP: Disconnected {0}", s.getRemoteHost().getHostAddress());
                log.log(Level.FINER, "There are now {0} active POP connections", connections.size());
            }
        } finally {
            connections.remove(channel);
        }
    }

    public void newConnection(SocketChannel channel, ServerInstance serverInstance, InetAddress remoteHost) {
        POPConnectionState state = new POPConnectionState();
        state.setApopPid(channel.hashCode());
        state.setApopTimestamp((int) (System.currentTimeMillis() / 1000));
        state.setServerInstance(serverInstance);
        state.setRemoteHost(remoteHost);

        String greeting = "+OK " + serverInstance.getNetworkAttributes().getHostname()
                + " POP3 server ready <"
                + state.getApopPid() + "." + state.getApopTimestamp()
                + "@" + serverInstance.getNetworkAttributes().getHostname()
                + ">\r\n";
        ByteBuffer buf = ByteBuffer.wrap(greeting.getBytes());
        try {
            while (buf.hasRemaining()) {
                channel.write(buf);
            }
        } catch (IOException ex) {
            Logger.getLogger(POPGrizzlyFilter.class.getName()).log(Level.WARNING,
                    "I/O error while sending POP greeting", ex);
        }

        connections.put(channel, state);
        Logger log = Logger.getLogger(POPGrizzlyFilter.class.getName());
        log.log(Level.INFO, "POP: Connection from {0}", remoteHost.getHostAddress());
        log.log(Level.FINER, "There are now {0} active POP connections", connections.size());
    }

    public boolean execute(Context ctx) throws IOException {
        String s = (String) ctx.getAttribute(ProtocolParser.MESSAGE);
        if (s != null) {
            WorkerThread workerThread = (WorkerThread) Thread.currentThread();
            SelectableChannel channel = ctx.getSelectionKey().channel();
            ByteBuffer buf = workerThread.getByteBuffer();
            buf.clear();

            POPCommandResponse response = POPProtocolHandler.processCommand(s, connections.get(channel));

            if (response.getSendMessage() == null) {
                // Normal response - send what's in response.getResponseMessage()
                for (int pos = 0; pos < response.getResponseMessage().getBytes().length; pos += buf.capacity()) {
                    buf.put(response.getResponseMessage().getBytes(), pos, Math.min(buf.capacity(), response.getResponseMessage().getBytes().length - pos));
                    buf.flip();
                    ((SocketChannel) channel).write(buf);
                    buf.clear();
                }
            } else {
                // The client has requested a message, so we stream it here
                // instead of loading it all in memory.
                FileInputStream fis = response.getSendMessage().getInputStream();
                FileChannel fc = fis.getChannel();
                buf.clear();

                // First send the POP response line.
                buf.put("+OK Message follows\r\n".getBytes());
                buf.flip();
                ((SocketChannel) channel).write(buf);
                buf.clear();

                if (response.getLinesToSend() == -1) {
                    // They used the RETR command, and want the complete message.
                    while (fc.read(buf) != -1) {
                        buf.flip();
                        while (buf.hasRemaining())
                            ((SocketChannel) channel).write(buf);
                        buf.clear();
                    }
                } else {
                    // They used the TOP command, and only want part of the message.
                    int numberOfNewlines = 0;
                    boolean lastCharWasNewline = false;
                    boolean pastHeaders = false;
                    while ((response.getLinesToSend() == 0 || numberOfNewlines < response.getLinesToSend()) && (fc.read(buf) != -1)) {
                        buf.flip();
                        while (buf.hasRemaining()) {
                            byte b = buf.get();
                            if (b == '\n') {
                                if (lastCharWasNewline)
                                    pastHeaders = true;
                                else
                                    lastCharWasNewline = true;

                                if (pastHeaders) {
                                    if (numberOfNewlines++ == response.getLinesToSend())
                                        break;
                                }
                            } else if (b == '\r') {
                                // Ignore \r, since we are looking for newlines
                            } else {
                                lastCharWasNewline = false;
                            }
                        }

                        // flipping either entire buffer or at last requested line.
                        buf.limit(buf.position());
                        buf.position(0);

                        while (buf.hasRemaining())
                            ((SocketChannel) channel).write(buf);
                        buf.clear();
                    }
                }

                buf.put(".\r\n".getBytes());
                buf.flip();
                while (buf.hasRemaining())
                    ((SocketChannel) channel).write(buf);
                buf.clear();

                fc.close();
                fis.close();
            }

            if (response.isDisconnect())
                ctx.getSelectorHandler().closeChannel(channel);
        }

        return false;
    }

    public boolean postExecute(Context ctx) throws IOException {
        return true;
    }
}
