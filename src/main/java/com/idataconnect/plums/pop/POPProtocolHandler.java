/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.pop;

import com.idataconnect.plums.Main;
import com.idataconnect.plums.MessageDigestUtils;
import com.idataconnect.plums.Persistence;
import com.idataconnect.plums.entity.MailMessage;
import com.idataconnect.plums.entity.MailUser;
import com.idataconnect.plums.entity.Mailbox;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.Base64OutputStream;

/**
 * POP protocol handler.
 * @author Ben Upsavs
 */
public class POPProtocolHandler {

    private static final String[] quitCommandOnly = new String[]{"QUIT"};
    private static final String[] preAuthenticationCommands = new String[]{"QUIT", "CAPA", "USER", "APOP", "AUTH"};
    private static final String[] prePassCommands = new String[]{"QUIT", "PASS"};
    private static final String[] transactionCommands = new String[]{"QUIT", "CAPA", "LIST", "RETR", "TOP", "DELE", "UIDL", "NOOP", "RSET", "STAT"};
    private static final POPCommandResponse unknownCommandResponse = new POPCommandResponse("-ERR Unknown Command\r\n", false);
    private static final Map<String, POPConnectionState> lockedMailboxes = new HashMap<String, POPConnectionState>();

    public static boolean mailboxHasLock(String mailboxName) {
        return lockedMailboxes.containsKey(mailboxName);
    }

    /**
     * If possible, obtains a POP3 mailbox lock. It should be used on the
     * INBOX when a user logs in.
     * @param mailboxName The name of the mailbox to lock.
     * @param state The connection state which the lock is associated with.
     * @return Whether or not a lock could be obtained.
     */
    public static boolean getMailboxLock(String mailboxName, POPConnectionState state) {
        // Attempt to reduce synchronization overhead by using double checked
        // locking
        if (lockedMailboxes.containsKey(mailboxName))
            return false;
        synchronized (lockedMailboxes) {
            if (lockedMailboxes.containsKey(mailboxName))
                return false;

            lockedMailboxes.put(mailboxName, state);
            Logger.getLogger(POPProtocolHandler.class.getName()).log(Level.FINEST, "There are now {0} POP mailbox locks", lockedMailboxes.size());
            try {
                state.setLockedMailbox((Mailbox) Persistence.getMailboxByName(mailboxName).clone());
            } catch (CloneNotSupportedException ex) {
            }
            return true;
        }
    }

    /**
     * Removes an existing mailbox lock, if one exists.
     * @param state The connection state that the lock is associated with.
     * @return Whether or not the lock existed.
     */
    public static boolean removeMailboxLock(POPConnectionState state) {
        String keyToRemove = null;
        for (String key : lockedMailboxes.keySet()) {
            if (lockedMailboxes.get(key).equals(state)) {
                keyToRemove = key;
                break;
            }
        }

        if (keyToRemove != null) {
            synchronized (lockedMailboxes) {
                lockedMailboxes.remove(keyToRemove);
                Logger.getLogger(POPProtocolHandler.class.getName()).log(Level.FINEST, "There are now {0} POP mailbox locks", lockedMailboxes.size());
                return true;
            }
        }

        return false;
    }

    /**
     * Process a POP3 command.
     * @param command The command sent from the client
     * @param state The state associated with this connection
     * @return The command response.
     */
    public static POPCommandResponse processCommand(String command, POPConnectionState state) {
        boolean error = false;
        POPCommandResponse response = null;

        if (command.length() == 0) {
            response = unknownCommandResponse;
            error = true;
        } else {
            String[] allowedCommands = quitCommandOnly;
            boolean allowed = false;

            if (state.getMainState().equals(POPConnectionState.MainState.AUTHENTICATING)
                    && state.getAuthState().equals(POPConnectionState.AuthState.CRAM_MD5_RESPONSE)) {
                if (command.equals("*")) {
                    response = new POPCommandResponse("-ERR Authentication aborted\r\n", false);
                } else {
                    String token = null;
                    try {
                        Base64InputStream is = new Base64InputStream(new ByteArrayInputStream(command.getBytes("UTF-8")));
                        ByteArrayOutputStream os = new ByteArrayOutputStream((int) (command.length() * 0.7));
                        int b;
                        while ((b = is.read()) != -1) {
                            os.write(b);
                        }
                        is.close();
                        os.close();
                        token = os.toString("UTF-8");
                    } catch (Exception ex) {
                        response = new POPCommandResponse("-ERR Invalid authentication token\r\n", false);
                        state.setMainState(POPConnectionState.MainState.PRE_AUTHENTICATION);
                        state.setAuthState(POPConnectionState.AuthState.NONE);
                        error = true;
                    }

                    if (!error) {
                        StringTokenizer st = new StringTokenizer(token, " ");
                        if (!st.hasMoreTokens()) {
                            response = new POPCommandResponse("-ERR Invalid authentication token\r\n", false);
                            state.setMainState(POPConnectionState.MainState.PRE_AUTHENTICATION);
                            state.setAuthState(POPConnectionState.AuthState.NONE);
                            error = true;
                        } else {
                            String username = st.nextToken();

                            if (!st.hasMoreTokens()) {
                                response = new POPCommandResponse("-ERR Invalid authentication token\r\n", false);
                                state.setMainState(POPConnectionState.MainState.PRE_AUTHENTICATION);
                                state.setAuthState(POPConnectionState.AuthState.NONE);
                                error = true;
                            } else {
                                boolean authenticated = false;

                                MailUser u = Persistence.getUserByEmailAddress(username);
                                if (u != null) {
                                    String challenge = "<" + state.getApopPid()
                                            + "." + state.getCramMd5Timestamp()
                                            + "@" + state.getServerInstance().getNetworkAttributes().getHostname()
                                            + ">";

                                    String digest = MessageDigestUtils.hmacMd5(challenge, u.getPassword());
                                    String decodedCorrectAnswer = username + " " + digest;
                                    ByteArrayOutputStream os = new ByteArrayOutputStream((int) (decodedCorrectAnswer.length() * 1.4));
                                    Base64OutputStream b64os = new Base64OutputStream(os);
                                    String correctAnswer = null;
                                    try {
                                        b64os.write(decodedCorrectAnswer.getBytes("UTF-8"));
                                        b64os.close();
                                        os.close();
                                        correctAnswer = os.toString("UTF-8");
                                    } catch (Exception ex) {
                                        // Can't encode Base64 :(
                                        throw new RuntimeException(ex);
                                    }
                                    if (command.equals(correctAnswer))
                                        authenticated = true;
                                }

                                if (authenticated) {
                                    if (getMailboxLock("user." + u.getEmailAddress(), state)) {
                                        state.setAuthenticatedUsername(u.getEmailAddress());
                                        state.setMainState(POPConnectionState.MainState.TRANSACTION);
                                        state.setAuthState(POPConnectionState.AuthState.AUTHENTICATED);
                                        state.setMainState(POPConnectionState.MainState.TRANSACTION);
                                        response = new POPCommandResponse("+OK Authenticated. Mailbox locked and ready.\r\n", false);
                                    } else {
                                        response = new POPCommandResponse("-ERR [IN-USE] Mailbox is locked by another thread\r\n", false);
                                    }
                                } else {
                                    response = new POPCommandResponse("-ERR [AUTH] Authentication failed\r\n", false);
                                    state.setMainState(POPConnectionState.MainState.PRE_AUTHENTICATION);
                                    state.setAuthState(POPConnectionState.AuthState.NONE);
                                    error = true;
                                }
                            }
                        }
                    }
                }
            } else if (command.trim().length() == 0) {
                response = new POPCommandResponse("-ERR Received nothing\r\n", false);
                error = true;
            } else {
                switch (state.getMainState()) {
                    case PRE_AUTHENTICATION:
                        allowedCommands = preAuthenticationCommands;
                        break;
                    case PRE_PASS:
                        allowedCommands = prePassCommands;
                        break;
                    case TRANSACTION:
                        allowedCommands = transactionCommands;
                        break;
                }

                // Split the command into tokens and grab the first one
                StringTokenizer st = new StringTokenizer(command, " ");
                String firstCommandPart = st.nextToken();

                // Check if the command is currently allowed
                for (String allowedCommand : allowedCommands) {
                    if (allowedCommand.equalsIgnoreCase(firstCommandPart)) {
                        allowed = true;
                        break;
                    }
                }

                if (!allowed) {
                    // If the command isn't allowed, send them an unknown command error
                    error = true;
                    response = unknownCommandResponse;
                } else if (firstCommandPart.equalsIgnoreCase("QUIT")) {
                    // Delete messages marked for deletion and disconnect
                    // the client.
                    String mailboxName = "user." + state.getAuthenticatedUsername();
                    Map<Integer, Boolean> seqMap = state.getMarkedForDeletionMap();
                    Map<Integer, Boolean> uidMap = new HashMap<Integer, Boolean>();
                    for (Integer seq : seqMap.keySet()) {
                        MailMessage m = state.getLockedMailbox().getMessages().get(seq - 1);
                        uidMap.put(m.getMessageUid(), Boolean.TRUE);
                    }
                    Persistence.deleteMessagesFromMailbox(mailboxName, uidMap.keySet());
                    response = new POPCommandResponse("+OK Goodbye\r\n", true);
                } else if (firstCommandPart.equalsIgnoreCase("APOP")) {
                    if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing username and MD5 digest\r\n", false);
                        error = true;
                    } else {
                        String username = st.nextToken();
                        if (!st.hasMoreTokens()) {
                            response = new POPCommandResponse("-ERR Missing MD5 digest\r\n", false);
                            error = true;
                        } else {
                            String md5Digest = st.nextToken();
                            MailUser u = Persistence.getUserByEmailAddress(username);
                            boolean authenticated = false;
                            if (u != null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("<");
                                sb.append(state.getApopPid());
                                sb.append(".");
                                sb.append(state.getApopTimestamp());
                                sb.append("@");
                                sb.append(state.getServerInstance().getNetworkAttributes().getHostname());
                                sb.append(">");
                                sb.append(u.getPassword());

                                authenticated = MessageDigestUtils.md5(sb.toString()).equals(md5Digest);
                            }

                            if (authenticated) {
                                if (getMailboxLock("user." + u.getEmailAddress(), state)) {
                                    state.setAuthenticatedUsername(u.getEmailAddress());
                                    state.setMainState(POPConnectionState.MainState.TRANSACTION);
                                    response = new POPCommandResponse("+OK INBOX locked and ready\r\n", false);
                                } else {
                                    response = new POPCommandResponse("-ERR [IN-USE] Mailbox is locked by another thread\r\n", false);
                                }
                            } else // Authentication failed
                            {
                                response = new POPCommandResponse("-ERR Authentication failed\r\n", false);
                                error = true;
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("AUTH")) {
                    if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing mechanism\r\n", false);
                        error = true;
                    } else {
                        String mechanism = st.nextToken();
                        if (mechanism.equalsIgnoreCase("CRAM-MD5")) {
                            state.setCramMd5Timestamp((int) (System.currentTimeMillis() / (long) 1000));
                            String challenge = "<" + state.getApopPid()
                                    + "." + state.getCramMd5Timestamp()
                                    + "@" + state.getServerInstance().getNetworkAttributes().getHostname()
                                    + ">";
                            ByteArrayOutputStream os = new ByteArrayOutputStream((int) (challenge.length() * 1.4));
                            Base64OutputStream b64os = new Base64OutputStream(os);
                            try {
                                b64os.write(challenge.getBytes("UTF-8"));
                                b64os.close();
                                os.close();

                                response = new POPCommandResponse("+ " + os.toString("UTF-8") + "\r\n", false);
                            } catch (Exception ex) {
                                // Can't Base64 encode :(
                                throw new RuntimeException(ex);
                            }
                            state.setMainState(POPConnectionState.MainState.AUTHENTICATING);
                            state.setAuthState(POPConnectionState.AuthState.CRAM_MD5_RESPONSE);
                        } else {
                            response = new POPCommandResponse("-ERR Mechanism unavailable\r\n", false);
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("USER")) {
                    if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing username\r\n", false);
                        error = true;
                    } else {
                        state.setLastUsername(st.nextToken());
                        state.setMainState(POPConnectionState.MainState.PRE_PASS);
                        response = new POPCommandResponse("+OK Send password\r\n", false);
                    }
                } else if (firstCommandPart.equalsIgnoreCase("PASS")) {
                    // A check near the bottom of this method will reset the state
                    // to PRE_AUTHENTICATION if there is an error, so there is
                    // no need to do it here.
                    if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing password\r\n", false);
                        error = true;
                    } else {
                        MailUser u = Persistence.getUserByEmailAddressAndPassword(state.getLastUsername(), st.nextToken());
                        if (u == null) // Bad username/password combo
                        {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                            }
                            response = new POPCommandResponse("-ERR Login incorrect\r\n", false);
                            error = true;
                        } else // Good login
                        {
                            if (getMailboxLock("user." + u.getEmailAddress(), state)) {
                                state.setAuthenticatedUsername(u.getEmailAddress());
                                state.setMainState(POPConnectionState.MainState.TRANSACTION);
                                response = new POPCommandResponse("+OK INBOX locked and ready\r\n", false);
                            } else {
                                response = new POPCommandResponse("-ERR [IN-USE] Mailbox is locked by another thread\r\n", false);
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("NOOP")) {
                    response = new POPCommandResponse("+OK Command succeeded\r\n", false);
                } else if (firstCommandPart.equalsIgnoreCase("STAT")) {
                    Mailbox f = state.getLockedMailbox();
                    if (f == null) {
                        response = new POPCommandResponse("-ERR Can't find INBOX\r\n", true);
                    } else {
                        int numberOfMessages = f.getMessages().size();
                        int totalSize = 0;
                        for (int count = 0; count < f.getMessages().size(); count++) {
                            MailMessage m = f.getMessages().get(count);
                            if (state.isMarkedForDeletion(count + 1))
                                numberOfMessages--;
                            else
                                totalSize += m.getMessageSize();
                        }

                        response = new POPCommandResponse("+OK " + numberOfMessages + " " + totalSize + "\r\n", false);
                    }
                } else if (firstCommandPart.equalsIgnoreCase("LIST")) {
                    Mailbox f = state.getLockedMailbox();
                    if (f == null) {
                        response = new POPCommandResponse("-ERR Can't find INBOX\r\n", true);
                    } else {
                        int messageNumber = -1;
                        if (st.hasMoreTokens()) {
                            String messageNumberString = st.nextToken();
                            try {
                                messageNumber = Integer.parseInt(messageNumberString);
                            } catch (NumberFormatException ex) {
                            }
                        }

                        StringBuilder sb = new StringBuilder();
                        if (messageNumber == -1) // Didn't ask for a specific message
                        {
                            sb.append("+OK List follows\r\n");
                            for (int count = 0; count < f.getMessages().size(); count++) {
                                if (!state.isMarkedForDeletion(count + 1))
                                    sb.append(count + 1).append(" ").append(f.getMessages().get(count).getMessageSize()).append("\r\n");
                            }
                            sb.append(".\r\n");
                        } else {
                            if (messageNumber >= 1 && messageNumber <= f.getMessages().size() && !state.isMarkedForDeletion(messageNumber)) {
                                sb.append("+OK ").append(messageNumber).append(" ").append(f.getMessages().get(messageNumber - 1).getMessageSize()).append("\r\n");
                            } else {
                                response = new POPCommandResponse("-ERR No such message (" + messageNumber + ")\r\n", false);
                                error = true;
                            }
                        }

                        if (!error)
                            response = new POPCommandResponse(sb.toString(), false);
                    }
                } else if (firstCommandPart.equalsIgnoreCase("RETR")) {
                    if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing message number\r\n", false);
                        error = true;
                    } else {
                        Mailbox f = state.getLockedMailbox();
                        if (f == null) {
                            response = new POPCommandResponse("-ERR Can't find INBOX\r\n", true);
                        } else {
                            String messageNumberString = st.nextToken();
                            int messageNumber = -1;
                            try {
                                messageNumber = Integer.parseInt(messageNumberString);
                            } catch (NumberFormatException ex) {
                                response = new POPCommandResponse("-ERR Bad message number\r\n", false);
                                error = true;
                            }

                            if (!error) {
                                if (messageNumber >= 1 && messageNumber <= f.getMessages().size() && !state.isMarkedForDeletion(messageNumber)) {
                                    response = new POPCommandResponse(f.getMessages().get(messageNumber - 1));
                                } else {
                                    response = new POPCommandResponse("-ERR No such message (" + messageNumber + ")\r\n", false);
                                    error = true;
                                }
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("UIDL")) {
                    Mailbox f = state.getLockedMailbox();
                    if (f == null) {
                        response = new POPCommandResponse("-ERR Can't find INBOX\r\n", true);
                    } else {
                        int messageNumber = -1;
                        if (st.hasMoreTokens()) {
                            String messageNumberString = st.nextToken();
                            try {
                                messageNumber = Integer.parseInt(messageNumberString);
                            } catch (NumberFormatException ex) {
                            }
                        }

                        StringBuilder sb = new StringBuilder();
                        if (messageNumber == -1) // Didn't ask for a specific message
                        {
                            sb.append("+OK Unique ID List follows\r\n");
                            for (int count = 0; count < f.getMessages().size(); count++) {
                                if (!state.isMarkedForDeletion(count + 1))
                                    sb.append(count + 1).append(" ").append(f.getUidValidity()).append(".").append(f.getMessages().get(count).getMessageUid()).append("\r\n");
                            }
                            sb.append(".\r\n");
                        } else {
                            if (messageNumber >= 1 && messageNumber <= f.getMessages().size() && !state.isMarkedForDeletion(messageNumber)) {
                                sb.append("+OK ").append(messageNumber).append(" ").append(f.getUidValidity()).append(".").append(f.getMessages().get(messageNumber - 1).getMessageUid()).append("\r\n");
                            } else {
                                response = new POPCommandResponse("-ERR No such message (" + messageNumber + ")\r\n", false);
                                error = true;
                            }
                        }

                        if (!error)
                            response = new POPCommandResponse(sb.toString(), false);
                    }
                } else if (firstCommandPart.equalsIgnoreCase("DELE")) {
                    Mailbox f = state.getLockedMailbox();
                    if (f == null) {
                        response = new POPCommandResponse("-ERR Can't find INBOX\r\n", true);
                    } else if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing message number\r\n", false);
                        error = true;
                    } else {
                        String messageNumberString = st.nextToken();
                        int messageNumber = -1;
                        try {
                            messageNumber = Integer.parseInt(messageNumberString);
                        } catch (NumberFormatException ex) {
                            response = new POPCommandResponse("-ERR Bad message number\r\n", false);
                            error = true;
                        }

                        if (!error) {
                            if (messageNumber >= 1 && messageNumber <= f.getMessages().size() && !state.isMarkedForDeletion(messageNumber)) {
                                state.markForDelete(messageNumber);
                                response = new POPCommandResponse("+OK Message marked for deletion\r\n", false);
                            } else {
                                response = new POPCommandResponse("-ERR No such message (" + messageNumber + ")\r\n", false);
                                error = true;
                            }
                        }
                    }
                } else if (firstCommandPart.equalsIgnoreCase("CAPA")) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("+OK Capabilities follow\r\n");
                    sb.append("SASL CRAM-MD5\r\n");
                    sb.append("EXPIRE NEVER\r\n");
                    sb.append("LOGIN-DELAY 0\r\n");
                    sb.append("TOP\r\n");
                    sb.append("UIDL\r\n");
                    sb.append("PIPELINING\r\n");
                    sb.append("RESP-CODES\r\n");
                    sb.append("AUTH-RESP-CODE\r\n");
                    sb.append("IMPLEMENTATION " + Main.PRODUCT_NAME + " " + Main.VERSION + "\r\n");
                    sb.append(".\r\n");
                    response = new POPCommandResponse(sb.toString(), false);
                } else if (firstCommandPart.equalsIgnoreCase("TOP")) {
                    if (!st.hasMoreTokens()) {
                        response = new POPCommandResponse("-ERR Missing both arguments to TOP\r\n", false);
                        error = true;
                    } else {
                        String messageNumberString = st.nextToken();

                        if (!st.hasMoreTokens()) {
                            response = new POPCommandResponse("-ERR Missing second argument to TOP\r\n", false);
                            error = true;
                        } else {
                            int messageNumber = -1;
                            boolean numberInvalid = false;
                            try {
                                messageNumber = Integer.parseInt(messageNumberString);
                            } catch (NumberFormatException ex) {
                                numberInvalid = true;
                            }

                            if (numberInvalid) {
                                response = new POPCommandResponse("-ERR Unparseable number: " + messageNumberString + "\r\n", false);
                                error = true;
                            } else {
                                String numberOfLinesString = st.nextToken();
                                int numberOfLines = -1;
                                numberInvalid = false;
                                try {
                                    numberOfLines = Integer.parseInt(numberOfLinesString);
                                } catch (NumberFormatException ex) {
                                    numberInvalid = true;
                                }

                                if (numberInvalid || (numberOfLines < 0)) {
                                    response = new POPCommandResponse("-ERR Unparseable number: " + numberOfLinesString + "\r\n", false);
                                    error = true;
                                } else {
                                    Mailbox f = state.getLockedMailbox();
                                    if (messageNumber < 1 || messageNumber > f.getMessages().size() || state.isMarkedForDeletion(messageNumber)) {
                                        response = new POPCommandResponse("-ERR No such message (" + messageNumber + ")\r\n", false);
                                        error = true;
                                    } else {
                                        response = new POPCommandResponse(f.getMessages().get(messageNumber - 1), numberOfLines);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (error) {
            if (state.getMainState().equals(POPConnectionState.MainState.PRE_PASS))
                state.setMainState(POPConnectionState.MainState.PRE_AUTHENTICATION);
            state.setRecentBadCommands(state.getRecentBadCommands() + 1);
            if (state.getRecentBadCommands() > 15)
                response = new POPCommandResponse(response.getResponseMessage() + "\r\n" + "-ERR Too many bad commands. Disconnecting.\r\n", true);
        } else
            state.setRecentBadCommands(0);

        assert (response != null);

        return response;
    }
}
