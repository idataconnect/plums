/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.pop;

import com.idataconnect.plums.entity.Mailbox;
import com.idataconnect.plums.network.ServerInstance;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * State information for a POP3 connection. Each client connection to
 * a POP3 instance has one of these state instances attached.
 * @author Ben Upsavs
 */
public class POPConnectionState {

    /**
     * The main state of a POP3 connection.
     */
    public enum MainState {

        /** Waiting for authentication command state */
        PRE_AUTHENTICATION,

        /** In the middle of a multiple step authentication (except USER/PASS ) */
        AUTHENTICATING,

        /** Waiting for PASS command state */
        PRE_PASS,

        /** Transaction state, so named in RFC1939 */
        TRANSACTION,
    }

    public enum AuthState {

        /** Not currently authenticating */
        NONE,
        /** CRAM-MD5 selected, waiting for response. */
        CRAM_MD5_RESPONSE,
        /** User is authenticated. */
        AUTHENTICATED,
    }
    private MainState mainState = MainState.PRE_AUTHENTICATION;
    private AuthState authState = AuthState.NONE;
    private String lastUsername;
    private String authenticatedUsername;
    private int recentBadCommands;
    private int apopPid;
    private int apopTimestamp;
    private int cramMd5Timestamp;
    private ServerInstance serverInstance;
    private Mailbox lockedMailbox;
    private Map<Integer, Boolean> markedForDeletion = new HashMap<Integer, Boolean>();
    private InetAddress remoteHost;

    /**
     * Marks the message for delete. It will actually be deleted once a QUIT
     * command is received.
     * @param messageNumber The sequential message number to mark as deleted.
     */
    public void markForDelete(Integer messageNumber) {
        markedForDeletion.put(messageNumber, Boolean.TRUE);
    }

    /**
     * Checks if the message with sequential message number
     * <code>messageNumber</code> is marked for deletion.
     * @param messageNumber The sequential message number.
     * @return Whether or not the message is marked for deletion.
     */
    public boolean isMarkedForDeletion(Integer messageNumber) {
        return markedForDeletion.containsKey(messageNumber);
    }

    /**
     * Gets the map of messages numbers marked for deletion. Each key will be
     * the sequential id of the message for the current connection that
     * this state is associated with, and each value will always be
     * <code>Boolean.TRUE</code>.
     * @return The map of message numbers marked for deletion.
     */
    public Map<Integer, Boolean> getMarkedForDeletionMap() {
        return markedForDeletion;
    }

    /**
     * Gets the main state of the POP3 connection that this state is associated
     * with.
     * @return The main state of the POP3 connection.
     */
    public MainState getMainState() {
        return mainState;
    }

    /**
     * Sets the main state of the POP3 connection that this state is
     * associated with.
     * @param mainState The new main state of the POP3 connection.
     */
    public void setMainState(MainState mainState) {
        this.mainState = mainState;
    }

    /**
     * Gets the current authentication state for the current POP3 connection.
     * This should be set to NONE if the client is not currently authenticating.
     * @return The current authentication state.
     */
    public AuthState getAuthState() {
        return authState;
    }

    /**
     * Sets the current authentication state for the current POP3 connection.
     * @param authState The current authentication state.
     */
    public void setAuthState(AuthState authState) {
        this.authState = authState;
    }

    /**
     * Gets the last username given by the client as an argument to the
     * USER command.
     * @return The last username given by the client as an argument to the
     * USER command.
     */
    public String getLastUsername() {
        return lastUsername;
    }

    /**
     * Sets the last username given by the client as an argument to the
     * USER command.
     * @param lastUsername The last username given by the client as an
     * argument to the USER command.
     */
    public void setLastUsername(String lastUsername) {
        this.lastUsername = lastUsername;
    }

    /**
     * Gets the username of the authenticated user, if they are currently
     * logged in. This will be the user's full e-mail address.
     * @return The current username, or <code>null</code> if there is no
     * user logged in.
     */
    public String getAuthenticatedUsername() {
        return authenticatedUsername;
    }

    /**
     * Sets the username of the currently logged in user. This will be
     * the user's full email address.
     * @param authenticatedUsername The username of the currently logged in
     * user.
     */
    public void setAuthenticatedUsername(String authenticatedUsername) {
        this.authenticatedUsername = authenticatedUsername;
    }

    /**
     * Gets the number of bad commands that the client has sent in a row. Once
     * this value becomes too large, the client will be disconnected.
     * @return The number of bad commands that the client has sent in a row.
     */
    public int getRecentBadCommands() {
        return recentBadCommands;
    }

    /**
     * Sets the number of bad commands that the client has sent in a row. This
     * number should be incremented when a POP3 -ERR response is generated
     * because of a bad client command, but not because of a temporary server
     * error or failure to lock the mailbox.
     * @param recentBadCommands The number of bad commands that the client
     * has sent in a row.
     */
    public void setRecentBadCommands(int recentBadCommands) {
        this.recentBadCommands = recentBadCommands;
    }

    /**
     * Gets the APOP PID that was generated when the client connected to
     * the server. This is used by APOP authentication to prevent
     * replay attacks. This may also be used for CRAM-MD5 authentication.
     * @return The APOP PID that was generated when the client connected
     * to the server.
     */
    public int getApopPid() {
        return apopPid;
    }

    /**
     * Sets the APOP PID that was generated when the client connected to
     * the server.
     * @param apopPid The APOP PID
     */
    public void setApopPid(int apopPid) {
        this.apopPid = apopPid;
    }

    /**
     * Gets the APOP timestamp that was generated when the client connected
     * to the server. This is used by APOP authentication to prevent replay
     * attacks.
     * @return The APOP timestamp that was generated when the client connected
     * to the server.
     */
    public int getApopTimestamp() {
        return apopTimestamp;
    }

    /**
     * Sets the APOP timestamp that was generated when the client connected
     * to the server.
     * @param apopTimestamp The APOP timestamp.
     */
    public void setApopTimestamp(int apopTimestamp) {
        this.apopTimestamp = apopTimestamp;
    }

    /**
     * Gets the CRAM-MD5 timestamp that was generated when the client issued
     * an AUTH CRAM-MD5 command.
     * @return The CRAM-MD5 timestamp that was generated when the client
     * issued an AUTH CRAM-MD5 command.
     */
    public int getCramMd5Timestamp() {
        return cramMd5Timestamp;
    }

    /**
     * Sets the CRAM-MD5 timestamp that was generated when the client
     * issued an AUTH CRAM-MD5 command.
     * @param cramMd5Timestamp The generated timestamp.
     */
    public void setCramMd5Timestamp(int cramMd5Timestamp) {
        this.cramMd5Timestamp = cramMd5Timestamp;
    }

    /**
     * Gets the server instance that was used to create the current POP3
     * listener.
     * @return The server instance used to create the current POP3 listener.
     */
    public ServerInstance getServerInstance() {
        return serverInstance;
    }

    /**
     * Sets the server instance that was used to create the current POP3
     * listener.
     * @param serverInstance The server instance used to create the current
     * POP3 listener.
     */
    public void setServerInstance(ServerInstance serverInstance) {
        this.serverInstance = serverInstance;
    }

    /**
     * Gets a copy of the <code>Mailbox</code> that was captured during login, if
     * there is currently a user logged in.
     * @return The mailbox that was captured during login.
     */
    public Mailbox getLockedMailbox() {
        return lockedMailbox;
    }

    /**
     * Sets the copy of the <code>Mailbox</code> that was captured during login.
     * @param lockedMailbox The mailbox that was captured during login.
     */
    public void setLockedMailbox(Mailbox lockedMailbox) {
        this.lockedMailbox = lockedMailbox;
    }

    /**
     * Gets the address of the connected remote host.
     * @return The address of the connected remote host.
     */
    public InetAddress getRemoteHost() {
        return remoteHost;
    }

    /**
     * Sets the address of the connected remote host.
     * @param remoteHost The address of the connected remote host.
     */
    public void setRemoteHost(InetAddress remoteHost) {
        this.remoteHost = remoteHost;
    }
}
