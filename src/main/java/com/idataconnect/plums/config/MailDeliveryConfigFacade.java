/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.config;

import com.idataconnect.plums.Persistence;

/**
 * A utility class which simplifies loading and saving server settings.
 * @author Ben Upsavs
 */
public class MailDeliveryConfigFacade {

    /**
     * Gets the current delivery mechanism.
     * @return The current delivery mechanism.
     */
    public static DeliveryMechanism getDeliveryMechanism() {
        String deliveryMechanismString = Persistence.getGlobalConfigValue("deliveryMechanism", "INTERNAL_DATABASE");
        for (DeliveryMechanism m : DeliveryMechanism.values()) {
            if (m.toString().equals(deliveryMechanismString))
                return m;
        }

        return null;
    }

    /**
     * Modifies the current delivery mechanism.
     * @param mechanism The new deliery mechanism to use.
     */
    public static void updateDeliveryMechanism(DeliveryMechanism mechanism) {
        Persistence.setGlobalConfigValue("deliveryMechanism", mechanism.toString());
    }
}
