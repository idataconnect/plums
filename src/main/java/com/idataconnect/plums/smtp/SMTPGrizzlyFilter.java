/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.smtp;

import com.idataconnect.plums.network.PlumsGrizzlyFilter;
import com.idataconnect.plums.network.ServerInstance;
import com.sun.grizzly.Context;
import com.sun.grizzly.ProtocolParser;
import com.sun.grizzly.util.OutputWriter;
import com.sun.grizzly.util.WorkerThread;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SMTP Grizzly Protocol Filter.
 * @author Ben Upsavs
 */
public class SMTPGrizzlyFilter implements PlumsGrizzlyFilter {

    /**
     * A map of channels to connection state objects associated with this protocol.
     */
    static final Map<SelectableChannel, SMTPConnectionState> connections = new HashMap<SelectableChannel, SMTPConnectionState>();

    /**
     * Cleans up the connection associated with <code>channel</code>.
     * @param channel The connection's channel.
     */
    public void cleanupConnection(SocketChannel channel) {
        try {
            SMTPConnectionState s = connections.get(channel);
            if (s != null) {
                Logger log = Logger.getLogger(SMTPGrizzlyFilter.class.getName());
                log.log(Level.INFO, "SMTP: Disconnected {0}", s.getRemoteHost().getHostAddress());
                log.log(Level.FINER, "There are now {0} active SMTP connections", connections.size());
            }
        } finally {
            connections.remove(channel);
        }
    }

    /**
     * Adds a new connection to the list, and sends the client an SMTP
     * greeting.
     * @param channel The channel of the connection.
     * @param serverInstance The server instance that the client connected to.
     * @param remoteHost The remote host which initiated the connection.
     */
    public void newConnection(SocketChannel channel, ServerInstance serverInstance, InetAddress remoteHost) {
        SMTPConnectionState state = new SMTPConnectionState();
        state.setServerInstance((SMTPServerInstance) serverInstance);
        state.setRemoteHost(remoteHost);
        state.setCramMd5Pid(channel.hashCode());

        String greeting = "220 " + serverInstance.getNetworkAttributes().getHostname() + " Service ready\r\n";
        ByteBuffer buf = ByteBuffer.wrap(greeting.getBytes());
        try {
            OutputWriter.flushChannel((SocketChannel) channel, buf);
        } catch (IOException ex) {
            Logger.getLogger(SMTPGrizzlyFilter.class.getName()).log(Level.WARNING,
                    "I/O error while sending SMTP greeting", ex);
        }

        connections.put(channel, state);
        Logger log = Logger.getLogger(SMTPGrizzlyFilter.class.getName());
        log.log(Level.INFO, "SMTP: Connection from {0}", remoteHost.getHostAddress());
        log.log(Level.FINER, "There are now {0} active SMTP connections", connections.size());
    }

    /** {@inheritDoc} */
    public boolean execute(Context ctx) throws IOException {
        String s = (String) ctx.getAttribute(ProtocolParser.MESSAGE);
        if (s != null) {
            WorkerThread workerThread = (WorkerThread) Thread.currentThread();
            SelectableChannel channel = ctx.getSelectionKey().channel();
            ByteBuffer buf = workerThread.getByteBuffer();
            buf.clear();

            SMTPCommandResponse response = SMTPProtocolHandler.processCommand(s, connections.get(channel));

            if (response != null) {
                buf.put(response.getResponseMessage().getBytes());
                buf.flip();
                while (buf.hasRemaining())
                    ((SocketChannel) channel).write(buf);
                buf.clear();

                if (response.isDisconnect())
                    ctx.getSelectorHandler().closeChannel(channel);
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    public boolean postExecute(Context ctx) throws IOException {
        return true;
    }
}
