/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.idataconnect.plums.smtp;

/**
 * Response from {@link SMTPProtocolHandler#processCommand}.
 * @author Ben Upsavs
 */
public class SMTPCommandResponse {

    private String responseMessage;
    private boolean disconnect;

    /**
     * Constructs a new, empty instance.
     */
    public SMTPCommandResponse() {
    }

    /**
     * Constructs a response with the specified message, and choosing not
     * to disconnect the session.
     * @param responseMessage The message to send to the client.
     */
    public SMTPCommandResponse(String responseMessage) {
        this(responseMessage, false);
    }

    /**
     * Constructs a new, populated instance.
     * @param responseMessage The message to send to the client.
     * @param disconnect Whether to disconnect the connection.
     */
    public SMTPCommandResponse(String responseMessage, boolean disconnect) {
        this.responseMessage = responseMessage;
        this.disconnect = disconnect;
    }

    /**
     * Gets whether to disconnect the connection.
     * @return Whether to disconnect the connection.
     */
    public boolean isDisconnect() {
        return disconnect;
    }

    /**
     * Sets whether to disconnect the connection.
     * @param disconnect Whether to disconnect the connection.
     */
    public void setDisconnect(boolean disconnect) {
        this.disconnect = disconnect;
    }

    /**
     * Gets the message to send to the client.
     * @return The message to send to the client.
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * Sets the message to send to the client.
     * @param responseMessage The message to send to the client.
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
