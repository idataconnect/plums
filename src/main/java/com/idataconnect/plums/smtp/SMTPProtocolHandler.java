/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.smtp;

import com.idataconnect.plums.Main;
import com.idataconnect.plums.MessageDigestUtils;
import com.idataconnect.plums.Persistence;
import com.idataconnect.plums.entity.HostedDomain;
import com.idataconnect.plums.entity.MailMessageQueueItem;
import com.idataconnect.plums.entity.MailUser;
import com.idataconnect.plums.spool.MailMessageQueueManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.Base64OutputStream;

/**
 * SMTP Protocol handler.
 * @author Ben Upsavs
 */
public class SMTPProtocolHandler {

    /** pre-initialized "unknown command" response. */
    static final SMTPCommandResponse UNKNOWN_COMMAND_RESPONSE = new SMTPCommandResponse("500 Unknown command\r\n");

    /**
     * Creates an RFC date formatter.
     *
     * @return a <code>DateFormat</code> instance
     */
    public static DateFormat getDateFormatter() {
        return new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z (z)");
    }

    static void resetState(SMTPConnectionState state) {
        // If state is PRE_HELO, leave it there, else set it to PRE_MAIL_FROM
        if (!state.getMainState().equals(SMTPConnectionState.MainState.PRE_HELO))
            state.setMainState(SMTPConnectionState.MainState.PRE_MAIL_FROM);

        // Reset authentication state - If authenticated, leave that way, else
        // set back to none.
        if (!state.getAuthState().equals(SMTPConnectionState.AuthState.AUTHENTICATED))
            state.setAuthState(SMTPConnectionState.AuthState.NONE);

        // Reset the from and to addresses
        state.setMailFrom(null);
        state.clearRcptTo();

        // Reset number of recipients rejected
        state.setNumRecipientsRejected(0);
    }

    /**
     * Parses an e-mail address given by an SMTP client, removing any deprecated
     * routing tokens and angular brackets. Addresses with double quotes in the
     * local part are unsupported, since it's doubtful that they are supported
     * in most other implementations.
     *
     * @param smtpAddress The address given by an SMTP client.
     * @return A normal e-mail address, or <code>null</code> if a valid one
     * could not be found.
     */
    static String parseAddress(final String smtpAddress) {
        // This routine is complicated but it is intended to be faster than
        // any easier way of doing it.

        if (smtpAddress.trim().equalsIgnoreCase("<postmaster>"))
            return "postmaster"; // Special postmaster case

        if (smtpAddress.charAt(0) != '<')
            return null;

        int endLocation = smtpAddress.lastIndexOf('>');
        if (endLocation == -1)
            return null;
        else if (endLocation == 1)
            return ""; // Empty sender used during a DSN.

        boolean dotWasLastChar = false;
        boolean lastCharNotAllowedNextToDot = false;
        boolean domainPart = true;
        for (int count = endLocation - 1; count >= 0; count--) {
            char ch = smtpAddress.charAt(count);
            if (Character.isLetterOrDigit(ch) || ch == '!' || ch == '#'
                    || ch == '$' || ch == '%' || ch == '&' || ch == '\''
                    || ch == '*' || ch == '+' || ch == '-' || ch == '/'
                    || ch == '=' || ch == '?' || ch == '^' || ch == '_'
                    || ch == '`' || ch == '{' || ch == '|' || ch == '}'
                    || ch == '~' || ch == '.' || ch == '<' || ch == '@'
                    || ch == ':') {
                if (count == endLocation - 1) {
                    if (ch == '.' || ch == '@' || ch == '-')
                        return null;
                } else if (ch == '@') {
                    if (!domainPart)
                        return null; // Can't have 2 @ signs.

                    domainPart = false;
                    continue;
                }

                if (domainPart) {
                    if (ch == '!' || ch == '#'
                            || ch == '$' || ch == '%' || ch == '&' || ch == '\''
                            || ch == '*' || ch == '+' || ch == '/'
                            || ch == '=' || ch == '?' || ch == '^' || ch == '_'
                            || ch == '`' || ch == '{' || ch == '|' || ch == '}') {
                        return null; // These are only allowed in the user part.
                    } else if (dotWasLastChar && ch == '-')
                        return null; // Dash not allowed next to dot.
                } else if (ch == ':' || ch == '<') {
                    if (dotWasLastChar || lastCharNotAllowedNextToDot)
                        return null; // First char can't be dot.
                    return smtpAddress.substring(count + 1, endLocation);
                }

                if (ch == '.') {
                    if (count == 1)
                        return null; // First char can't be dot.
                    else if (dotWasLastChar)
                        return null; // Can't have 2 dots in a row.
                    else if (lastCharNotAllowedNextToDot && domainPart)
                        return null;

                    dotWasLastChar = true;
                } else
                    dotWasLastChar = false;

                if (ch == '@' || ch == '-' || ch == '.')
                    lastCharNotAllowedNextToDot = true;
                else
                    lastCharNotAllowedNextToDot = false;
            } else { // Bad character
                return null;
            }
        }

        return null; // No start bracket or : sign found
    }

    /**
     * Process a SMTP command, or a line of incoming e-mail data.
     * @param command The command sent from the client
     * @param state The state associated with this connection
     * @return The command response.
     */
    public static SMTPCommandResponse processCommand(String command, SMTPConnectionState state) {
        boolean error = false;
        SMTPCommandResponse response = null;

        boolean contains8Bit = false;
        for (int count = 0; count < command.length(); count++) {
            if (command.charAt(count) > 127) {
                contains8Bit = true;
                break;
            }
        }

        if (contains8Bit) {
            response = new SMTPCommandResponse("421 Message contains 8 bit\r\n", true);
            error = true;
        } else if (state.getMainState().equals(SMTPConnectionState.MainState.DATA)) {
            // Handle the state where we are receiving a message.
            if (command.equals(".")) // End of message
            {
                // The mesage is completely received.
                // Check for error.
                if (state.getQueueException() != null) {
                    // Error occured saving queue file(s). Delete them all.
                    for (MailMessageQueueItem q : state.getQueueItems()) {
                        FileOutputStream fos = state.getFileOutputStreams().get(q);
                        try {
                            fos.getChannel().close();
                            fos.close();

                        } catch (IOException ex) {
                            Logger.getLogger(SMTPProtocolHandler.class.getName()).warning("Error closing queue file.");
                        }
                    }
                    state.setByteBuffer(null);
                    Persistence.deleteMailMessageQueueItems(state.getQueueItems());
                    response = new SMTPCommandResponse("451 Error: " + state.getQueueException().getMessage());
                } else {
                    for (MailMessageQueueItem q : state.getQueueItems()) {
                        FileOutputStream fos = state.getFileOutputStreams().get(q);
                        try {
                            fos.getChannel().close();
                            fos.close();

                        } catch (IOException ex) {
                            Logger.getLogger(SMTPProtocolHandler.class.getName()).warning("Error closing queue file.");
                        }
                        q.loadMessageSize();
                        q.setIncoming(false);
                    }

                    Persistence.updateMailMessageQueueItems(state.getQueueItems());

                    response = new SMTPCommandResponse("250 OK" + (state.getNumRecipientsRejected() > 0 ? ", some recipients rejected" : "") + "\r\n");
                    for (MailMessageQueueItem qi : state.getQueueItems())
                        MailMessageQueueManager.attemptDelivery(qi.getQueueId());
                }
                resetState(state);
            } else { // Received another line of the incoming message
                // TODO if the user is trusted (authenticated or some other
                // reason for trust), add the following information to the
                // headers:
                //   o  Addition of a message-id field when none appears
                //   o  Addition of a date, time, or time zone when none appears
                // (taken from RFC5321 Section 6.4)

                if (command.length() != 0) {
                    // If the line starts with a . and has other characters, remove
                    // the first character.
                    command = command.charAt(0) == '.' ? command.substring(1) : command;
                    state.getByteBuffer().put(command.getBytes());
                }
                state.getByteBuffer().put((byte) '\r');
                state.getByteBuffer().put((byte) '\n');
                state.getByteBuffer().flip();
                int bufferPosition = state.getByteBuffer().position();
                for (MailMessageQueueItem qi : state.getQueueItems()) {
                    try {
                        while (state.getByteBuffer().hasRemaining())
                            state.getFileOutputStreams().get(qi).getChannel().write(state.getByteBuffer());
                    } catch (IOException ex) {
                        if (state.getQueueException() == null) {
                            // Log first error
                            Logger.getLogger(SMTPProtocolHandler.class.getName())
                                    .log(Level.SEVERE, "Error while saving mail queue message", ex);
                        }
                        state.setQueueException(ex);
                    }
                    state.getByteBuffer().position(bufferPosition);
                }
                state.getByteBuffer().clear();
            }
        } else if (state.getMainState().equals(SMTPConnectionState.MainState.AUTHENTICATING)
                && state.getAuthState().equals(SMTPConnectionState.AuthState.LOGIN_USERNAME)) {
            if (command.equals("*")) {
                state.setMainState(state.getRemoteIdentifier() == null
                        ? SMTPConnectionState.MainState.PRE_HELO
                        : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                state.setAuthState(SMTPConnectionState.AuthState.NONE);
                response = new SMTPCommandResponse("501 Authentication aborted\r\n");
            } else {
                state.setEncodedLoginUsername(command);
                state.setAuthState(SMTPConnectionState.AuthState.LOGIN_PASSWORD);
                response = new SMTPCommandResponse("334 UGFzc3dvcmQ6\r\n");
            }
        } else if (state.getMainState().equals(SMTPConnectionState.MainState.AUTHENTICATING)
                && state.getAuthState().equals(SMTPConnectionState.AuthState.LOGIN_PASSWORD)) {
            if (command.equals("*")) {
                state.setMainState(state.getRemoteIdentifier() == null
                        ? SMTPConnectionState.MainState.PRE_HELO
                        : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                state.setAuthState(SMTPConnectionState.AuthState.NONE);
                response = new SMTPCommandResponse("501 Authentication aborted\r\n");
            } else {
                String username = null, password = null;
                try {
                    username = unencodeBase64String(state.getEncodedLoginUsername());
                    password = unencodeBase64String(command);
                } catch (IOException ex) {
                    response = new SMTPCommandResponse("501 Invalid authentication token\r\n");
                    state.setMainState(state.getRemoteIdentifier() == null
                            ? SMTPConnectionState.MainState.PRE_HELO
                            : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                    state.setAuthState(SMTPConnectionState.AuthState.NONE);
                    error = true;
                }

                if (!error) {
                    MailUser u = Persistence.getUserByEmailAddressAndPassword(username, password);
                    if (u != null) {
                        state.setAuthState(SMTPConnectionState.AuthState.AUTHENTICATED);
                        state.setMainState(state.getRemoteIdentifier() == null
                                ? SMTPConnectionState.MainState.PRE_HELO
                                : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                        state.setAuthenticationMechanism("LOGIN");
                        state.setAuthenticatedUsername(u.getEmailAddress());
                        response = new SMTPCommandResponse("235 Authenticated\r\n");
                    } else {
                        response = new SMTPCommandResponse("535 Authentication failed\r\n");
                        error = true;
                        state.setMainState(state.getRemoteIdentifier() == null
                                ? SMTPConnectionState.MainState.PRE_HELO
                                : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                        state.setAuthState(SMTPConnectionState.AuthState.NONE);
                    }
                }
            }
        } else if (state.getMainState().equals(SMTPConnectionState.MainState.AUTHENTICATING)
                && state.getAuthState().equals(SMTPConnectionState.AuthState.CRAM_MD5_RESPONSE)) {
            if (command.equals("*")) {
                state.setMainState(state.getRemoteIdentifier() == null
                        ? SMTPConnectionState.MainState.PRE_HELO
                        : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                state.setAuthState(SMTPConnectionState.AuthState.NONE);
                response = new SMTPCommandResponse("501 Authentication aborted\r\n");
            } else {
                String token = null;
                try {
                    token = unencodeBase64String(command);
                } catch (IOException ex) {
                    response = new SMTPCommandResponse("501 Invalid authentication token\r\n");
                    state.setMainState(state.getRemoteIdentifier() == null
                            ? SMTPConnectionState.MainState.PRE_HELO
                            : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                    state.setAuthState(SMTPConnectionState.AuthState.NONE);
                    error = true;
                }

                if (!error) {
                    StringTokenizer st = new StringTokenizer(token, " ");
                    if (!st.hasMoreTokens()) {
                        response = new SMTPCommandResponse("501 Invalid authentication token\r\n");
                        state.setMainState(state.getRemoteIdentifier() == null
                                ? SMTPConnectionState.MainState.PRE_HELO
                                : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                        state.setAuthState(SMTPConnectionState.AuthState.NONE);
                        error = true;
                    } else {
                        String username = st.nextToken();

                        if (!st.hasMoreTokens()) {
                            response = new SMTPCommandResponse("501 Invalid authentication token\r\n");
                            state.setMainState(state.getRemoteIdentifier() == null
                                    ? SMTPConnectionState.MainState.PRE_HELO
                                    : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                            state.setAuthState(SMTPConnectionState.AuthState.NONE);
                            error = true;
                        } else {
                            boolean authenticated = false;

                            MailUser u = Persistence.getUserByEmailAddress(username);
                            if (u != null) {
                                String challenge = "<" + state.getCramMd5Pid()
                                        + "." + state.getCramMd5Timestamp()
                                        + "@" + state.getServerInstance().getNetworkAttributes().getHostname()
                                        + ">";

                                String digest = MessageDigestUtils.hmacMd5(challenge, u.getPassword());
                                String decodedCorrectAnswer = username + " " + digest;
                                String correctAnswer = encodeBase64String(decodedCorrectAnswer);

                                if (command.equals(correctAnswer))
                                    authenticated = true;
                            }

                            if (authenticated) {
                                state.setAuthState(SMTPConnectionState.AuthState.AUTHENTICATED);
                                state.setMainState(state.getRemoteIdentifier() == null
                                        ? SMTPConnectionState.MainState.PRE_HELO
                                        : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                                state.setAuthenticationMechanism("CRAM-MD5");
                                state.setAuthenticatedUsername(u.getEmailAddress());
                                response = new SMTPCommandResponse("235 Authenticated\r\n");
                            } else {
                                response = new SMTPCommandResponse("535 Authentication failed\r\n");
                                error = true;
                                state.setMainState(state.getRemoteIdentifier() == null
                                        ? SMTPConnectionState.MainState.PRE_HELO
                                        : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                                state.setAuthState(SMTPConnectionState.AuthState.NONE);
                            }
                        }
                    }
                }
            }
        } else if (command.trim().length() == 0) {
            response = UNKNOWN_COMMAND_RESPONSE;
            error = true;
        } else {
            // Split the command into tokens and grab the first one
            StringTokenizer st = new StringTokenizer(command, " ");
            String firstCommandPart = st.nextToken();

            if (firstCommandPart.equalsIgnoreCase("QUIT")) {
                response = new SMTPCommandResponse("221 " + state.getServerInstance().getNetworkAttributes().getHostname() + " Service closing transmission channel.\r\n", true);
            } else if (firstCommandPart.equalsIgnoreCase("NOOP")) {
                response = new SMTPCommandResponse("250 OK\r\n", false);
            } else if (firstCommandPart.equalsIgnoreCase("EXPN")) {
                response = new SMTPCommandResponse("502 Command not implemented\r\n");
            } else if (firstCommandPart.equalsIgnoreCase("HELO") || firstCommandPart.equalsIgnoreCase("EHLO")) {
                if (!st.hasMoreTokens()) {
                    response = new SMTPCommandResponse("501 Missing identifier\r\n");
                } else {
                    state.setRemoteIdentifier(st.nextToken());
                    if (firstCommandPart.equalsIgnoreCase("HELO")) {
                        // Legacy HELO
                        response = new SMTPCommandResponse("250 " + state.getServerInstance().getNetworkAttributes().getHostname() + " greets " + state.getRemoteIdentifier() + "\r\n");
                    } else {
                        // Extended HELO (EHLO)
                        state.setEhlo(true);

                        StringBuilder sb = new StringBuilder();
                        sb.append("250-").append(state.getServerInstance().getNetworkAttributes().getHostname()).append(" greets ").append(state.getRemoteIdentifier()).append("\r\n");
                        sb.append("250-PIPELINING\r\n");
                        String authMechs = Persistence.getSMTPAuthenticationMechanismsAsString();
                        if (authMechs.length() != 0)
                            sb.append("250-AUTH ").append(authMechs).append("\r\n");
                        sb.append("250 VRFY\r\n");
                        //TODO enumerate additional capabilities here
                        response = new SMTPCommandResponse(sb.toString());
                    }

                    state.setMainState(SMTPConnectionState.MainState.PRE_MAIL_FROM);
                    resetState(state);
                }
            } else if (firstCommandPart.equalsIgnoreCase("RSET")) {
                resetState(state);

                response = new SMTPCommandResponse("250 OK State reset\r\n");
            } else if (firstCommandPart.equalsIgnoreCase("MAIL")) {
                if (!st.hasMoreTokens()) {
                    response = UNKNOWN_COMMAND_RESPONSE;
                    error = true;
                } else {
                    String secondCommandPart = st.nextToken(":").trim();
                    if (!secondCommandPart.equalsIgnoreCase("FROM")) {
                        response = UNKNOWN_COMMAND_RESPONSE;
                        error = true;
                    } else {
                        if (!st.hasMoreTokens()) {
                            response = UNKNOWN_COMMAND_RESPONSE;
                            error = true;
                        } else if (!state.getMainState().equals(SMTPConnectionState.MainState.PRE_MAIL_FROM)) {
                            response = new SMTPCommandResponse("503 Command out of sequence\r\n");
                            error = true;
                        } else {
                            String address = parseAddress(st.nextToken("").substring(1).trim()); // Remainder
                            if (address == null) {
                                response = new SMTPCommandResponse("501 Could not parse address\r\n");
                                error = true;
                            } else {
                                // TODO Sender validity checks

                                state.setMailFrom(address);
                                state.setMainState(SMTPConnectionState.MainState.PRE_DATA);

                                response = new SMTPCommandResponse("250 OK Sender accepted\r\n");
                            }
                        }
                    }
                }
            } else if (firstCommandPart.equalsIgnoreCase("RCPT")) {
                if (!st.hasMoreTokens()) {
                    response = UNKNOWN_COMMAND_RESPONSE;
                    error = true;
                } else {
                    String secondCommandPart = st.nextToken(":").trim();
                    if (!secondCommandPart.equalsIgnoreCase("TO")) {
                        response = UNKNOWN_COMMAND_RESPONSE;
                        error = true;
                    } else {
                        if (!st.hasMoreTokens()) {
                            response = UNKNOWN_COMMAND_RESPONSE;
                            error = true;
                        } else if (!state.getMainState().equals(SMTPConnectionState.MainState.PRE_DATA)) {
                            response = new SMTPCommandResponse("503 Command out of sequence\r\n");
                            error = true;
                        } else {
                            String address = parseAddress(st.nextToken("").substring(1).trim()); // Remainder
                            if (address == null) {
                                response = new SMTPCommandResponse("501 Could not parse address\r\n");
                                state.anotherRecipientRejected();
                            } else {
                                if (address.equals("postmaster")) {
                                    // TODO handle postmaster case
                                } else {
                                    StringTokenizer st2 = new StringTokenizer(address, "@");
                                    st2.nextToken();
                                    String domain = st2.nextToken();

                                    HostedDomain hostedDomain = Persistence.getHostedDomain(domain);
                                    if (hostedDomain != null) {
                                        // Domain is hosted here; check for a local user.
                                        MailUser u = Persistence.getUserByEmailAddress(address);
                                        if (u != null) {
                                            // TODO check quota, etc.
                                            state.addRcptTo(address);
                                            response = new SMTPCommandResponse("250 OK Local recipient accepted\r\n");
                                        } else {
                                            response = new SMTPCommandResponse("550 No such user here\r\n");
                                            state.anotherRecipientRejected();
                                        }
                                    } else {
                                        // Domain is not hosted here; must relay
                                        if (state.canRelay()) {
                                            // TODO additional remote recipient checks

                                            state.addRcptTo(address);
                                            response = new SMTPCommandResponse("250 OK Remote recipient accepted\r\n");
                                        } else {
                                            response = new SMTPCommandResponse("550 Relaying denied\r\n");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (firstCommandPart.equalsIgnoreCase("DATA")) {
                // Check if there are any valid recipients
                if (state.getRcptTo().isEmpty()) {
                    response = new SMTPCommandResponse("554 No valid recipients\r\n");
                } else {
                    boolean localError = false;
                    try {
                        state.setQueueItems(MailMessageQueueManager.queueMessage(state));
                        state.getFileOutputStreams().clear();

                        // Now that this connection is receiving a message, it
                        // gets one byte buffer, if it doesn't already have one.
                        if (state.getByteBuffer() == null)
                            state.setByteBuffer(ByteBuffer.allocate(Integer.parseInt(Persistence.getGlobalConfigValue("smtpByteBufferSize", "8192"))));

                        // Generate received headers
                        for (MailMessageQueueItem qi : state.getQueueItems()) {
                            // Add our received header
                            StringBuilder rh = new StringBuilder();
                            rh.append("Received: from ");
                            rh.append(state.getRemoteIdentifier());
                            rh.append(" (");
                            rh.append(state.getRemoteHost().getHostName());
                            rh.append(" [");
                            rh.append(state.getRemoteHost().getHostAddress());
                            rh.append("])\r\n");
                            rh.append("\tby ");
                            rh.append(state.getServerInstance().getNetworkAttributes().getHostname());
                            rh.append(" (");
                            rh.append(Main.PRODUCT_NAME);
                            rh.append(") with ");
                            if (state.isEhlo())
                                rh.append('E');
                            rh.append("SMTP");
                            if (state.isAuthenticated())
                                rh.append("A");
                            rh.append(" id ");
                            rh.append(qi.getQueueIdForDisplay());
                            rh.append("\r\n");
                            rh.append("\tFor <");
                            rh.append(qi.getRecipient());
                            rh.append(">; ");
                            rh.append(getDateFormatter().format(qi.getQueueTime()));
                            rh.append("\r\n");

                            // Add the I/O stuff to the state object and write
                            // the received header to the queue file.
                            FileOutputStream fos = qi.getOutputStream();
                            state.getFileOutputStreams().put(qi, fos);
                            FileChannel c = fos.getChannel();
                            ByteBuffer bb = state.getByteBuffer();
                            state.setByteBuffer(bb);
                            bb.put(rh.toString().getBytes());
                            bb.flip();
                            while (bb.hasRemaining())
                                c.write(bb);
                            bb.clear();
                        }
                    } catch (IOException ex) {
                        response = new SMTPCommandResponse("451 Error: " + ex.getMessage());
                        localError = true;
                    }
                    if (!localError) {
                        response = new SMTPCommandResponse("354 Start mail input; end with <CRLF>.<CRLF>\r\n");
                        state.setMainState(SMTPConnectionState.MainState.DATA);
                    }
                }
            } else if (firstCommandPart.equalsIgnoreCase("VRFY")) {
                if (!st.hasMoreTokens()) {
                    response = new SMTPCommandResponse("501 Missing address\r\n");
                    error = true;
                } else {
                    String address = st.nextToken();
                    String parsedAddress = parseAddress(address);
                    boolean verified = false;
                    if (parsedAddress != null) {
                        MailUser u = Persistence.getUserByEmailAddress(address);
                        verified = (u != null);
                    }

                    if (verified)
                        response = new SMTPCommandResponse("250 <" + parsedAddress + ">\r\n");
                    else
                        response = new SMTPCommandResponse("252 Unlikely\r\n");
                }
            } else if (firstCommandPart.equalsIgnoreCase("AUTH")) {
                if (!st.hasMoreTokens()) {
                    response = new SMTPCommandResponse("501 Missing mechanism\r\n");
                    error = true;
                } else {
                    if (state.isAuthenticated()) {
                        // Already authenticated - they shouldn't try again
                        response = new SMTPCommandResponse("503 Already authenticated\r\n");
                        error = true;
                    } else {
                        if (!state.getAuthState().equals(SMTPConnectionState.AuthState.NONE)
                                || state.getMainState().equals(SMTPConnectionState.MainState.PRE_DATA)) {
                            response = new SMTPCommandResponse("503 Command out of sequence.\r\n");
                            error = true;
                        } else {
                            String mechanism = st.nextToken();
                            if (!Persistence.isSMTPAuthenticationMechanismEnabled(mechanism)) {
                                // They tried a mech not listed after EHLO
                                response = new SMTPCommandResponse("504 Mechanism unavailable\r\n");
                                error = true;
                            } else {
                                if (mechanism.equalsIgnoreCase("LOGIN")) {
                                    // Send 'Username:' Base64 encoded.
                                    response = new SMTPCommandResponse("334 VXNlcm5hbWU6\r\n");
                                    state.setAuthState(SMTPConnectionState.AuthState.LOGIN_USERNAME);
                                    state.setMainState(SMTPConnectionState.MainState.AUTHENTICATING);
                                } else if (mechanism.equalsIgnoreCase("PLAIN")) {
                                    if (!st.hasMoreTokens()) {
                                        response = new SMTPCommandResponse("501 Missing authentication token\r\n");
                                        error = true;
                                    } else {
                                        String authToken = st.nextToken();
                                        String decodedAuthToken = null;
                                        try {
                                            decodedAuthToken = unencodeBase64String(authToken);
                                        } catch (IOException ex) {
                                            response = new SMTPCommandResponse("501 Invalid authentication token\r\n");
                                            error = true;
                                        }

                                        if (!error) {
                                            // Split at nulls
                                            StringTokenizer st2 = new StringTokenizer(decodedAuthToken, "\u0000");
                                            String token1, token2, token3;
                                            boolean authenticated = false;
                                            if (st2.hasMoreTokens()) {
                                                token1 = st2.nextToken();
                                                if (st2.hasMoreTokens()) {
                                                    token2 = st2.nextToken();
                                                    if (st2.hasMoreTokens() || (decodedAuthToken.charAt(0) == 0)) {
                                                        if (st2.hasMoreTokens())
                                                            token3 = st2.nextToken();
                                                        else
                                                            token3 = token2;

                                                        MailUser u = Persistence.getUserByEmailAddressAndPassword(token1, token3);
                                                        if (u != null) {
                                                            authenticated = true;
                                                            state.setAuthenticatedUsername(u.getEmailAddress());
                                                        }
                                                    }
                                                }
                                            }

                                            if (authenticated) {
                                                state.setAuthState(SMTPConnectionState.AuthState.AUTHENTICATED);
                                                state.setMainState(state.getRemoteIdentifier() == null
                                                        ? SMTPConnectionState.MainState.PRE_HELO
                                                        : SMTPConnectionState.MainState.PRE_MAIL_FROM);
                                                state.setAuthenticationMechanism("PLAIN");

                                                response = new SMTPCommandResponse("235 OK Authenticated\r\n", false);
                                            } else {
                                                response = new SMTPCommandResponse("535 Authentication failed\r\n", false);
                                            }
                                        }
                                    }
                                } else if (mechanism.equalsIgnoreCase("CRAM-MD5")) {
                                    state.setCramMd5Timestamp((int) (System.currentTimeMillis() / (long) 1000));
                                    String authToken = "<" + state.getCramMd5Pid()
                                            + "." + state.getCramMd5Timestamp()
                                            + "@" + state.getServerInstance().getNetworkAttributes().getHostname()
                                            + ">";
                                    authToken = encodeBase64String(authToken);
                                    response = new SMTPCommandResponse("334 " + authToken + "\r\n", false);
                                    state.setMainState(SMTPConnectionState.MainState.AUTHENTICATING);
                                    state.setAuthState(SMTPConnectionState.AuthState.CRAM_MD5_RESPONSE);
                                }
                            }
                        }
                    }
                }
            } else {
                // Unknown command
                response = UNKNOWN_COMMAND_RESPONSE;
                error = true;
            }
        }

        if (error) {
            // The act of disconnecting clients who send bad commands is
            // technically a violation of the SMTP spec, but no one wants a
            // rampaging client hammering their server forever. To keep with the
            // spirit of the spec, we will use a high value here, and return the
            // same response code that would be sent if the server abruptly shut
            // down.
            state.setRecentBadCommands(state.getRecentBadCommands() + 1);
            if (state.getRecentBadCommands() > 50) {
                response = new SMTPCommandResponse("421 Too many bad commands. Disconnecting.\r\n", true);
            }
        } else {
            state.setRecentBadCommands(0);
        }

        assert (response != null);

        return response;
    }

    private static String unencodeBase64String(String base64String) throws IOException {
        Base64InputStream is = new Base64InputStream(new ByteArrayInputStream(base64String.getBytes("UTF-8")));
        ByteArrayOutputStream os = new ByteArrayOutputStream((int) (base64String.length() * 0.7));
        int b;
        while ((b = is.read()) != -1) {
            os.write(b);
        }
        is.close();
        os.close();
        return os.toString("UTF-8");
    }

    private static String encodeBase64String(String string) {
        ByteArrayOutputStream os = new ByteArrayOutputStream((int) (string.length() * 1.4));
        Base64OutputStream b64os = new Base64OutputStream(os);
        try {
            b64os.write(string.getBytes("UTF-8"));
            b64os.close();
            os.close();
            return os.toString("UTF-8");
        } catch (Exception ex) {
            // Error while encoding base64 (shouldn't happen)
            throw new RuntimeException(ex);
        }
    }
}
