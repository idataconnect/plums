/*
 * Copyright (c) 2008-2013, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of i Data Connect! nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.idataconnect.plums.smtp;

import com.idataconnect.plums.entity.MailMessageQueueItem;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * State information for one SMTP connection. This includes the authentication
 * information, sender, recipients, remote host information, etc.
 * @author Ben Upsavs
 */
public class SMTPConnectionState {

    /**
     * An enum representing the main state an SMTP connection is currently in.
     */
    public enum MainState {

        /** Before HELO or EHLO is received. */
        PRE_HELO,
        /** Before MAIL FROM: is received. */
        PRE_MAIL_FROM,
        /**
         * Before DATA is received. At this point, any number of RCPT TO
         * commands may have been received.
         */
        PRE_DATA,
        /** DATA has been received. E-Mail transfer is in progress. */
        DATA,
        /** In the process of multiple step authentication. */
        AUTHENTICATING,
    }

    /**
     * An enum representing the authentication state an SMTP connection is
     * currently in.
     */
    public enum AuthState {

        /** Not in any auth state. */
        NONE,
        /** Already successfully authenticated. */
        AUTHENTICATED,
        /** LOGIN mech selected, waiting for username part. */
        LOGIN_USERNAME,
        /** LOGIN mech selected, waiting for password part. */
        LOGIN_PASSWORD,
        /** CRAM-MD5 selected, waiting for response. */
        CRAM_MD5_RESPONSE,
    }
    private MainState mainState = MainState.PRE_HELO;
    private AuthState authState = AuthState.NONE;
    private String authenticatedUsername;
    private String authenticationMechanism;
    private int cramMd5Pid;
    private int cramMd5Timestamp;
    private boolean trusted;
    private SMTPServerInstance serverInstance;
    private int recentBadCommands;
    private String remoteIdentifier;
    private String mailFrom;
    private final Set<String> rcptTo = new HashSet<String>();
    private int numRecipientsRejected;
    private InetAddress remoteHost;
    private String encodedLoginUsername;
    private List<MailMessageQueueItem> queueItems;
    private Map<MailMessageQueueItem, FileOutputStream> fileOutputStreams =
            new HashMap<MailMessageQueueItem, FileOutputStream>();
    private ByteBuffer byteBuffer;
    private IOException queueException;
    private boolean ehlo;

    /**
     * Gets the main state of the connection.
     * @return The main state of the connection.
     */
    public MainState getMainState() {
        return mainState;
    }

    /**
     * Sets the main state of the current connection.
     * @param mainState The main state of the current connection.
     */
    public void setMainState(MainState mainState) {
        this.mainState = mainState;
    }

    /**
     * Gets the authentication state of the current connection.
     * @return The authentication state of the current connection.
     */
    public AuthState getAuthState() {
        return authState;
    }

    /**
     * Sets the authentication state for the current connection.
     * @param authState The authentication state for the current connection.
     */
    public void setAuthState(AuthState authState) {
        this.authState = authState;
    }

    /**
     * Gets the server instance that the current connection is associated with.
     * @return The server instance.
     */
    public SMTPServerInstance getServerInstance() {
        return serverInstance;
    }

    /**
     * Sets the server instance that the current connection is associated with.
     * @param serverInstance The server instance.
     */
    public void setServerInstance(SMTPServerInstance serverInstance) {
        this.serverInstance = serverInstance;
    }

    /**
     * Gets the number of contiguously bad commands sent to the server via
     * this connection.
     * @return The number of recent bad commands.
     */
    public int getRecentBadCommands() {
        return recentBadCommands;
    }

    /**
     * Sets the number of contiguously bad commands sent to the server
     * via this connection.
     * @param recentBadCommands The number of recent bad commands.
     */
    public void setRecentBadCommands(int recentBadCommands) {
        this.recentBadCommands = recentBadCommands;
    }

    /**
     * Gets the remote identifier sent by the client.
     * @return The remote identifier.
     */
    public String getRemoteIdentifier() {
        return remoteIdentifier;
    }

    /**
     * Sets the remote identifier sent by the client.
     * @param remoteIdentifier The remote identifier.
     */
    public void setRemoteIdentifier(String remoteIdentifier) {
        this.remoteIdentifier = remoteIdentifier;
    }

    /**
     * Gets the sender's email address that was set during this connection.
     * @return The sender's email address.
     */
    public String getMailFrom() {
        return mailFrom;
    }

    /**
     * Sets the sender's email address that was set during this connection.
     * @param mailFrom The sender's email address.
     */
    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    /**
     * Increments the rejected recipients counter.
     */
    public void anotherRecipientRejected() {
        numRecipientsRejected++;
    }

    /**
     * Gets the number of recipients that were rejected during this connection.
     * @return The number of recipients that were rejected during this connection.
     */
    public int getNumRecipientsRejected() {
        return numRecipientsRejected;
    }

    /**
     * Sets the number of recipients that were rejected during this connection.
     * @param numRecipientsRejected The number of recipients that were rejected
     * during this connection.
     */
    public void setNumRecipientsRejected(int numRecipientsRejected) {
        this.numRecipientsRejected = numRecipientsRejected;
    }

    /**
     * Gets the remote host that initiated this connection.
     * @return The remote host.
     */
    public InetAddress getRemoteHost() {
        return remoteHost;
    }

    /**
     * Sets the remote host that initiated this connection.
     * @param remoteHost The remote host.
     */
    public void setRemoteHost(InetAddress remoteHost) {
        this.remoteHost = remoteHost;
    }

    /**
     * Gets the authenticated username for this connection.
     * @return The authenticated username.
     */
    public String getAuthenticatedUsername() {
        return authenticatedUsername;
    }

    /**
     * Sets the authenticated username for this connection.
     * @param authenticatedUsername The authenticated username.
     */
    public void setAuthenticatedUsername(String authenticatedUsername) {
        this.authenticatedUsername = authenticatedUsername;
    }

    /**
     * Gets the name of the mechanism used to successfully authenticate to this
     * connection.
     * @return The authentication mechanism's name.
     */
    public String getAuthenticationMechanism() {
        return authenticationMechanism;
    }

    /**
     * Sets the name of the mechanism used to successfully authenticate to
     * this connection.
     * @param authenticationMechanism
     */
    public void setAuthenticationMechanism(String authenticationMechanism) {
        this.authenticationMechanism = authenticationMechanism;
    }

    /**
     * Gets the CRAM-MD5 pid which is set when the CRAM-MD5 authentication
     * request begins.
     * @return The CRAM-MD5 pid.
     */
    public int getCramMd5Pid() {
        return cramMd5Pid;
    }

    /**
     * Sets the CRAM-MD5 pid which is set when the CRAM-MD5 authentication
     * request begins.
     * @param cramMd5Pid The CRAM-MD5 pid.
     */
    public void setCramMd5Pid(int cramMd5Pid) {
        this.cramMd5Pid = cramMd5Pid;
    }

    /**
     * Gets the CRAM-MD5 timestamp which is set when the CRAM-MD5 authentication
     * request begins.
     * @return The CRAM-MD5 timestamp.
     */
    public int getCramMd5Timestamp() {
        return cramMd5Timestamp;
    }

    /**
     * Sets the CRAM-MD5 timestamp which is set when the CRAM-MD5 authentication
     * request begins.
     * @param cramMd5Timestamp
     */
    public void setCramMd5Timestamp(int cramMd5Timestamp) {
        this.cramMd5Timestamp = cramMd5Timestamp;
    }

    /**
     * If the user is trusted by means other than authentication. For example,
     * the user could be on the local network.
     * @return Whether the user is trusted.
     */
    public boolean isTrusted() {
        return trusted;
    }

    /**
     * Sets whether this user is trusted by means other than authentication.
     * @param trusted Whether the user is trusted.
     */
    public void setTrusted(boolean trusted) {
        this.trusted = trusted;
    }

    /**
     * Gets the encoded username sent by the client during the last AUTH LOGIN
     * transaction.
     * @return The encoded AUTH LOGIN username.
     */
    public String getEncodedLoginUsername() {
        return encodedLoginUsername;
    }

    /**
     * Sets the encoded username sent by the client during the last AUTH LOGIN
     * transaction.
     * @param encodedLoginUsername The encoded AUTH LOGIN username.
     */
    public void setEncodedLoginUsername(String encodedLoginUsername) {
        this.encodedLoginUsername = encodedLoginUsername;
    }

    /**
     * Gets the queue items, to be used when saving a message to the mail queue.
     * @return A list of queue items.
     */
    public List<MailMessageQueueItem> getQueueItems() {
        return queueItems;
    }

    /**
     * Sets the queue items, to be used when saving a message to the mail queue.
     * @param queueItems A list of queue items.
     */
    public void setQueueItems(List<MailMessageQueueItem> queueItems) {
        this.queueItems = queueItems;
    }

    public Map<MailMessageQueueItem, FileOutputStream> getFileOutputStreams() {
        return fileOutputStreams;
    }

    public void setFileOutputStreams(Map<MailMessageQueueItem, FileOutputStream> fileOutputStreams) {
        this.fileOutputStreams = fileOutputStreams;
    }

    public ByteBuffer getByteBuffer() {
        return byteBuffer;
    }

    public void setByteBuffer(ByteBuffer byteBuffer) {
        this.byteBuffer = byteBuffer;
    }

    public IOException getQueueException() {
        return queueException;
    }

    public void setQueueException(IOException queueException) {
        this.queueException = queueException;
    }

    /**
     * Gets whether the client sent an extended hello (EHLO) rather than
     * a legacy hello (HELO).
     * @return Whether the client sent an EHLO.
     */
    public boolean isEhlo() {
        return ehlo;
    }

    /**
     * Sets whether the client sent an extended hello (EHLO) rather than
     * a legacy hello (HELO).
     * @param ehlo Whether the client sent an EHLO.
     */
    public void setEhlo(boolean ehlo) {
        this.ehlo = ehlo;
    }

    /**
     * A convenience method to check if the client is authenticated. This is
     * equivalent to
     * <code>getAuthState().equals(SMTPConnectionState.AuthState.AUTHENTICATED)</code>.
     * @return Whether the client is authenticated.
     */
    public boolean isAuthenticated() {
        return getAuthState().equals(AuthState.AUTHENTICATED);
    }

    // TODO make relay rules more configurable (e.g. not relaying all authenticated)
    /**
     * Whether the client is currently permitted to relay to recipients on
     * remote hosts. This is either because they are authenticated,
     * or because they are trusted for some other reason, e.g. IP address.
     * @return Whether the client may relay to remote recipients.
     */
    public boolean canRelay() {
        return isTrusted() || isAuthenticated();
    }

    /**
     * Adds a recipient to receive a copy of the current message.
     * @param rcptToAddress The recipient's email address.
     */
    public void addRcptTo(String rcptToAddress) {
        rcptTo.add(rcptToAddress);
    }

    /**
     * Removes all recipients from the list of recipients to receive the
     * current message.
     */
    public void clearRcptTo() {
        rcptTo.clear();
    }

    /**
     * Gets the set of recipients.
     * @return The set of recipients.
     */
    public Set<String> getRcptTo() {
        return rcptTo;
    }
}
